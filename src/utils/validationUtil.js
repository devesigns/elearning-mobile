export const getValidationMessage = (code) => {
    switch (code) {
        case 'ACCOUNT_INVALID': return 'Số điện thoại hoặc email không hợp lệ'
        case 'EMAIL_INVALID': return 'Email không hợp lệ'
        case 'EMAIL_MISSING': return 'Bạn phải nhập email'
        case 'PASSWORD_INVALID': return 'Mật khẩu không hợp lệ - cần ít nhất 4 ký tự'
        case 'PASSWORD_MISSING': return 'Bạn phải nhập mật khẩu'
        case 'NAME_MISSING': return 'Bạn phải nhập họ tên đầy đủ'
        case 'BIRTHDAY_INVALID': return 'Ngày sinh không hợp lệ'
        case 'PHONE_INVALID': return 'Số điện thoại không hợp lệ'
        case 'PHONE_MISSING': return 'Bạn phải nhập số điện thoại'
        case 'CODE_INVALID': return 'Mã xác nhận không hợp lệ'
        case 'SIGNIN_UNAUTHORIZED': return 'Tài khoản hoặc mật khẩu không chính xác'
        case 'SIGNIN_404': return 'Tài khoản không tồn tại'
        case 'REQUIRED_FIELD': return 'Trường này là bắt buộc'
        case 'THUMBNAIL_MISSING': return 'Bạn phải upload ảnh thumbnail trước'
        default: return code;
    }
}

const dateInMonth = {
    31: [1, 3, 5, 7, 8, 10, 12],
    30: [4, 6, 9, 11],
    28: [2]
}

const isLeapYear = (year) => {
    return (
        year % 400 === 0 ||
        (year % 4 === 0 && year % 100 != 0)
    )
}

export const validate = {
    validateAccount: (text) => {
        let valid = validate.validatePhone(text);

        if (valid != true) {
            valid = validate.validateEmail(text);

            if (valid === true) {
                return 'email';
            }
        } else {
            return 'phone';
        }

        return 'ACCOUNT_INVALID';
    },
    validateName: (text) => {
        if (text.length === 0) {
            return 'NAME_MISSING';
        }

        return true;
    },
    validatePhone: (text) => {
        let re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;

        if (re.test(text) && text.length > 9 && text.length < 16) {
            return true;
        }

        if (text.length === 0) {
            return 'PHONE_MISSING';
        }

        return 'PHONE_INVALID';
    },
    validateEmail: (text) => {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(text).toLowerCase())) {
            return true;
        }

        if (text.length === 0) {
            return 'EMAIL_MISSING';
        }

        return 'EMAIL_INVALID';
    },
    validatePassword: (text) => {
        if (text.length < 4 && text.length > 0) {
            return 'PASSWORD_INVALID';
        }

        if (text.length === 0) {
            return 'PASSWORD_MISSING';
        }

        return true;
    },
    validatePasswordRetype: (password, passwordRetype) => {
        return (password === passwordRetype) ? true : 'PASSWORD_MISMATCH'
    },
    validateBirthday: (text) => {
        if (text.length < 10) {
            return 'BIRTHDAY_INVALID';
        }

        let arr = text.split('-');
        let year = parseInt(arr[0]);
        let month = parseInt(arr[1]);
        let date = parseInt(arr[2]);

        console.log(arr);

        if (date === 29 && month === 2) {
            if (!isLeapYear(year)) {
                return 'BIRTHDAY_INVALID';
            }
        } else if (dateInMonth[date] && !dateInMonth[date].includes(month)) {
            return 'BIRTHDAY_INVALID';
        }

        return true;
    },
    validateCode: (text) => {
        if (text.length != 6) {
            return 'CODE_INVALID';
        }

        return true;
    }
}

// ---------

export const birthdayConstraint = (oldText, text) => {
    // Template: YYYY-MM-DD

    // If text length > 10, return
    if (text.length > 10) {
        return oldText;
    }

    let output = text;
    let deleteMode = false;

    // If oldText > newText, user is deleting
    if (oldText && oldText.length > text.length) {
        deleteMode = true;
    }

    // On entering/deleting text, insert/remove "-" for month & day
    if ((text.length == 5 || text.length == 8) && !deleteMode) {
        output = output.slice(0, output.length - 1) + '-' + output.slice(output.length - 1, output.length);
    } else if ((text.length == 5 || text.length == 8) && deleteMode) {
        output = output.slice(0, output.length - 1);
    }

    if (!deleteMode) {
        // Month
        if (text.length > 5 && text.length < 8) {
            let month = text.slice(5, 7);
            
            if (parseInt(month) > 12) {
                month = '12';
            } else if (parseInt(month) < 1) {
                month = '01';
            }

            output = output.slice(0, 5) + month;
        }

        // Date
        if (text.length > 8) {
            let date = text.slice(9, 11);
            
            if (parseInt(date) > 31) {
                date = '31';
            } else if (parseInt(date) < 1) {
                date = '01';
            }

            output = output.slice(0, 9) + date;
        }
    }

    return output;
}
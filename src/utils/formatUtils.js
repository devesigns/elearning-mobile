export const fromMinuteToLessonTime = (value) => {
    let day = value % 1440;
    value -= day * 1440;

    let hour = value % 60;
    value -= hour * 60;

    return (day ? day + 'd' : '') + (hour ? hour + 'h' : '') + (value + 'm');
}
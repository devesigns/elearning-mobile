import {
    Dimensions,
    Platform
} from 'react-native';

const window = Dimensions.get('window');
const width = window.width > window.height ? window.height : window.width;
const height = window.width > window.height ? window.width : window.height;

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 375;
const guidelineBaseHeight = 812;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;

export {scale, verticalScale, moderateScale};

export const isIphoneX = () => {
    return (
        Platform.OS === 'ios' &&
        !Platform.isPad &&
        !Platform.isTVOS &&
        ((window.height === 812 || window.width === 812) || (window.height === 896 || window.width === 896))
    );
}
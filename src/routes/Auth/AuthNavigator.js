import {
    createStackNavigator
} from 'react-navigation-stack';
import MainHeader from '../../components/common/MainHeader';
import AuthMainScreen from '../../components/Auth/AuthMainScreen';
import AuthSignInScreen from '../../components/Auth/SignIn/AuthSignInScreen';
import AuthSignUpSuccessScreen from '../../components/Auth/SignUp/AuthSignUpSuccess';
import AuthSignUpScreen from '../../components/Auth/SignUp/AuthSignUpScreen';
import AuthForgetPasswordScreen from '../../components/Auth/ForgetPassword/AuthForgetPasswordScreen';
import AuthForgetPasswordChangePasswordSuccessScreen from '../../components/Auth/ForgetPassword/AuthForgetPasswordChangePasswordSuccessScreen';
import AuthPhoneVerificationScreen from '../../components/Auth/Misc/AuthPhoneVerificationScreen';
import AuthEmailVerificationScreen from '../../components/Auth/Misc/AuthEmailVerificationScreen';
import AuthEmailVerificationSuccessScreen from '../../components/Auth/Misc/AuthEmailVerificationSuccessScreen';
import AuthSocialScreen from '../../components/Auth/Social/AuthSocialScreen';

const AuthNavigator = createStackNavigator(
    {   
        AuthMain: {
            screen: AuthMainScreen,
            navigationOptions: {
                header: null
            }
        },
        AuthSignIn: {
            screen: AuthSignInScreen,
            navigationOptions: {
                title: 'ĐĂNG NHẬP'
            }
        },
        AuthSignUp: {
            screen: AuthSignUpScreen,
            navigationOptions: {
                title: 'TẠO TÀI KHOẢN'
            }
        },
        AuthPhoneVerification: {
            screen: AuthPhoneVerificationScreen,
            navigationOptions: {
                title: 'XÁC NHẬN SỐ ĐIỆN THOẠI',
                headerLeft: null
            }
        },
        AuthEmailVerification: {
            screen: AuthEmailVerificationScreen,
            navigationOptions: {
                title: 'XÁC NHẬN EMAIL',
                headerLeft: null
            }
        },
        AuthEmailVerificationSuccess: {
            screen: AuthEmailVerificationSuccessScreen,
            navigationOptions: {
                header: null
            },
            path: 'verify-email'
        },
        AuthSignUpSuccess: {
            screen: AuthSignUpSuccessScreen,
            navigationOptions: {
                title: 'ĐĂNG KÝ THÀNH CÔNG'
            }
        },
        AuthForgetPassword: {
            screen: AuthForgetPasswordScreen,
            navigationOptions: {
                title: 'QUÊN MẬT KHẨU'
            }
        },
        AuthForgetPasswordChangePasswordSuccess: {
            screen: AuthForgetPasswordChangePasswordSuccessScreen,
            navigationOptions: {
                title: 'THÀNH CÔNG'
            }
        },
        /**
         * Social Login
         */
        AuthSocialSignIn: {
            screen: AuthSocialScreen,
            navigationOptions: {
                header: null
            },
            path: 'social-login'
        }
    },
    {
        initialRouteName: 'AuthMain',
        defaultNavigationOptions: {
            header: MainHeader
        }
        // transitionConfig: () => {
        //     return {
        //         transitionSpec: {
        //             duration: 400,
        //             easing: Easing.out(Easing.poly(4)),
        //             timing: Animated.timing,
        //             useNativeDriver: true,
        //         },
        //         screenInterpolator: (sceneProps) => {      
        //             const { layout, position, scene } = sceneProps
            
        //             const thisSceneIndex = scene.index
        //             const width = layout.initWidth
            
        //             const translateX = position.interpolate({
        //                 inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        //                 outputRange: [width, 0, -width / 4]
        //             })
            
        //             const slideFromRight = { transform: [{ translateX }] }

        //             return slideFromRight
        //         }
        //     }
        // }
    }
);

export default AuthNavigator;
import {
    createStackNavigator
} from 'react-navigation-stack';
import MainHeader from '../../../components/common/MainHeader';
import CoacherProfileScreen from '../../../components/Coacher/Profile/CoacherProfileScreen';
import CoacherLessonListScreen from '../../../components/Coacher/LessonList/CoacherLessonListScreen';
import CoacherCourseListScreen from '../../../components/Coacher/CourseList/CoacherCourseListScreen';

const CoacherNavigator = createStackNavigator(
    {   
        CoacherProfile: {
            screen: CoacherProfileScreen,
            navigationOptions: {
                header: null
            }
        },
        CoacherLessonList: {
            screen: CoacherLessonListScreen,
            navigationOptions: {
                title: 'TẤT CẢ BÀI HỌC'
            }
        },
        CoacherCourseList: {
            screen: CoacherCourseListScreen,
            navigationOptions: {
                title: 'TẤT CẢ KHÓA HỌC'
            } 
        }
    },
    {
        defaultNavigationOptions: {
            header: MainHeader
        }
    }
);

export default CoacherNavigator;
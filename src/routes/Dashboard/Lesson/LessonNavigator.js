import {
    createStackNavigator
} from 'react-navigation-stack';
import LessonScreen from '../../../components/Lesson/LessonScreen';
import ExerciseScreen from '../../../components/Lesson/Exercise/ExerciseScreen';

const LessonNavigator = createStackNavigator(
    {   
        LessonMain: {
            screen: LessonScreen,
            navigationOptions: {
                header: null
            }
        },
        LessonExercise: {
            screen: ExerciseScreen,
            navigationOptions: {
                header: null
            }
        }
    },
    {
        initialRouteName: 'LessonMain'
    }
);

export default LessonNavigator;
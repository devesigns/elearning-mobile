import React from 'react';
import {
    Image
} from 'react-native';
import {
    createBottomTabNavigator
} from 'react-navigation-tabs';
import {
    baseIcon,
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding
} from '../../styles/base';
import {
    moderateScale
} from '../../utils';
import CustomIcon from '../../components/common/CustomIcon';
import HomeScreen from '../../components/Home/HomeScreen';
import AccountNavigator from './Account/AccountNavigator';
import MyClassNavigator from './MyClass/MyClassNavigator';
import CoacherListScreen from '../../components/Coacher/CoacherListScreen';

const DashboardTabNavigator = createBottomTabNavigator(
    {   
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <CustomIcon
                        name="home"
                        size={baseIcon.xl}
                        style={{
                            color: tintColor
                        }}
                    />
                )
            },
            path: 'home'
        },
        CoacherList: {
            screen: CoacherListScreen,
            navigationOptions: {
                tabBarIcon: (props) => {
                    if (props.focused) {
                        return (
                            <Image
                                source={require('../../assets/img/coacher-active.png')}
                                style={{
                                    height: baseIcon.xl,
                                    width: baseIcon.xl
                                }}
                            />
                        )
                    } else {
                        return (
                            <Image
                                source={require('../../assets/img/coacher.png')}
                                style={{
                                    height: baseIcon.xl,
                                    width: baseIcon.xl
                                }}
                            />
                        )
                    }
                }
            }
        },
        MyClass: {
            screen: MyClassNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <CustomIcon
                        name="book"
                        size={baseIcon.xl}
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        },
        Account: {
            screen: AccountNavigator,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <CustomIcon
                        name="user"
                        size={baseIcon.xl}
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        }
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            style: {
                height: moderateScale(50),
                borderTopWidth: 0,
                elevation: 3,
                shadowOffset: {
                    width: 3,
                    height: 3
                },
                shadowOpacity: 0.5,
                shadowColor: 'rgba(0, 0, 0, 0.3)',

            },
            tabStyle: {
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: basePadding.xs,
                backgroundColor: baseColor.one
            },
            labelStyle: {
                padding: 0,
                fontSize: baseFontSize.xs,
                fontFamily: baseFontFamily.regular,
            },
            activeTintColor: baseColor.three,
            inactiveTintColor: baseColor.five,
            showLabel: false
        }
    }
);

export default DashboardTabNavigator;
import {
    createStackNavigator
} from 'react-navigation-stack';
import MainHeader from '../../../components/common/MainHeader';
import AccountScreen from '../../../components/Account/AccountScreen';
import AchievementScreen from '../../../components/Achievement/AchievementScreen';
import PrivacyPolicyScreen from '../../../components/Policy/PrivacyPolicyScreen';
import PromotionScreen from '../../../components/Promotion/PromotionScreen';

const AccountNavigator = createStackNavigator(
    {   
        AccountMain: {
            screen: AccountScreen,
            navigationOptions: {
                header: null
            }
        },
        AccountAchievement: {
            screen: AchievementScreen,
            navigationOptions: {
                title: 'THÀNH TÍCH',
                header: MainHeader
            }
        },
        PrivacyPolicy: {
            screen: PrivacyPolicyScreen,
            navigationOptions: {
                title: 'CHÍNH SÁCH BẢO MẬT',
                header: MainHeader
            }
        },
        Promotion: {
            screen: PromotionScreen,
            navigationOptions: {
                title: 'MÃ KHUYẾN MÃI',
                header: MainHeader
            }
        }
    },
    {
        initialRouteName: 'AccountMain'
    }
);

AccountNavigator.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default AccountNavigator;
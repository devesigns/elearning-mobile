import {
    createStackNavigator
} from 'react-navigation-stack';
import MainHeader from '../../../components/common/MainHeader';
import MyClassScreen from '../../../components/MyClass/MyClassScreen';
import MyClassLessonListScreen from '../../../components/MyClass/MyClassLessonListScreen';
import MyClassCoacherListScreen from '../../../components/MyClass/MyClassCoacherListScreen';

const MyClassNavigator = createStackNavigator(
    {   
        MyClassMain: {
            screen: MyClassScreen,
            navigationOptions: {
                title: 'BÀI HỌC CỦA TÔI'
            }
        },
        MyClassPlaylist: {
            screen: MyClassLessonListScreen
        },
        MyClassSubscribedCoacher: {
            screen: MyClassCoacherListScreen
        }
    },
    {
        initialRouteName: 'MyClassMain',
        defaultNavigationOptions: {
            header: MainHeader
        }
    }
);

MyClassNavigator.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
    };
};

export default MyClassNavigator;
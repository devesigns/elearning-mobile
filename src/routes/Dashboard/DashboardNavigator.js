import {
    createStackNavigator
} from 'react-navigation-stack';
import DashboardTabNavigator from './DashboardTabNavigator';
import LessonNavigator from './Lesson/LessonNavigator';
import CourseDetailScreen from '../../components/Course/CourseDetailScreen';
import MainHeader from '../../components/common/MainHeader';
import CoacherNavigator from './Coacher/CoacherNavigator';
import MessageNavigator from './Message/MessageNavigator';
import SearchResultScreen from '../../components/SearchResult/SearchResultScreen';

const DashboardNavigator = createStackNavigator(
    {   
        DashboardTab: {
            screen: DashboardTabNavigator,
            navigationOptions: {
                header: null
            },
            path: ''
        },
        Lesson: {
            screen: LessonNavigator,
            navigationOptions: {
                header: null
            }
        },
        CourseDetail: {
            screen: CourseDetailScreen,
            navigationOptions: {
                title: 'CHI TIẾT LỘ TRÌNH',
                header: MainHeader
            }
        },
        Coacher: {
            screen: CoacherNavigator,
            navigationOptions: {
                header: null
            }
        },
        Message: {
            screen: MessageNavigator,
            navigationOptions: {
                header: null
            }
        },
        SearchResult: {
            screen: SearchResultScreen,
            navigationOptions: {
                header: null
            }
        }
    },
    {
        initialRouteName: 'DashboardTab'
    }
);

export default DashboardNavigator;
import {
    createStackNavigator
} from 'react-navigation-stack';
import MainHeader from '../../../components/common/MainHeader';
import MessageInboxScreen from '../../../components/Message/MessageInboxScreen';
import MessageConversationScreen from '../../../components/Message/MessageConversationScreen';

const MessageNavigator = createStackNavigator(
    {   
        MessageInbox: {
            screen: MessageInboxScreen,
            navigationOptions: {
                header: null
            }
        },
        MessageConversation: {
            screen: MessageConversationScreen,
            navigationOptions: (props) => {
                return {
                    title: props.navigation.getParam('screenName')
                }
            }
        }
    },
    {
        initialRouteName: 'MessageInbox',
        defaultNavigationOptions: {
            header: MainHeader
        }
    }
);

export default MessageNavigator;
import {
    createAppContainer,
    createSwitchNavigator
} from 'react-navigation';
import DashboardNavigator from './Dashboard/DashboardNavigator';
import AuthNavigator from './Auth/AuthNavigator';
import SplashScreen from '../components/Splash/SplashScreen';

const AppNavigator = createSwitchNavigator(
    {   
        Splash: {
            screen: SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        Auth: {
            screen: AuthNavigator,
            navigationOptions: {
                header: null
            },
            path: 'auth'
        },
        Dashboard: {
            screen: DashboardNavigator,
            navigationOptions: {
                header: null
            },
            path: ''
        }
    },
    {
        initialRouteName: 'Splash',
        // transitionConfig: () => {
        //     return {
        //         transitionSpec: {
        //             duration: 400,
        //             easing: Easing.out(Easing.poly(4)),
        //             timing: Animated.timing,
        //             useNativeDriver: true,
        //         },
        //         screenInterpolator: (sceneProps) => {      
        //             const { layout, position, scene } = sceneProps
            
        //             const thisSceneIndex = scene.index
        //             const width = layout.initWidth
            
        //             const translateX = position.interpolate({
        //                 inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
        //                 outputRange: [width, 0, -width / 4]
        //             })
            
        //             const slideFromRight = { transform: [{ translateX }] }

        //             return slideFromRight
        //         }
        //     }
        // }
    }
);

export default createAppContainer(AppNavigator);
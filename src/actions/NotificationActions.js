import {
    NOTIFICATION_REDUCER_UPDATE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';

export const notificationReducerUpdate = (props, value) => {
    return {
        type: NOTIFICATION_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const notiUpdateUserDevice = (userToken, fcmToken) => {
    return (dispatch) => {
        fetch(
            apiUrl + '/notifications/register_device/gcm',
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${userToken}`
                },
                body: JSON.stringify({
                    registration_id: fcmToken,
                    cloud_message_type: 'FCM'
                })
            }
        )
        .then((res) => {
            console.log('Update FCM token', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Update FCM token', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }
}

export const notificationFetch = (token) => {   
    return (dispatch) => {
        fetch(
            apiUrl + `/notifications/`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get notifications', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get notifications', res);
            if (res.code === 200) {
                dispatch({
                    type: NOTIFICATION_REDUCER_UPDATE,
                    payload: {
                        props: 'notifications',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const notificationStatusUpdate = (token, data) => {
    return (dispatch) => {
        fetch(
            apiUrl + `/notifications/${data.notiId}`,
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify({
                    status: true
                })
            },
        )
        .then((res) => {
            console.log('Update notifications', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Update notifications', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
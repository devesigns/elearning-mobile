import _ from 'lodash';
import {
    MYCLASS_REDUCER_UPDATE,
    MYCLASS_GET_LESSON,
    MYCLASS_GET_COACHER
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';

export const myClassReducerUpdate = (props, value) => {
    return {
        type: MYCLASS_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const myClassGetPlaylist = (token) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/history/my-playlist',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get playlists', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get playlists', res);
            if (res.code === 200) {
                if (!_.isEmpty(res.results)) {
                    dispatch({
                        type: MYCLASS_REDUCER_UPDATE,
                        payload: {
                            props: 'playlists',
                            value: [ res.results ]
                        }
                    });
                } else {
                    dispatch({
                        type: MYCLASS_REDUCER_UPDATE,
                        payload: {
                            props: 'playlists',
                            value: []
                        }
                    });
                }
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const myClassGetLesson = (token) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/course/',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get courses', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get courses', res);
            if (res.code === 200) {
                dispatch({
                    type: MYCLASS_GET_LESSON,
                    payload: {
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const myClassGetBookmarkedLesson = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/history/bookmarks?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get bookmarked lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get bookmarked lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: MYCLASS_GET_LESSON,
                    payload: {
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const myClassGetSubscribedCoacher = (token, data) => {
    let endpoint = apiUrl + `/teacher?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}&subscribed=1`;
    if (data.searchQuery) {
        endpoint += `&name=${data.searchQuery}`;
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get subscribed coachers', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get subscribed coachers', res);
            if (res.code === 200) {
                dispatch({
                    type: MYCLASS_GET_COACHER,
                    payload: {
                        coachers: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
import _ from 'lodash';
import {
    PROMOTION_REDUCER_UPDATE,
    PROMOTION_GET_PROMOTION,
    PROMOTION_REDEEM_CODE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';

export const promotionReducerUpdate = (props, value) => {
    return {
        type: PROMOTION_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const promotionGetPromotion = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/promocode/history?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get promotions', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get promotions', res);
            if (res.code === 200) {
                dispatch({
                    type: PROMOTION_GET_PROMOTION,
                    payload: {
                        promotions: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const promotionRedeemCode = (token, data) => {
    let formData = {
        code: data.code
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/promocode/use',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Redeem code', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then(async (res) => {
            console.log('Redeem code', res);
            if (res.code === 200) {
                alert('Nhận mã thành công!');
                dispatch({
                    type: PROMOTION_REDEEM_CODE,
                    payload: {
                        promotion: {
                            promo_code: res.results
                        }
                    }
                });
            } else if (res.code === 1001) {
                alert('Mã không hợp lệ!');
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
            }
        })
    }
}
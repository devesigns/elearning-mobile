import {
    HELPER_REDUCER_UPDATE,
    HELPER_TOGGLE_LOADING,
    HELPER_HANDLE_ERROR
} from './types';
import {
    authSignOut
} from './AuthActions';

export const helperReducerUpdate = (props, value) => {
    return {    
        type: HELPER_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const helperToggleLoading = (props) => {
    return {
        type: HELPER_TOGGLE_LOADING,
        payload: {
            props
        }
    }
}

export const helperHandleError = (data) => {
    console.log('Error', data);

    return (dispatch) => {
        if (data.type == 'SERVER_ERROR') {
            alert('Something is wrong with the server!');
            console.log(data.res);
            return;
        }

        if (data.res.code === 1002) {
            alert('Có lỗi xảy ra. Vui lòng đăng nhập lại!');
            dispatch(authSignOut());
        } else {
            dispatch({
                type: HELPER_HANDLE_ERROR,
                payload: data
            });
        }
    }
}
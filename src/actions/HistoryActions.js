import _ from 'lodash';
import {
    HISTORY_REDUCER_UPDATE,
    HISTORY_GET_COURSE_SUMMARY
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    accountGetLatestAchievement
} from './AccountActions';
import {
    apiUrl
} from '../const/apiConfig';

export const historyReducerUpdate = (props, value) => {
    return {
        type: HISTORY_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const historyGetCourseSummary = (token, data = null) => {
    let endpoint = apiUrl + `/history/summary`;
    if (data && data.courseId) {
        endpoint += `?course_id=${data.courseId}`;
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get course summary', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get course summary', res);
            if (res.code === 200) {
                if (_.isEmpty(res.results)) {
                    dispatch({
                        type: HISTORY_REDUCER_UPDATE,
                        payload: {
                            props: 'courseSummaries',
                            value: []
                        }
                    })

                    return;
                }

                let courseSummary = res.results;
                let courseId = null;

                if (!data.courseId && courseSummary.all_courses.length >= 1) {
                    courseId = courseSummary.all_courses[0];
                    for (let i = 1; i < courseSummary.all_courses.length; i++) {
                        dispatch(historyGetCourseSummary(token, {
                            courseId: courseSummary.all_courses[i]
                        }))
                    }
                } else {
                    courseId = data.courseId;
                }

                dispatch({
                    type: HISTORY_GET_COURSE_SUMMARY,
                    payload: {
                        courseSummary,
                        courseId
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }))
            }
        })
    }
}

export const historyUpdateStatus = (token, data) => {
    let formData = {
        course: data.courseId,
        material_type: data.materialType,
        material_id: data.materialId,
        status: data.status
    }

    return async (dispatch) => { 
        fetch(
            apiUrl + '/history/learning-tracker',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Update learning tracker', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Update learning tracker', res);

            if (res.code === 200) {
                console.log('Successfully updating status of', data.materialType);
                dispatch(accountGetLatestAchievement(token));
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }))
            }
        })
    } 
}

export const historyUpdateLessonTime = (token, data) => {
    let formData = {
        session: data.sessionId,
        learned_time: data.videoTime
    }

    console.log('Update lesson time', formData);

    return async (dispatch) => {
        fetch(
            apiUrl + `/history/time-tracker`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Update lesson time', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Update lesson time', res);
            if (res.code === 200) {
                // ...
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
import _ from 'lodash';
import {
    Platform,
    Linking
} from 'react-native';
import {
    ACCOUNT_REDUCER_UPDATE,
    ACCOUNT_GET_ACHIEVEMENT,
    ACCOUNT_GET_LATEST_ACHIEVEMENT,
    ACCOUNT_GET_AVAILABLE_PLAN
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';
import AsyncStorage from '@react-native-community/async-storage';

export const accountReducerUpdate = (props, value) => {
    return {
        type: ACCOUNT_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const accountGetUserInfo = (token) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/user/profile',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get user info', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get user info', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_REDUCER_UPDATE,
                    payload: {
                        props: 'user',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const accountEditUserAvatar = (token, data) => {
    let imageFile = data.avatar;
    if (imageFile.fileName) {
        if (imageFile.fileName.endsWith('HEIC')) {
            imageFile.fileName = imageFile.fileName.replace('.HEIC', '.JPG');
        } else if (imageFile.fileName.endsWith('heic')) {
            imageFile.fileName = imageFile.fileName.replace('.heic', '.JPG');
        }
    } else {
        imageFile.fileName = 'Unnamed.JPG';
    }

    let formData = new FormData();
    formData.append('avatar', {
        name: imageFile.fileName,
        type: imageFile.type,
        uri: Platform.OS === 'android' ? imageFile.uri : imageFile.uri.replace("file://", "")
    })

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/user',
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: formData
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Edit user avatar', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Edit user avatar', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_REDUCER_UPDATE,
                    payload: {
                        props: 'user',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
            }
        })
    }
}

export const accountEditUserInfo = (token, data) => {
    let formData = new FormData();
    formData.append('fullname', data.name);
    formData.append('job_description', data.jobDesc);

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/user',
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: formData
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Edit user info', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Edit user info', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_REDUCER_UPDATE,
                    payload: {
                        props: 'user',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
            }
        })
    }
}

export const accountGetAchievement = (token, data) => {
    return async (dispatch) => {
        fetch(
            apiUrl + `/reward/user-reward?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get achievement', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get achievement', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_GET_ACHIEVEMENT,
                    payload: {
                        achievements: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const accountGetRecentAchievement = (token, data) => {
    return async (dispatch) => {
        fetch(
            apiUrl + `/reward/user-reward?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get recent achievement', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get recent achievement', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_REDUCER_UPDATE,
                    payload: {
                        props: 'recentAchievements',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const accountGetLatestAchievement = (token) => {
    return async (dispatch) => {
        fetch(
            apiUrl + `/reward/user-reward?limit=8&offset=0`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get latest achievement', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get latest achievement', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_GET_LATEST_ACHIEVEMENT,
                    payload: {
                        achievements: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const accountUpdateReward = (token, data) => {
    let formData = {
        reward: data.rewardId
    }

    return async (dispatch) => {
        fetch(
            apiUrl + '/reward/update-user-reward',
            {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Update reward', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Update reward', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
            }
        })
    }
}

export const accountUpdatePlan = (token, data) => {
    let formData = {
        product: data.type
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/subscription/upgrade',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Update plan', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then(async (res) => {
            console.log('Update plan', res);
            if (res.code === 200) {
                Linking.openURL(res.results.url);
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'signIn',
                    res
                }));
            }
        })
    }
}

export const accountGetAvailablePlan = (token) => {
    return (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/subscription/plan',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get available plans', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then(async (res) => {
            console.log('Get available plans', res);
            if (res.code === 200) {
                dispatch({
                    type: ACCOUNT_GET_AVAILABLE_PLAN,
                    payload: {
                        plans: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'signIn',
                    res
                }));
            }
        })
    }
}
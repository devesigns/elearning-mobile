import _ from 'lodash';
import {
    COURSE_REDUCER_UPDATE,
    COURSE_GET_DETAIL,
    COURSE_GET_COURSE_SUMMARY_SINGLE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';
export const courseReducerUpdate = (props, value) => {
    return {
        type: COURSE_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const courseGetCourse = (token) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/course',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get courses', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get courses', res);
            if (res.code === 200) {
                dispatch({
                    type: COURSE_REDUCER_UPDATE,
                    payload: {
                        props: 'courses',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const courseGetCourseSummarySingle = (token, data) => {
    let endpoint = apiUrl + `/history/summary`;
    if (data && data.courseId) {
        endpoint += `?course_id=${data.courseId}`;
    }

    return async (dispatch) => {
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get course summary single', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get course summary single', res);
            if (res.code === 200) {
                dispatch({
                    type: COURSE_GET_COURSE_SUMMARY_SINGLE,
                    payload: {
                        courseSummary: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }))
            }
        })
    }
}

export const courseGetDetail = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/${data.courseId}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get course detail', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get course detail', res);
            if (res.code === 200) {
                dispatch({
                    type: COURSE_GET_DETAIL,
                    payload: {
                        courseForm: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
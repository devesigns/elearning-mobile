import _ from 'lodash';
import {
    LESSON_REDUCER_UPDATE,
    LESSON_GET_LESSON,
    LESSON_GET_SESSION,
    LESSON_GET_COMMENT,
    LESSON_POST_COMMENT,
    LESSON_RESET,
    LESSON_LIKE_LESSON,
    LESSON_BOOKMARK_LESSON
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';
import AsyncStorage from '@react-native-community/async-storage';

export const lessonReducerUpdate = (props, value) => {
    return {
        type: LESSON_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const lessonGetLesson = (token, data) => {
    return async (dispatch) => {
        fetch(
            apiUrl + `/course/lesson?course_id=${data.courseId}&limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_LESSON,
                    payload: {
                        type: 'lessons',
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonSearchLesson = (token, data) => {
    let endpoint = apiUrl + `/course/lesson?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`;
    if (data.searchQuery) {
        endpoint += `&title=${data.searchQuery}`;
    }
    
    return async (dispatch) => {
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Search lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Search lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_LESSON,
                    payload: {
                        type: 'searchedLessons',
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetPopularLesson = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/lesson?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}&popular=1`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get popular lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get popular lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_LESSON,
                    payload: {
                        type: 'popularLessons',
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetSubscribedLesson = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/lesson?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}&subscribed=1`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get subscribed lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get subscribed lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_LESSON,
                    payload: {
                        type: 'subscribedLessons',
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetPinnedLesson = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/course/single-lessons',
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get pinned lessons', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get pinned lessons', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_REDUCER_UPDATE,
                    payload: {
                        props: 'pinnedLessons',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetLessonDetail = (token, data, type = 'normal') => {
    let endpoint = apiUrl;
    if (type === 'single') {
        endpoint += `/course/single-lessons/${data.lessonId}`;
    } else {
        endpoint += `/course/lesson/${data.lessonId}`;
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get lesson detail', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get lesson detail', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_REDUCER_UPDATE,
                    payload: {
                        props: 'lessonForm',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetSession = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/session?lesson_id=${data.lessonId}&limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get sessions', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get sessions', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_SESSION,
                    payload: {
                        sessions: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonGetComment = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/comments?lesson_id=${data.lessonId}&limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get comments', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get comments', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_GET_COMMENT,
                    payload: {
                        comments: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonPostComment = (token, data) => {
    let formData = {
        lesson: data.lessonId,
        content: data.content
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/course/comments?lesson_id=${data.lessonId}`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Post comments', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Post comments', res);
            if (res.code === 200) {
                dispatch({
                    type: LESSON_POST_COMMENT,
                    payload: {
                        comment: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonReset = () => {
    return {
        type: LESSON_RESET
    }
}

/**
 * Action menu actions
 */

export const lessonLikeLesson = (token, data) => {
    let formData = {
        lesson: data.lessonId
    }

    return async (dispatch) => {
        dispatch({
            type: LESSON_LIKE_LESSON
        });

        fetch(
            apiUrl + `/history/like`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Like lesson', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Like lesson', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const lessonBookmarkLesson = (token, data) => {
    let formData = {
        lesson: data.lessonId
    }

    return async (dispatch) => {
        dispatch({
            type: LESSON_BOOKMARK_LESSON
        });

        fetch(
            apiUrl + `/history/bookmarks`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Bookmark lesson', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Bookmark lesson', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
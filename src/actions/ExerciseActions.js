import _ from 'lodash';
import {
    EXERCISE_REDUCER_UPDATE,
    EXERCISE_SET_COMPLETE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';
import AsyncStorage from '@react-native-community/async-storage';

export const exerciseReducerUpdate = (props, value) => {
    return {
        type: EXERCISE_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const exerciseGetExercise = (token, data) => {
    return async (dispatch) => {
        fetch(
            apiUrl + `/course/exercise?session_id=${data.sessionId}&limit=${16}&offset=${0}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get exercises', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get exercises', res);
            if (res.code === 200) {
                dispatch({
                    type: EXERCISE_REDUCER_UPDATE,
                    payload: {
                        props: 'exercises',    
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const exerciseSetComplete = () => {
    return {
        type: EXERCISE_SET_COMPLETE
    }
}
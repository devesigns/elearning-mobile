import _ from 'lodash';
import {
    MESSAGE_REDUCER_UPDATE,
    MESSAGE_GET_CONVERSATION,
    MESSAGE_GET_MESSAGE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';

export const messageReducerUpdate = (props, value) => {
    return {
        type: MESSAGE_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

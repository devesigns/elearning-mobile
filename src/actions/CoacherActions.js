import {
    COACHER_REDUCER_UPDATE,
    COACHER_GET_COACHER,
    COACHER_GET_LESSON,
    COACHER_GET_COURSE,
    COACHER_SUBSCRIBE
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import {
    apiUrl
} from '../const/apiConfig';

export const coacherReducerUpdate = (props, value) => {
    return {
        type: COACHER_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const coacherGetCoacher = (token, data) => {
    let endpoint = apiUrl + `/teacher?limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`;
    if (data.searchQuery) {
        endpoint += `&name=${data.searchQuery}`;
    }
    
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get coachers', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get coachers', res);
            if (res.code === 200) {
                dispatch({
                    type: COACHER_GET_COACHER,
                    payload: {
                        coachers: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const coacherGetDetail = (token, data) => {
    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + `/teacher/${data.coacherId}`,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Get coacher detail', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get coacher detail', res);
            if (res.code === 200) {
                dispatch({
                    type: COACHER_REDUCER_UPDATE,
                    payload: {
                        props: 'coacherForm',
                        value: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const coacherGetLesson = (token, data) => {
    let endpoint = apiUrl + `/course/lesson?created_by=${data.coacherId}&limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`;

    if (data.type === 'popular') {
        endpoint += '&popular=1'
    } else if (data.type === 'new') {
        endpoint += '&new=1';
    }

    if (data.searchQuery) {
        endpoint += `&title=${data.searchQuery}`;
    }

    return async (dispatch) => {
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get coacher lesson', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get coacher lesson', res);
            if (res.code === 200) {
                dispatch({
                    type: COACHER_GET_LESSON,
                    payload: {
                        type: data.type,
                        lessons: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const coacherGetCourse = (token, data) => {
    let endpoint = apiUrl + `/course?created_by=${data.coacherId}&limit=${data.perPage}&offset=${(data.page - 1) * data.perPage}`;

    if (data.type === 'popular') {
        endpoint += '&popular=1'
    } else if (data.type === 'new') {
        endpoint += '&new=1';
    }

    if (data.searchQuery) {
        endpoint += `&title=${data.searchQuery}`;
    }

    return async (dispatch) => {
        fetch(
            endpoint,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                }
            },
        )
        .then((res) => {
            console.log('Get coacher course', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Get coacher course', res);
            if (res.code === 200) {
                dispatch({
                    type: COACHER_GET_COURSE,
                    payload: {
                        type: data.type,
                        courses: res.results
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}

export const coacherSubscribe = (token, data) => {
    let formData = {
        teacher: data.coacherId
    }

    return async (dispatch) => {
        dispatch({
            type: COACHER_SUBSCRIBE
        });

        fetch(
            apiUrl + `/teacher/subscribe`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            console.log('Subscribe to coacher', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Subscribe to coacher', res);
            if (res.code === 200) {
                // ---
            } else {
                dispatch(helperHandleError({
                    type: -1,
                    res: res
                }));
    
            }
        })
    }
}
export const AUTH_REDUCER_UPDATE = 'auth_reducer_update';
export const AUTH_LOGIN_SUCCESS = 'auth_login_success';
export const AUTH_LOGOUT_SUCCESS = 'auth_logout_success';
// ---
export const HELPER_REDUCER_UPDATE = 'helper_reducer_update';
export const HELPER_TOGGLE_LOADING = 'helper_toggle_loading';
export const HELPER_HANDLE_ERROR = 'helper_handle_error';
// ---
export const COURSE_REDUCER_UPDATE = 'course_reducer_update';
export const COURSE_GET_DETAIL = 'course_get_detail';
export const COURSE_GET_COURSE_SUMMARY_SINGLE = 'course_get_course_summary_single';
// ---
export const LESSON_REDUCER_UPDATE ='lesson_reducer_update';
export const LESSON_GET_LESSON = 'lesson_get_lesson';
export const LESSON_GET_SESSION = 'lesson_get_session';
export const LESSON_GET_COMMENT = 'lesson_get_comment';
export const LESSON_POST_COMMENT = 'lesson_post_comment';
export const LESSON_RESET = 'lesson_reset';
export const LESSON_LIKE_LESSON = 'lesson_like_lesson';
export const LESSON_BOOKMARK_LESSON = 'lesson_bookmark_lesson';
// ---
export const EXERCISE_REDUCER_UPDATE = 'exercise_reducer_update';
export const EXERCISE_GET_EXERCISE = 'exercise_get_exercise';
export const EXERCISE_SET_COMPLETE = 'exercise_complete';
// ---
export const ACCOUNT_REDUCER_UPDATE = 'account_reducer_update';
export const ACCOUNT_GET_ACHIEVEMENT = 'account_get_achievement';
export const ACCOUNT_GET_LATEST_ACHIEVEMENT = 'account_get_latest_achievement';
export const ACCOUNT_GET_AVAILABLE_PLAN = 'account_get_available_plan';
// ---
export const HISTORY_REDUCER_UPDATE = 'history_reducer_update';
export const HISTORY_GET_COURSE_SUMMARY = 'history_get_course_summary';
// ---
export const NOTIFICATION_REDUCER_UPDATE = 'notification_reducer_update';
// ---
export const MYCLASS_REDUCER_UPDATE = 'myclass_reducer_update';
export const MYCLASS_GET_LESSON = 'myclass_get_lesson';
export const MYCLASS_GET_COACHER = 'myclass_get_coacher';
// ---
export const COACHER_REDUCER_UPDATE = 'coacher_reducer_update';
export const COACHER_GET_COACHER = 'coacher_get_coacher';
export const COACHER_GET_LESSON = 'coacher_get_lesson';
export const COACHER_GET_COURSE = 'coacher_get_course';
export const COACHER_SUBSCRIBE = 'coacher_subscribe';
// ---
export const MESSAGE_REDUCER_UPDATE = 'message_reducer_update';
export const MESSAGE_GET_CONVERSATION = 'message_get_conversation';
export const MESSAGE_GET_MESSAGE = 'message_get_message';
// ---
export const PROMOTION_REDUCER_UPDATE = 'promotion_reducer_update';
export const PROMOTION_GET_PROMOTION = 'promotion_get_promotion';
export const PROMOTION_REDEEM_CODE = 'promotion_redeem_code';
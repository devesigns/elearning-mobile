import _ from 'lodash';
import {
    Linking
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {
    AUTH_REDUCER_UPDATE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT_SUCCESS
} from './types';
import {
    helperToggleLoading,
    helperHandleError
} from './HelperActions';
import { accountReducerUpdate } from './AccountActions';
import {
    apiUrl,
    facebookApiUrl,
    zaloApiUrl
} from '../const/apiConfig';
import { navigate } from '../utils';

export const authReducerUpdate = (props, value) => {
    return {
        type: AUTH_REDUCER_UPDATE,
        payload: {
            props,
            value
        }
    }
}

export const authGetToken = () => {
    return async (dispatch) => {
        let tokenString = await AsyncStorage.getItem('sla-token');
        let accessToken = await JSON.parse(tokenString);

        if (!_.isEmpty(accessToken)) {
            console.log(accessToken);
            dispatch({
                type: AUTH_REDUCER_UPDATE,
                payload: {
                    props: 'accessToken',
                    value: accessToken
                }
            });
        } else {
            dispatch({
                type: AUTH_REDUCER_UPDATE,
                payload: {
                    props: 'accessToken',
                    value: 'none'
                }
            });
        }
    }
}

export const authSignIn = (data) => {
    let formData = {
        username: data.account,
        password: data.password
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/login',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Sign in', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then(async (res) => {
            console.log('Sign in', res);
            if (res.code === 200) {
                await AsyncStorage.setItem('sla-token', JSON.stringify(res.results.token));

                dispatch({
                    type: AUTH_LOGIN_SUCCESS,
                    payload: {
                        token: res.results.token
                    }
                });

                // dispatch({
                //     type: ACCOUNT_REDUCER_UPDATE,
                //     payload: {
                //         props: 'user',
                //         value: res.results.user
                //     }
                // })
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'signIn',
                    res
                }));
            }
        })
    }
}

export const authSignUp = (data) => {
    let formData = {
        username: data.account,
        fullname: data.name,
        password: data.password
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/register',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Sign up', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Sign up', res);
            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'register',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'signUp',
                    res
                }));
            }
        })
    }
}

export const authSignOut = () => {
    return async (dispatch) => {
        await AsyncStorage.removeItem('sla-token');

        dispatch({
            type: AUTH_LOGOUT_SUCCESS
        })

        navigate('Auth');

        dispatch(accountReducerUpdate('user', {}));
    }
}

export const authSendOTP = (data) => {
    let formData = {
        phone: data.account,
        type: data.type
    };

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/send-sms-token',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Send OTP', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Send OTP', res);

            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'sendOTP',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: data.type === 'registration' ? 'signUp' : 'forgetPassword',
                    res
                }));
            }
        })
    }
}

export const authVerifyOTP = (data) => {
    let formData = {
        phone: data.account,
        token: data.code
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/verify-phone-number',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Verify OTP', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Verify OTP', res);

            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'verifyOTP',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'otp',
                    res
                }));
            }
        })
    }
}

export const authSendEmail = (data) => {
    let formData = {
        email: data.account
    };

    return async (dispatch) => { 
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/request-reset-password',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Send email', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Send email', res);

            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'sendEmail',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: data.type === 'registration' ? 'signUp' : 'forgetPassword',
                    res
                }));
            }
        })
    } 
}

export const authResetPassword = (data) => {
    let formData = {
        username: data.account,
        new_password: data.password
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/reset-password',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Reset password', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Reset password', res);

            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'resetPassword',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'resetPassword',
                    res
                }));
            }
        })
    }
}

export const authChangePassword = (token, data) => {
    let formData = {
        new_password: data.newPassword
    }

    return async (dispatch) => {
        dispatch(helperToggleLoading('globalLoading'));
        fetch(
            apiUrl + '/auth/change-password',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Token ${token}`
                },
                body: JSON.stringify(formData)
            },
        )
        .then((res) => {
            dispatch(helperToggleLoading('globalLoading'));
            console.log('Change password', res);

            if (res.status === 500) {
                dispatch(helperHandleError({
                    type: 'SERVER_ERROR'
                }));
                
                return null;
            } else {
                return res.json();
            }
        })
        .then((res) => {
            console.log('Change password', res);

            if (res.code === 200) {
                dispatch({
                    type: AUTH_REDUCER_UPDATE,
                    payload: {
                        props: 'changePassword',
                        value: 'success'
                    }
                });
            } else {
                dispatch(helperHandleError({
                    type: 'authValidation',
                    section: 'changePassword',
                    res
                }));
            }
        })
    }
}

/**
 * Social signin
 */

export const authSignInWithFacebook = (data) => {
    return async (dispatch) => {
        Linking.openURL(facebookApiUrl)
    }
}

export const AuthSignInWithZalo = (data) => {
    return async (dispatch) => {
        Linking.openURL(zaloApiUrl)
    }
}
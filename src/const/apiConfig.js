export const apiUrl = 'https://api.tvbh.app/api/v1';
export const facebookApiUrl = 'https://www.facebook.com/v3.0/dialog/oauth?client_id=2790300454325310&redirect_uri=https://api.tvbh.app/api/v1/social-account/facebook&state=123456';
export const zaloApiUrl = 'https://oauth.zaloapp.com/v3/auth?app_id=3145957010193574304&redirect_uri=https://api.tvbh.app/api/v1/social-account/zalo&state=active'
import _ from 'lodash';
import {
    COURSE_REDUCER_UPDATE,
    COURSE_GET_DETAIL,
    COURSE_GET_COURSE_SUMMARY_SINGLE
} from '../actions/types';

const INITIAL_STATE = {
    courseForm: {},
    courses: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case COURSE_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case COURSE_GET_COURSE_SUMMARY_SINGLE: {
            let newCourseForm = {};

            if (!_.isEmpty(action.payload.courseSummary)) {
                newCourseForm = {
                    ...action.payload.courseSummary,
                    ...state.courseForm
                };
                newCourseForm.course.percentage = newCourseForm.completed_percentage;
            } else {
                newCourseForm = {
                    completed_percentage: 0,
                    ...state.courseForm
                }
                newCourseForm.course.percentage = 0;
            }

            return {
                ...state,
                courseForm: newCourseForm
            }
        }
        case COURSE_GET_DETAIL: {
            let newCourseForm = { ...state.courseForm };
            newCourseForm.course = {
                ...state.courseForm.course,
                ...action.payload.courseForm
            };

            return {
                ...state,
                courseForm: newCourseForm
            }
        }
        default: {
            return state;
        }
    }
}
import {
    NOTIFICATION_REDUCER_UPDATE
} from '../actions/types';

const INITIAL_STATE = {
    notifications: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case NOTIFICATION_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        default: {
            return state;
        }
    }
}
import {
    MESSAGE_REDUCER_UPDATE,
    MESSAGE_GET_CONVERSATION,
    MESSAGE_GET_MESSAGE
} from '../actions/types';

const INITIAL_STATE = {
    conversations: null,
    currentConversation: {
        id: '',
        messages: null
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MESSAGE_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case MESSAGE_GET_CONVERSATION: {
            let newConversations = [
                ...(state.conversations ? state.conversations : []),
                ...action.payload.conversations
            ]

            return {
                ...state,
                conversations: newConversations
            }
        }
        case MESSAGE_GET_MESSAGE: {
            let newMessages = [
                ...(state.currentConversation.messages ? state.currentConversation.messages : []),
                ...action.payload.messages
            ]

            return {
                ...state,
                currentConversation: {
                    ...state.currentConversation,
                    messages: newMessages
                }
            }
        }
        default: {
            return state;
        }
    }
}
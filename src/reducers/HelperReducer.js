import _ from 'lodash';
import {
    HELPER_REDUCER_UPDATE,
    HELPER_TOGGLE_LOADING,
    HELPER_HANDLE_ERROR
} from '../actions/types';

const INITIAL_STATE = {
    loading: {
        globalLoading: false
    },
    authValidation: {
        signIn: null,
        signUp: null,
        forgetPassword: null,
        otp: null
    },
    userValidation: {
        userEdit: null
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case HELPER_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case HELPER_TOGGLE_LOADING: {
            let newLoading = { ...state.loading };
            newLoading[action.payload.props] = !newLoading[action.payload.props];
            
            return {
                ...state,
                loading: newLoading
            }
        }
        case HELPER_HANDLE_ERROR: {
            let newType = { ...state[action.payload.type] };
            let res = action.payload.res;

            if (_.isEmpty(res)) {
                newType[action.payload.section] = null;
            } else {
                switch (res.code) {
                    case 1001:  // Validation failed
                    {
                        if (!_.isEmpty(res.errors)) {
                            newType[action.payload.section] = res.errors;
                        }

                        break;
                    }
                    case 1200:  // User not found
                    case 1201:  // Wrong password
                    case 1203:  // Invalid phone number
                    case 1204:  // Invalid verification code
                    case 1205:  // User already existed
                    case 1207:  // Username already existed
                    {
                        if (res.message) { 
                            newType[action.payload.section] = `[${res.code}] ${res.message}`;
                        }

                        break;
                    }
                    default: {
                        if (action.payload.res.code && action.payload.res.message) {
                            alert(`Code ${action.payload.res.code}: ${action.payload.res.message}`);
                        } else {
                            alert(action.payload);
                        }

                        break;
                    }
                }
            }

            return {
                ...state,
                [action.payload.type]: newType
            }
        }
        default: {
            return state;
        }
    }
}
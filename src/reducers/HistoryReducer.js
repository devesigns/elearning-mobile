import {
    HISTORY_REDUCER_UPDATE,
    HISTORY_GET_COURSE_SUMMARY
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {
    courseSummaries: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case HISTORY_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case HISTORY_GET_COURSE_SUMMARY: {
            let courseSummary = action.payload.courseSummary;
            courseSummary.course.id = action.payload.courseId;
            courseSummary.course.percentage = courseSummary.completed_percentage;

            let newCourseSummaries = [
                ...(state.courseSummaries ? state.courseSummaries : []),
                courseSummary
            ];
            
            return {
                ...state,
                courseSummaries: newCourseSummaries
            }
        }
        default: {
            return state;
        }
    }
}
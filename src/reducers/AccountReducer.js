import {
    ACCOUNT_REDUCER_UPDATE,
    ACCOUNT_GET_ACHIEVEMENT,
    ACCOUNT_GET_LATEST_ACHIEVEMENT,
    ACCOUNT_GET_AVAILABLE_PLAN
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {
    user: {},
    achievements: null,
    latestAchievements: null,
    recentAchievements: null,
    availablePlans: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACCOUNT_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case ACCOUNT_GET_ACHIEVEMENT: {
            let newAchievements = [
                ...(state.achievements ? state.achievements : []),
                ...action.payload.achievements
            ];

            return {
                ...state,
                achievements: newAchievements
            }
        }
        case ACCOUNT_GET_LATEST_ACHIEVEMENT: {
            let newLatestAchievements = [];

            for (let i = 0; i < action.payload.achievements.length; i++) {
                if (!action.payload.achievements[i].seen && action.payload.achievements[i].achieved) {
                    newLatestAchievements.push(action.payload.achievements[i]);
                }
            }

            return {
                ...state,
                latestAchievements: newLatestAchievements
            }
        }
        case ACCOUNT_GET_AVAILABLE_PLAN: {
            let plans = [ ...action.payload.plans ];

            for (let i = 0; i < plans.length; i++) {
                switch (plans[i].type) {
                    case 'monthly': {
                        plans[i].unit = 'tháng';
                        break;
                    }
                    case 'yearly': {
                        plans[i].unit = 'năm';
                        break;
                    }
                    default: break;
                }
            }

            return {
                ...state,
                availablePlans: plans
            }
        }
        default: {
            return state;
        }
    }
}
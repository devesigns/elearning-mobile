import {
    AUTH_REDUCER_UPDATE,
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGOUT_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
    accessToken: null,
    sendOTP: null,
    verifyOTP: null,
    sendEmail: null,
    verifyEmail: null,
    register: null,
    changePassword: null,
    accountForm: {}
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case AUTH_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case AUTH_LOGIN_SUCCESS: {    
            return {
                ...state,
                accessToken: action.payload.token
            }
        }
        case AUTH_LOGOUT_SUCCESS: {
            return {
                ...state,
                accessToken: null
            }
        }
        default: {
            return state;
        }
    }
}
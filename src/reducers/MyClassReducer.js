import {
    MYCLASS_REDUCER_UPDATE,
    MYCLASS_GET_LESSON,
    MYCLASS_GET_COACHER
} from '../actions/types';

const INITIAL_STATE = {
    playlists: null,
    lessons: null,
    coachers: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case MYCLASS_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case MYCLASS_GET_LESSON: {
            let newLessons = [
                ...(state.lessons ? state.lessons : []),
                ...action.payload.lessons
            ]

            return {
                ...state,
                lessons: newLessons
            }
        }
        case MYCLASS_GET_COACHER: {
            let newCoachers = [
                ...(state.coachers ? state.coachers : []),
                ...action.payload.coachers
            ]

            return {
                ...state,
                coachers: newCoachers
            }
        }
        default: {
            return state;
        }
    }
}
import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import HelperReducer from './HelperReducer';
import CourseReducer from './CourseReducer';
import LessonReducer from './LessonReducer';
import AccountReducer from './AccountReducer';
import ExerciseReducer from './ExerciseReducer';
import HistoryReducer from './HistoryReducer';
import MyClassReducer from './MyClassReducer';
import CoacherReducer from './CoacherReducer';
import MessageReducer from './MessageReducer';
import PromotionReducer from './PromotionReducer';
import NotificationReducer from './NotificationReducer';

export default combineReducers({
    auth: AuthReducer,
    helper: HelperReducer,
    course: CourseReducer,
    lesson: LessonReducer,
    exercise: ExerciseReducer,
    account: AccountReducer,
    history: HistoryReducer,
    myClass: MyClassReducer,
    coacher: CoacherReducer,
    message: MessageReducer,
    promotion: PromotionReducer,
    notification: NotificationReducer
});
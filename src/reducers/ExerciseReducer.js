import {
    EXERCISE_REDUCER_UPDATE,
    EXERCISE_GET_EXERCISE,
    EXERCISE_SET_COMPLETE
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {
    currentExerciseArrayId: null,
    exercises: null,
    exerciseComplete: false
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case EXERCISE_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case EXERCISE_GET_EXERCISE: {
            let newExercises = [
                ...(state.exercises ? state.exercises : []),
                ...action.payload.exercises
            ];

            return {
                ...state,
                exercises: newExercises
            }
        }
        case EXERCISE_SET_COMPLETE: {
            return {
                ...INITIAL_STATE,
                exerciseComplete: true
            }
        }
        default: {
            return state;
        }
    }
}
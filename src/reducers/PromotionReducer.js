import {
    PROMOTION_REDUCER_UPDATE,
    PROMOTION_GET_PROMOTION,
    PROMOTION_REDEEM_CODE
} from '../actions/types';

const INITIAL_STATE = {
    promotions: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PROMOTION_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case PROMOTION_GET_PROMOTION: {
            let newPromotions = [
                ...(state.promotions ? state.promotions : []),
                ...action.payload.promotions
            ]

            return {
                ...state,
                promotions: newPromotions
            }
        }
        case PROMOTION_REDEEM_CODE: {
            let newPromotions = [
                action.payload.promotion,
                ...(state.promotions ? state.promotions : [])
            ]

            return {
                ...state,
                promotions: newPromotions
            }
        }
        default: {
            return state;
        }
    }
}
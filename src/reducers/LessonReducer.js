import {
    LESSON_REDUCER_UPDATE,
    LESSON_GET_LESSON,
    LESSON_GET_SESSION,
    LESSON_GET_COMMENT,
    LESSON_POST_COMMENT,
    LESSON_RESET,
    LESSON_LIKE_LESSON,
    LESSON_BOOKMARK_LESSON
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

const INITIAL_STATE = {
    lessonId: '',
    lessonType: '',
    lessonForm: {},
    lessons: null,
    popularLessons: null,
    pinnedLessons: null,
    subscribedLessons: null,
    currentSession: {
        id: null,
        arrayId: null,
        src: null,
        duration: null
    },
    continuedSession: {},
    sessions: null,
    comments: null,
    searchedLessons: null
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LESSON_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case LESSON_GET_LESSON: {
            let newLessons = [
                ...(state[action.payload.type] ? state[action.payload.type] : []),
                ...action.payload.lessons
            ];

            return {
                ...state,
                [action.payload.type]: newLessons
            }
        }
        case LESSON_GET_SESSION: {
            let newSessions = [
                ...(state.sessions ? state.sessions : []),
                ...action.payload.sessions
            ];

            return {
                ...state,
                sessions: newSessions
            }
        }
        case LESSON_GET_COMMENT: {
            let newComments = [
                ...(state.comments ? state.comments : []),
                ...action.payload.comments
            ];

            return {
                ...state,
                comments: newComments
            }
        }
        case LESSON_POST_COMMENT: {
            let newComments = [
                action.payload.comment,
                ...(state.comments ? state.comments : [])
            ];

            return {
                ...state,
                comments: newComments
            }
        }
        case LESSON_RESET: {
            return {
                ...INITIAL_STATE
            }
        }
        case LESSON_LIKE_LESSON: {
            let newLessonForm = { ...state.lessonForm };
            newLessonForm.liked = !newLessonForm.liked;
            newLessonForm.total_like = newLessonForm.total_like + (newLessonForm.liked ? 1 : -1);

            return {
                ...state,
                lessonForm: newLessonForm
            }
        }
        case LESSON_BOOKMARK_LESSON: {
            let newLessonForm = { ...state.lessonForm };
            newLessonForm.bookmarked = !newLessonForm.bookmarked;

            return {
                ...state,
                lessonForm: newLessonForm
            }
        }
        default: {
            return state;
        }
    }
}
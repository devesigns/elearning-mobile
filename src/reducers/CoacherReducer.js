import {
    COACHER_REDUCER_UPDATE,
    COACHER_GET_COACHER,
    COACHER_GET_LESSON,
    COACHER_GET_COURSE,
    COACHER_SUBSCRIBE
} from '../actions/types';

const INITIAL_STATE = {
    coachers: null,
    coacherForm: {},
    lessons: {
        popular: null,
        new: null,
        all: null
    },
    courses: {
        popular: null,
        new: null,
        all: null
    }
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case COACHER_REDUCER_UPDATE: {
            return {
                ...state,
                [action.payload.props]: action.payload.value
            }
        }
        case COACHER_GET_COACHER: {
            let newCoachers = [
                ...(state.coachers ? state.coachers : []),
                ...action.payload.coachers
            ]

            return {
                ...state,
                coachers: newCoachers
            }
        }
        case COACHER_GET_LESSON: {
            let newLessons = [
                ...(state[action.payload.type] ? state[action.payload.type] : []),
                ...action.payload.lessons
            ]

            return {
                ...state,
                lessons: {
                    ...state.lessons,
                    [action.payload.type]: newLessons
                }
            }
        }
        case COACHER_GET_COURSE: {
            let newCourses = [
                ...(state[action.payload.type] ? state[action.payload.type] : []),
                ...action.payload.courses
            ]

            return {
                ...state,
                courses: {
                    ...state.courses,
                    [action.payload.type]: newCourses
                }
            }
        }
        case COACHER_SUBSCRIBE: {
            let newCoacherForm = { ...state.coacherForm };
            newCoacherForm.subscribed = !newCoacherForm.subscribed;
            if (newCoacherForm.subscribed) {
                newCoacherForm.total_subscribers += 1;
            } else {
                newCoacherForm.total_subscribers -= 1;
            }

            return {
                ...state,
                coacherForm: newCoacherForm
            }
        }
        default: {
            return state;
        }
    }
}
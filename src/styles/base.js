import {
    Dimensions
} from 'react-native';
import {
    moderateScale
} from '../utils';

export const baseMargin = {
    xs: moderateScale(5),
    sm: moderateScale(10),
    md: moderateScale(15),
    lg: moderateScale(20),
    xl: moderateScale(25),
    xxl: moderateScale(30),
    xxxl: moderateScale(45)
}

export const basePadding = {
    xxs: moderateScale(3),
    xs: moderateScale(5),
    sm: moderateScale(10),
    md: moderateScale(15),
    lg: moderateScale(20),
    xl: moderateScale(25),
    xxl: moderateScale(30),
    xxxl: moderateScale(45)
}

export const baseFontSize = {
    xs: moderateScale(10),
    sm: moderateScale(12),
    md: moderateScale(14),
    lg: moderateScale(16),
    xl: moderateScale(18),
    xxl: moderateScale(20),
    sl: moderateScale(30)
}

export const baseFontFamily = {
    light: 'Quicksand-Light',
    regular: 'Quicksand-Regular',
    bold: 'Quicksand-Bold',
}

export const baseIcon = {
    xxs: moderateScale(14),
    xs: moderateScale(16),
    sm: moderateScale(18),
    md: moderateScale(20),
    lg: moderateScale(22),
    xl: moderateScale(24),
    xxl: moderateScale(26),
    xxxl: moderateScale(28),
    xxxxl: moderateScale(30)
}

export const baseColor = {
    one: '#FDFDFD',
    two: '#F0F0F2',
    three: '#FFCB45',
    four: '#292C2F',
    five: '#ADB2CA',
    six: '#3FC03C',
    seven: '#456EEA',
    eight: '#2E39BA',
    nine: '#F6F6F6',
    ten: '#F74F4F'
}

export const baseBorderRadius = {
    md: moderateScale(5),
    lg: moderateScale(10),
    xl: moderateScale(15)
}

const window = Dimensions.get('window');
const width = window.width > window.height ? window.height : window.width;
const height = window.width > window.height ? window.width : window.height;

export const dimension = {
    width,
    height
}
/**
 * 
 * Firebase Messaging Setup
 * 
 */

import _ from 'lodash';
import { useState, useEffect } from 'react';
import {
    Platform,
    Linking
} from 'react-native';
import { connect } from 'react-redux';
import {
    notiUpdateUserDevice
} from '../actions';
import firebase from 'react-native-firebase';

const NotificationHandler = (props) => {
    const [listener, setListener] = useState(() => null);

    /**
     * Methods
     */

    const checkMessagingPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            // user has permissions
            console.log('Messaging permission granted.');
        } else {
            // user doesn't have permission
            try {
                await firebase.messaging().requestPermission();
                // User has authorised
            } catch (error) {
                // User has rejected permissions
                alert('Học viện BHNT 4.0 sẽ không gửi notification cho bạn được!')

                if (Platform.OS === 'ios') {
                    console.log('Prompting to enable notification');
                    Linking.openURL(`app-settings://notification/com.devesigns.sla.elearning`);
                }
            }
        }
    }

    // Retrive the token and update user device token info
    const retrieveTCMToken = async () => {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            console.log('FCM Token', fcmToken, props.accessToken);
            props.notiUpdateUserDevice(props.accessToken, fcmToken);
            return fcmToken;
        } else {
            alert('Error: Device token not found')
        }
    }

    const onNotification = (notification) => {
        // Process your notification as required
        const localNotification = new firebase.notifications.Notification({
                show_in_foreground: true
            })
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setBody(notification.body);
        
        if (Platform.OS === 'android') {
            localNotification
            .android.setChannelId('sla-elearning-channel')
            .android.setSmallIcon('ic_stat_ic_notification')
            .android.setLargeIcon('ic_stat_ic_notification')
            .android.setPriority(firebase.notifications.Android.Priority.High);
        }

        console.log(localNotification);
        firebase.notifications().displayNotification(localNotification)
        .catch((error) => {
            console.log(error);
        });
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.accessToken && props.accessToken != 'none') {
            checkMessagingPermission();
            retrieveTCMToken();

            // Build a channel (Android only)
            if (Platform.OS === 'android') {
                const notiChannel = new firebase.notifications.Android.Channel('sla-elearning-channel', 'Elearning Channel', firebase.notifications.Android.Importance.Max)
                    .setDescription('Elearning notification channel');

                // Create the channel
                firebase.notifications().android.createChannel(notiChannel);
            }

            // Listener for incoming notification
            firebase.notifications().onNotification(onNotification);
        }

        return () => {
            if (listener) {
                listener();
            }
        }
    }, [props.accessToken]);

    return null;
}

const mapStateToProps = ({ auth }) => {
    return {
        accessToken: auth.accessToken
    }
}

export default connect(mapStateToProps, {
    notiUpdateUserDevice
})(NotificationHandler);
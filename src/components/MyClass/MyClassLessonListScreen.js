import React, { useState } from 'react';
import {
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    myClassReducerUpdate,
    myClassGetBookmarkedLesson
} from '../../actions';
import {
    basePadding,
    baseColor
} from '../../styles/base';
import MyClassLessonListItem from './MyClassLessonListItem';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import { VideoListPlaceholder } from '../common/Placeholder';

const MyClassLessonListScreen = (props) => {

    const [mounted, setMounted] = useState(false);

    /**
     * Methods
     */

    const onRefresh = () => {
        props.myClassReducerUpdate('lessons', null);
    }

    const onLessonPress = (lesson) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', lesson.id);
        props.lessonReducerUpdate('lessonType', lesson.course ? 'normal' : 'single');
        props.navigation.navigate('Lesson');
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }

    /**
     * Render methods
     */

    const renderLessonItem = ({item, index}) => {
        return (
            <MyClassLessonListItem
                data={item.lesson}
                onPress={() => onLessonPress(item.lesson)}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <CustomFlatListWithFetch
                keyExtractor={(item, index) => {
                    return 'playlist-item-' + item.lesson.id
                }}
                data={props.lessons}
                renderItem={renderLessonItem}
                fetchNextBatch={(page, perPage) => {
                    props.myClassGetBookmarkedLesson(props.accessToken, {
                        page,
                        perPage
                    })
                }}
                onRefresh={onRefresh}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md,
                }}
                renderPlaceholder={() => {
                    return (
                        <VideoListPlaceholder />
                    )
                }}
                perPage={16}
            />
        </SafeAreaView>
    )
}

MyClassLessonListScreen.navigationOptions = ({ navigation }) => {
    return {
        title: 'DANH SÁCH BÀI HỌC'
    }
}

const mapStateToProps = ({ auth, myClass }) => {
    return {
        accessToken: auth.accessToken,
        lessons: myClass.lessons
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    myClassGetBookmarkedLesson,
    myClassReducerUpdate
})(MyClassLessonListScreen);
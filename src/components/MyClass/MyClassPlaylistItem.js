import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension,
    baseBorderRadius,
    baseColor,
    baseIcon,
} from '../../styles/base';
import CustomMetaData from '../common/CustomMetaData';
import CustomIcon from '../common/CustomIcon';

const MyClassPlaylistItem = (props) => {

    return (
        <TouchableOpacity
            style={{
                
            }}
            onPress={props.onPress}
            delayPressIn={50}
        >
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <View>
                    <Image
                        source={{uri: props.data.thumbnail}}
                        style={{
                            width: dimension.width * 0.3,
                            height: dimension.width * 0.2,
                            borderRadius: baseBorderRadius.md,
                            backgroundColor: baseColor.two,
                            borderRadius: baseBorderRadius.md
                        }}
                        resizeMode={'cover'}
                    />
                    <View
                        style={{
                            position: 'absolute',
                            right: 0,
                            top: 0,
                            height: '100%',
                            padding: basePadding.sm,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0, 0, 0, 0.7)',
                            borderTopRightRadius: baseBorderRadius.md,
                            borderBottomRightRadius: baseBorderRadius.md,
                            zIndex: 1
                        }}
                    >
                        <CustomIcon
                            name={'menu'}
                            style={{
                                color: baseColor.one
                            }}
                            size={baseIcon.md}
                        />
                    </View>
                </View>
                <View
                    style={{
                        flex: 1,
                        paddingLeft: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        {props.data.name}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}
                    >
                        <CustomMetaData
                            iconName={'video'}
                            label={props.data.total_video + ' bài học'}
                        />
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default MyClassPlaylistItem;
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    FlatList,
    RefreshControl,
    ScrollView,
    TouchableOpacity,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    myClassReducerUpdate,
    myClassGetPlaylist
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon
} from '../../styles/base';
import MyClassPlaylistItem from './MyClassPlaylistItem';
import CustomIcon from '../common/CustomIcon';

const MyClassScreen = (props) => {

    const [refreshing, setRefreshing] = useState(false);

    /**
     * Methods
     */

    const onPlaylistPress = () => {
        props.navigation.navigate('MyClassPlaylist');
    }

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            setRefreshing(false)
        }, 1000);
        props.myClassReducerUpdate('playlists', null);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.playlists !== null) {
            onRefresh();
        }
    }, [])

    useEffect(() => {
        if (props.playlists === null) {
            props.myClassGetPlaylist(props.accessToken);
        }
    }, [props.playlists])

    /**
     * Render methods
     */

    const renderRefreshControl = () => {
        return (
            <RefreshControl
                style={{
                    zIndex: 999,
                    elevation: 3
                }}
                refreshing={refreshing}
                onRefresh={onRefresh}
                progressViewOffset={props.contentContainerStyle ? props.contentContainerStyle.paddingTop : 0}
            />
        )
    }

    const renderPlaylistItem = ({ item, index }) => {
        return (
            <MyClassPlaylistItem
                data={item}
                onPress={onPlaylistPress}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={renderRefreshControl()}
            >
                <View
                    style={{
                        padding: basePadding.md
                    }}
                >
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            props.navigation.navigate('MyClassSubscribedCoacher');
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.three,
                                fontSize: baseFontSize.md,
                                fontFamily: baseFontFamily.bold,
                                flex: 1
                            }}
                        >
                            Giảng viên đang theo học
                        </Text>
                        <CustomIcon
                            name='arrow-right'
                            size={baseIcon.md}
                            style={{
                                color: baseColor.three
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <View>
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold,
                            padding: basePadding.md,
                        }}
                    >
                        Playlist bài học
                    </Text>
                    <FlatList
                        data={props.playlists}
                        keyExtractor={(item, index) => {
                            return 'playlist-' + index;
                        }}
                        renderItem={renderPlaylistItem}
                        contentContainerStyle={{
                            paddingHorizontal: basePadding.md
                        }}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, myClass }) => {
    return {
        accessToken: auth.accessToken,
        playlists: myClass.playlists
    }
}

export default connect(mapStateToProps, {
    myClassReducerUpdate,
    myClassGetPlaylist
})(MyClassScreen);
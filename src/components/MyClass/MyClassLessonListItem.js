import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension,
    baseBorderRadius,
    baseColor,
} from '../../styles/base';
import CustomMetaData, { CustomMetaDataBreaker } from '../common/CustomMetaData';

const MyClassLessonListItem = (props) => {

    return (
        <TouchableOpacity
            style={{
                
            }}
            onPress={props.onPress}
            delayPressIn={50}
        >
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row'
                }}
            >
                <Image
                    source={{uri: props.data.thumbnail}}
                    style={{
                        width: dimension.width * 0.3,
                        height: dimension.width * 0.2,
                        borderRadius: baseBorderRadius.md,
                        backgroundColor: baseColor.two
                    }}
                    resizeMode={'cover'}
                />
                <View
                    style={{
                        flex: 1,
                        paddingLeft: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                        numberOfLines={2}
                        ellipsizeMode={'tail'}
                    >
                        {props.data.title}
                    </Text>
                    <Text
                        style={{
                            fontSize: baseFontSize.sm,
                            fontFamily: baseFontFamily.regular,
                        }}
                        numberOfLines={2}
                        ellipsizeMode={'tail'}
                    >
                        {props.data.description}
                    </Text>
                </View>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingTop: basePadding.xs,
                    paddingBottom: basePadding.md
                }}
            >
                <CustomMetaData
                    iconName={'video'}
                    label={props.data.total_video}
                />
                <CustomMetaDataBreaker />
                <CustomMetaData
                    iconName={'clock'}
                    label={props.data.total_time}
                />
                <CustomMetaDataBreaker />
                <CustomMetaData
                    iconName={'thumbs-up'}
                    label={props.data.total_like}
                />    
            </View>
        </TouchableOpacity>
    )
}

export default MyClassLessonListItem;
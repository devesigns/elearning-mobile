import React, { useState } from 'react';
import {
    View,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    coacherReducerUpdate,
    myClassReducerUpdate,
    myClassGetSubscribedCoacher
} from '../../actions';
import {
    basePadding,
    baseColor,
    baseMargin
} from '../../styles/base';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import CoacherGridItem from '../Coacher/CoacherGridItem';
import CustomSearchBar from '../common/CustomSearchBar';
import { AchievementPlaceholder } from '../common/Placeholder';

const MyClassCoacherListScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');
    
    /**
     * Methods
     */

    const onRefresh = () => {
        props.myClassReducerUpdate('coachers', null);
    }

    const onSearchSubmit = (query) => {
        if (query !== searchQuery) {
            setSearchQuery(query);
            onRefresh();
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }
    
    /**
     * UseEffect
     */

    /**
     * Render Methods
     */

    if (!mounted) {
        return null;
    }

    const renderGridItem = ({item, index}) => {
        return (
            <CoacherGridItem
                data={item}
                onPress={() => {
                    props.coacherReducerUpdate('coacherForm', {
                        id: item.id
                    });
                    props.navigation.navigate('CoacherProfile');
                }}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.two
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    paddingBottom: basePadding.md,
                    paddingHorizontal: basePadding.md
                }}
            >
                <CustomSearchBar
                    placeholder={'Tìm giảng viên...'}
                    onSubmit={onSearchSubmit}
                />
            </View>
            <CustomFlatListWithFetch
                keyExtractor={(item, index) => {
                    return 'coacher-' + index;
                }}
                data={props.coachers}
                renderItem={renderGridItem}
                numColumns={2}
                perPage={16}
                columnWrapperStyle={{
                    marginBottom: baseMargin.md,
                    justifyContent: 'space-between'
                }}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md
                }}
                onRefresh={onRefresh}
                fetchNextBatch={(page, perPage) => {
                    props.myClassGetSubscribedCoacher(props.accessToken, {
                        page,
                        perPage,
                        searchQuery
                    })
                }}
                renderPlaceholder={() => {
                    return (
                        <AchievementPlaceholder />
                    )
                }}
            />
        </SafeAreaView>
    )
}

MyClassCoacherListScreen.navigationOptions = ({ navigation }) => {
    return {
        title: 'GIẢNG VIÊN CỦA TÔI'
    }
}

const mapStateToProps = ({ auth, myClass }) => {
    return {
        accessToken: auth.accessToken,
        coachers: myClass.coachers
    }
}

export default connect(mapStateToProps, {
    coacherReducerUpdate,
    myClassReducerUpdate,
    myClassGetSubscribedCoacher
})(MyClassCoacherListScreen);
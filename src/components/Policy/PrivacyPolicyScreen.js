import React from 'react';
import {
    WebView
} from 'react-native-webview';

const PrivacyPolicyScreen = (props) => {
    return (
        <WebView
            source={{uri: 'https://tvbh.app/privacy-policy-of-sla/'}}
            style={[props.style, {}]}
        />
    )
}

export default PrivacyPolicyScreen;
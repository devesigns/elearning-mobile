import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    courseReducerUpdate,
    courseGetCourse
} from '../../actions';
import { moderateScale } from '../../utils';
import {
    dimension,
    baseColor,
    baseFontFamily,
    baseFontSize,
    basePadding
} from '../../styles/base';
import CourseArrow from './CourseArrow';

const CourseSlider = (props) => {
    const [arrowHeight, setArrowHeight] = useState(0);

    /**
     * Methods
     */

    const onCourseArrowPress = (item) => {
        props.courseReducerUpdate('courseForm', {
            course: item
        });
        props.navigation.navigate('CourseDetail');
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.courses === null) {
            props.courseGetCourse(props.accessToken);
        }
    }, [props.courses])

    /**
     * Render methods
     */

    if (_.isEmpty(props.courses)) {
        return null;
    }

    return (
        <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={{
                width: (dimension.width / 3) * props.courses.length
            }}
        >
            <View
                style={{
                    flexDirection: 'row',
                    height: arrowHeight
                }}
            >
                {(() => {
                    let height = moderateScale(24);

                    return props.courses.map((item, index) => {
                        return (
                            <View
                                key={index.toString() + new Date().getTime().toString()}
                                style={{
                                    backgroundColor: 'transparent',
                                    position: 'absolute',
                                    zIndex: 999 - index,
                                    elevation: 999 - index,
                                    left: index * (dimension.width / 3) - height,
                                }}
                                onLayout={(event) => {
                                    if (!arrowHeight) {
                                        setArrowHeight(event.nativeEvent.layout.height);
                                    }
                                }}
                            >
                                <CourseArrow
                                    label={item.title}
                                    height={height}
                                    width={dimension.width / 3}
                                    labelStyle={{
                                        fontSize: baseFontSize.xs,
                                        fontFamily: baseFontFamily.bold,
                                        paddingLeft: height,
                                        paddingRight: basePadding.xs
                                    }}
                                    bgColor={(() => {
                                        switch (index % 3) {
                                            case 0: return baseColor.three
                                            case 1: return '#F6C648'
                                            case 2: return '#FFA127'
                                        }
                                    })()}
                                    onPress={() => onCourseArrowPress(item)}
                                />
                            </View>
                        )
                    })
                })()}
            </View>
        </ScrollView>
    );
}

const mapStateToProps = ({ auth, course }) => {
    return {
        accessToken: auth.accessToken,
        courses: course.courses
    }
}

export default connect(mapStateToProps, {
    courseReducerUpdate,
    courseGetCourse
})(withNavigation(CourseSlider));
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseIcon
} from '../../styles/base';
import { CustomMetaDataBreaker } from '../common/CustomMetaData';
import CustomIcon from '../common/CustomIcon';

const CourseLessonListItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                paddingVertical: basePadding.md,
                flexDirection: 'row',
                alignItems: 'center',
                borderBottomColor: baseColor.five,
                borderBottomWidth: props.noBorder ? 0 : 0.5
            }}
            onPress={props.onPress}
            delayPressIn={50}
        >
            <View
                style={{
                    flexDirection: 'column',
                    width: '70%'
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.xl,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    {props.data.title}
                </Text>
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        {props.data.total_video} video
                    </Text>
                    <CustomMetaDataBreaker
                        color={baseColor.four}
                    />
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        {props.data.total_time}
                    </Text>
                </View>
            </View>
            <View
                style={{
                    width: '30%',
                    justifyContent: 'center',
                    alignItems: 'flex-end'
                }}
            >
                <CustomIcon
                    name={'check-circle'}
                    style={{
                        color: props.data.completed ? baseColor.six : baseColor.five
                    }}
                    size={baseIcon.xl}
                />
            </View>
        </TouchableOpacity>
    )
}

export default CourseLessonListItem;
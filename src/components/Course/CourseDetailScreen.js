import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    LayoutAnimation,
    TouchableOpacity,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import {
    courseGetDetail,
    courseGetCourseSummarySingle,
    lessonReducerUpdate,
    lessonGetLesson,
    coacherReducerUpdate
} from '../../actions';
import {
    baseColor,
    basePadding,
    baseFontFamily,
    baseFontSize,
    baseIcon,
    baseMargin
} from '../../styles/base';
import CourseOverview from './CourseOverview';
import CustomPageBreak from '../common/CustomPageBreak';
import CourseLessonListItem from './CourseLessionListItem';
import { ScrollView } from 'react-native-gesture-handler';
import CustomMetaData, { CustomMetaDataBreaker } from '../common/CustomMetaData';
import { moderateScale } from '../../utils';

const CourseDetailScreen = (props) => {
    const [mounted, setMounted] = useState(false);
    const [lessonFetched, setLessonFetched] = useState(false);

    if (!mounted) {
        props.lessonReducerUpdate('lessons', null);
        setMounted(true);
    }

    /**
     * Methods
     */

    const onLessonPress = (item) => {
        props.lessonReducerUpdate('lessonId', item.id);
        props.lessonReducerUpdate('lessonType', 'normal');
        props.lessonReducerUpdate('currentSession', {});
        props.navigation.navigate('Lesson');
    }

    const renderLessonItem = ({item, index}) => {
        return (
            <CourseLessonListItem
                data={item}
                noBorder={index === props.lessons.length - 1}
                onPress={() => onLessonPress(item)}
            />
        )
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.courseForm.course.id) {
            if (props.courseForm.completed_percentage === undefined) {
                props.courseGetCourseSummarySingle(props.accessToken, {
                    courseId: props.courseForm.course.id
                })
            }

            if (_.isEmpty(props.courseForm.course.created_by)) {
                props.courseGetDetail(props.accessToken, {
                    courseId: props.courseForm.course.id
                })
            }
        }
    }, [props.courseForm])

    useEffect(() => {
        if (!_.isEmpty(props.lessons)) {
            // LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        } else {
            if (!lessonFetched) {
                props.lessonGetLesson(props.accessToken, {
                    courseId: props.courseForm.course.id,
                    page: 1,
                    perPage: 32
                })
                setLessonFetched(true);
            }
        }
    }, [props.lessons])

    /**
     * Render methods
     */

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    paddingVertical: basePadding.md
                }}
            >
                <CourseOverview
                    data={props.courseForm.course}
                    willDisplayFull
                />
                {/* Course Meta */}
                <View
                    style={{
                        padding: basePadding.md,
                        paddingTop: 0,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <CustomMetaData
                        iconName={'clock'}
                        label={props.courseForm.course.total_time}
                    />
                    <CustomMetaDataBreaker />
                    <CustomMetaData
                        iconName={'file-text'}
                        label={props.courseForm.course.total_lesson + ' bài'}
                    />
                </View>
                {/* Coacher Info */}
                {
                    !_.isEmpty(props.courseForm.course.created_by) ?
                    <>
                        <CustomPageBreak />
                        <View
                            style={{
                                padding: basePadding.md
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                                onPress={() => {
                                    props.coacherReducerUpdate('coacherForm', {
                                        id: props.courseForm.course.created_by.id
                                    });
                                    props.navigation.navigate('CoacherProfile');
                                }}
                            >
                                <Image
                                    source={{uri: props.courseForm.course.created_by.avatar}}
                                    style={{
                                        height: moderateScale(36),
                                        width: moderateScale(36),
                                        backgroundColor: baseColor.two,
                                        borderRadius: moderateScale(36) / 2
                                    }}
                                />
                                <View
                                    style={{
                                        paddingLeft: basePadding.sm,
                                        flex: 1
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: baseColor.four,
                                            fontSize: baseFontSize.md,
                                            fontFamily: baseFontFamily.bold
                                        }}
                                    >
                                        {props.courseForm.course.created_by.fullname}
                                    </Text>
                                    <Text
                                        style={{
                                            color: baseColor.five,
                                            fontSize: baseFontSize.sm,
                                            fontFamily: baseFontFamily.regular
                                        }}
                                    >
                                        {props.courseForm.course.created_by.job_description ? props.courseForm.course.created_by.job_description : 'Chưa có thông tin công việc'}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </>
                    :
                    null
                }
                <CustomPageBreak />
                <View
                    style={{
                        padding: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold,
                        }}
                    >
                        DANH SÁCH BÀI HỌC
                    </Text>
                    {
                        _.isEmpty(props.lessons) ?
                        <ActivityIndicator
                            color={baseColor.three}
                            size={baseIcon.xxxl}
                            style={{
                                marginTop: baseMargin.md
                            }}
                        />
                        :
                        <FlatList
                            keyExtractor={(item, index) => {
                                return 'lesson-' + item.id;
                            }}
                            data={props.lessons}
                            renderItem={renderLessonItem}
                        />
                    }
                </View>
            </ScrollView>
        </View>
    )
}

const mapStateToProps = ({ auth, course, lesson }) => {
    return {
        accessToken: auth.accessToken,
        courseForm: course.courseForm,
        lessons: lesson.lessons
    }
};

export default connect(mapStateToProps, {
    courseGetDetail,
    courseGetCourseSummarySingle,
    lessonReducerUpdate,
    lessonGetLesson,
    coacherReducerUpdate
})(CourseDetailScreen);
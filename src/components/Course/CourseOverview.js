import React, { useState } from 'react';
import {
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import {

} from '../../actions';
import {
    baseColor,
    dimension,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseMargin
} from '../../styles/base';
import { moderateScale } from '../../utils';
import CourseArrow from './CourseArrow';

const CourseProgressIndicator = (props) => {
    const [layout, setLayout] = useState({
        width: 0,
        height: 0
    });

    return (
        <View
            style={{
                position: 'absolute',
                left: props.percentage * (dimension.width * 0.7 - (props.padding ? props.padding : 0) - moderateScale(32) + baseMargin.xs) / 100 - layout.width / 2,
                top: 0,
                marginTop: baseMargin.sm,
                flexDirection: 'column',
                alignItems: 'center',
            }}
            onLayout={(event) => {
                setLayout(event.nativeEvent.layout);
            }}
        >
            <View
                style={{
                    width: 0,
                    height: 0,
                    borderStyle: 'solid',
                    borderLeftWidth: moderateScale(7),
                    borderRightWidth: moderateScale(7),
                    borderBottomWidth: moderateScale(7),
                    borderLeftColor: 'transparent',
                    borderRightColor: 'transparent',
                    borderBottomColor: baseColor.three
                }}
            />
            <Text
                style={{
                    color: baseColor.three,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.bold
                }}
            >
                {props.percentage}%
            </Text>
        </View>
    )
}

const CourseTrack = (props) => {
    return (
        <View
            style={{
                flex: 1
            }}
        >
            <View
                style={{
                    overflow: 'hidden',
                    height: 3,
                }}
            >
                <View
                    style={{
                        borderWidth: 3,
                        borderStyle: 'dashed',
                        borderColor: baseColor.five,
                        height: 30,
                        borderRadius: 0.5
                    }}
                />
                <View
                    style={{
                        position: 'absolute',
                        left: 0,
                        backgroundColor: baseColor.three,
                        height: 3,
                        width: props.percentage * (dimension.width * 0.7 - (props.padding ? props.padding : 0) - moderateScale(32) + baseMargin.xs) / 100
                    }}
                />
            </View>
            {
                props.percentage === 100 ?
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold,
                        position: 'absolute',
                        right: basePadding.xs,
                        marginTop: baseMargin.sm
                    }}
                >
                    Hoàn thành
                </Text>
                :
                <CourseProgressIndicator
                    percentage={props.percentage}
                    padding={props.padding}
                />
            }
        </View>
    )
}

const CourseOverview = (props) => {
    if (props.data.percentage === null || props.data.percentage === undefined) {
        return null;
    }

    return (
        <View>
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <CourseArrow
                    label={props.data ? props.data.title : 'Lorem Ipsum'}
                    height={moderateScale(32)}
                    width={dimension.width * 0.3}
                    labelStyle={{
                        paddingRight: basePadding.md
                    }}
                />
                <CourseTrack
                    percentage={props.data ? props.data.percentage : 0}
                    padding={props.padding}
                />
            </View>
            <View
                style={{
                    padding: basePadding.md
                }}
            >
                {
                    props.willDisplayFull ?
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            textAlign: 'justify'
                        }}
                    >
                        {
                            props.data ?
                            props.data.description
                            :
                            'No description.'
                        }
                    </Text>
                    :
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            textAlign: 'justify'
                        }}
                        numberOfLines={3}
                        ellipsizeMode={'tail'}
                    >
                        {
                            props.data ?
                            props.data.description
                            :
                            'No description.'
                        }
                    </Text>
                }
            </View>
        </View>
    )
}

export default CourseOverview;
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {

} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
} from '../../styles/base';

const CourseArrow = (props) => {
    if (!(props.label && props.height)) {
        return null;
    }

    return (
        <TouchableOpacity
            style={[
                {
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                    width: props.width + props.height,
                    zIndex: 999
                },
                props.containerStyle
            ]}
            onPress={props.onPress}
            activeOpacity={props.onPress ? 0.8 : 1}
        >
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <View
                    style={{
                        height: props.height * 2,
                        width: props.width,
                        backgroundColor: props.bgColor ? props.bgColor : baseColor.three
                    }}
                />
                <View
                    style={{
                        width: 0,
                        height: 0,
                        borderStyle: 'solid',
                        borderLeftWidth: props.height,
                        borderRightWidth: props.height,
                        borderBottomWidth: props.height,
                        borderLeftColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderBottomColor: props.bgColor ? props.bgColor : baseColor.three,
                        transform: [
                            {
                                rotate: '90deg'
                            },
                            {
                                translateY: props.height / 2
                            }
                        ]
                    }}
                />
            </View>
            <View
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: basePadding.sm
                }}
            >
                <Text
                    style={[
                        {
                            color: baseColor.one,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold,
                            textTransform: 'uppercase',
                            textAlign: 'center'
                        },
                        props.labelStyle
                    ]}
                >
                    {
                        props.label.length >= 35 ?
                        props.label.slice(0, 32) + '...'
                        :
                        props.label
                    }
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default CourseArrow;
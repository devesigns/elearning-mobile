import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    KeyboardAvoidingView,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    Platform,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    authSignIn
} from '../../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon
} from '../../../styles/base';
import {
    validate,
    getValidationMessage,
    isIphoneX,
    moderateScale
} from '../../../utils';
import AuthTextInput from '../Misc/AuthTextInput';
import AuthButton from '../Misc/AuthButton';
import CustomIcon from '../../common/CustomIcon';

const AuthSignInScreen = (props) => {
    const accountForm = props.navigation.getParam('accountForm');

    const [account, setAccount] = useState(!_.isEmpty(accountForm) ? accountForm.account : '');
    const [accountValidation, setAccountValidation] = useState('');
    const [password, setPassword] = useState(!_.isEmpty(accountForm) ? accountForm.password : '');
    const [passwordValidation, setPasswordValidation] = useState('');
    const [passwordVisible, setPasswordVisible] = useState(false);

    const togglePasswordVisible = () => {
        setPasswordVisible(!passwordVisible);
    }

    const resetForm = () => {
        setAccount('');
        setPassword('');
    }

    const resetValidation = () => {
        setAccountValidation('');
        setPasswordValidation('');
    }

    const validateForm = () => {
        let willResetForm = false;
        
        let accountValid = validate.validateAccount(account);
        if (accountValid != 'phone' && accountValid != 'email') {
            willResetForm = true;
            setAccountValidation(getValidationMessage(accountValid));
        }

        let passwordValid = validate.validatePassword(password);
        if (passwordValid != true) {
            willResetForm = true;
            setPasswordValidation(getValidationMessage(passwordValid));
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return true;
    }

    useEffect(() => {
        if (!_.isEmpty(props.signInValidation)) {
            for (let i = 0; i < props.signInValidation.length; i++) {
                if (props.signInValidation[i].field === 'username') {
                    setAccountValidation(props.signInValidation[i].message);
                } else {
                    setPasswordValidation(props.signInValidation[i].message);
                }
            }
        }
    }, [props.signInValidation]);

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1,
                    padding: basePadding.md
                }}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.four,
                                fontSize: baseFontSize.xxl,
                                fontFamily: baseFontFamily.bold,
                                paddingBottom: basePadding.md
                            }}
                        >
                            Mừng quay trở lại!
                        </Text>
                        <View
                            style={{
                                paddingBottom: basePadding.md
                            }}
                        >
                            {
                                props.signInValidation && (typeof props.signInValidation) != 'object' ?
                                <Text
                                    style={{
                                        color: 'red',
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.regular,
                                        paddingBottom: basePadding.xs
                                    }}
                                >
                                    {props.signInValidation}
                                </Text>
                                :
                                null
                            }
                            <View
                                style={{
                                    flexDirection: 'row'
                                }}
                            >
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.lg,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    SỐ ĐIỆN THOẠI&nbsp;
                                </Text>
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.lg,
                                        fontFamily: baseFontFamily.regular
                                    }}
                                >
                                    hoặc
                                </Text>
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.lg,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    &nbsp;EMAIL
                                </Text>
                            </View>
                            <AuthTextInput
                                value={account}
                                onChangeText={(value) => setAccount(value)}
                                fieldValidation={accountValidation}
                            />
                        </View>
                        <View
                            style={{
                                paddingBottom: basePadding.md
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.three,
                                    fontSize: baseFontSize.lg,
                                    fontFamily: baseFontFamily.bold
                                }}
                            >
                                MẬT KHẨU
                            </Text>
                            <AuthTextInput
                                value={password}
                                onChangeText={(value) => setPassword(value)}
                                fieldValidation={passwordValidation}
                                secureTextEntry={!passwordVisible}
                                renderRightIcon={() => {
                                    return (
                                        <TouchableOpacity
                                            style={{
                                                position: 'absolute',
                                                right: 0,
                                                zIndex: 999,
                                            }}
                                            onPress={togglePasswordVisible}
                                        >
                                            <CustomIcon
                                                name={passwordVisible ? 'eye-off' : 'eye'}
                                                size={baseIcon.md}
                                                style={{
                                                    color: baseColor.three,
                                                }}
                                            />
                                        </TouchableOpacity>
                                    )
                                }}
                            />
                        </View>
                        <TouchableOpacity
                            onPress={() => {
                                props.navigation.navigate('AuthForgetPassword');
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.three,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.regular,
                                }}
                            >
                                Quên mật khẩu?
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <AuthButton
                        style={{
                            alignSelf: 'center',
                            marginTop: 'auto'
                        }}
                        onPress={() => {
                            resetValidation();

                            if (!validateForm()) {
                                return;
                            }

                            props.authSignIn({
                                account,
                                password
                            })
                        }}
                    >
                        ĐĂNG NHẬP
                    </AuthButton>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ helper }) => {
    return {
        signInValidation: helper.authValidation.signIn
    }
}

export default connect(mapStateToProps, {
    authSignIn
})(AuthSignInScreen);
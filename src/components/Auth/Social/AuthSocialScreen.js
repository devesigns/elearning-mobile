import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import {
    authReducerUpdate
} from '../../../actions';

const AuthZaloScreen = (props) => {
    let token = props.navigation.getParam('token');
    token = token.replace(/([#_=])/g, '');
    console.log('Social token', token);

    const saveToken = async (token) => {
        await AsyncStorage.setItem('sla-token', JSON.stringify(token));
    }

    if (token) {
        saveToken(token);
        props.authReducerUpdate('accessToken', token);
    }    

    return null;
}

const mapStateToProps = null;

export default connect(mapStateToProps, {
    authReducerUpdate
})(AuthZaloScreen);
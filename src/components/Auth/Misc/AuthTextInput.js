import React from 'react';
import {
    View,
    TextInput,
    Text
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding
} from '../../../styles/base';

const AuthTextInput = (props) => {
    return (
        <View>
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <TextInput
                    value={props.value}
                    onChangeText={props.onChangeText}
                    style={[
                        {
                            color: baseColor.four,
                            width: '100%',
                            borderBottomColor: baseColor.three,
                            borderBottomWidth: 0.5,
                            fontSize: baseFontSize.xxl,
                            fontFamily: baseFontFamily.regular,
                            paddingVertical: basePadding.xs,
                            paddingLeft: 0,
                            paddingRight: props.renderRightIcon ? basePadding.xxxl : 0
                        },
                        props.style
                    ]}
                    underlineColorAndroid={'transparent'}
                    secureTextEntry={props.secureTextEntry}
                    multiline={props.multiline}
                    maxLength={props.maxLength}
                />
                {
                    props.renderRightIcon ?
                    props.renderRightIcon()
                    :
                    null
                }
            </View>
            {
                props.fieldValidation != ''?
                <Text
                    style={{
                        color: 'red',
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingVertical: basePadding.xs
                    }}
                >
                    {props.fieldValidation}
                </Text>
                :
                null
            }
        </View>
    )
}

export default AuthTextInput;
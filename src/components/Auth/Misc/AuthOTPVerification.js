import React, { useState, useRef, useEffect } from 'react';
import {
    View,
    ScrollView,
    Text,
} from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { connect } from 'react-redux';
import {
    helperReducerUpdate
} from '../../../actions';
import {
    basePadding,
    baseColor,
    baseFontFamily,
    baseFontSize,
    dimension
} from '../../../styles/base';
import {
    validate,
    getValidationMessage
} from '../../../utils';

const AuthOTPVerification = (props) => {
    const fontSize = (dimension.width - basePadding.md * 7) / (6 * 1.4);

    const [code, setCode] = useState('');
    const [codeValidation, setCodeValidation] = useState('');

    const resetForm = () => {
        setCode('');
    }

    const resetValidation = () => {
        setCodeValidation('');
        props.helperReducerUpdate('authValidation', {
            otp: null
        })
    }

    const validateForm = () => {
        let willResetForm = false;
        
        let codeValidation = validate.validateCode(code);
        if (codeValidation != true) {
            willResetForm = true;
            setCodeValidation(getValidationMessage(codeValidation));
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return true;
    }

    useEffect(() => {
        if (props.otpValidation) {
            resetForm();
        }
    }, [props.otpValidation])

    props.setOnSubmit(() => {
        resetValidation();

        if (validateForm()) {
            return {
                code
            }
        }

        return false;
    })

    return (
        <View
            style={{
                width: '100%',
                justifyContent: 'center'
            }}
        >
            <View
                style={{
                    flexDirection: 'row',
                    width: fontSize * 6 * 1.4 + basePadding.md * 5,
                    justifyContent: 'space-between',
                }}
            >
                <OTPInputView
                    code={code}
                    onCodeChanged={(value) => {
                        setCode(value);
                    }}
                    style={{
                        width: '100%',
                        height: fontSize * 1.5
                    }}
                    pinCount={6}
                    autoFocusOnLoad
                    codeInputFieldStyle={{
                        paddingVertical: 0,
                        paddingBottom: basePadding.xs,
                        width: fontSize * 1.4,
                        borderWidth: 0,
                        borderBottomWidth: 1,
                        borderBottomColor: baseColor.three,
                        justifyContent: 'center',
                        alignItems: 'center',
                        fontSize: fontSize,
                        fontFamily: baseFontFamily.bold,
                        color: baseColor.three
                    }}
                />
            </View>
            {
                codeValidation ?
                <Text
                    style={{
                        color: 'red',
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingTop: basePadding.xs
                    }}
                >
                    {codeValidation}
                </Text>
                :
                null
            }
            {
                props.otpValidation ?
                <Text
                    style={{
                        color: 'red',
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingTop: basePadding.xs
                    }}
                >
                    {props.otpValidation}
                </Text>
                :
                null
            }
        </View>
    )
}

const mapStateToProps = ({ helper }) => {
    return {
        otpValidation: helper.authValidation.otp
    }
}

export default connect(mapStateToProps, {
    helperReducerUpdate
})(AuthOTPVerification);
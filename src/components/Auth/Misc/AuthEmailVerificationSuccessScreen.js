import { connect } from 'react-redux';
import {
    authReducerUpdate,
    helperToggleLoading
} from '../../../actions';

const AuthEmailVerificationSuccessScreen = (props) => {
    props.helperToggleLoading('globalLoading');

    setTimeout(() => {
        props.authReducerUpdate('verifyEmail', 'success');
        props.helperToggleLoading('globalLoading');
        props.navigation.goBack();
    }, 1000);

    return null;
}

const mapStateToProps = null;

export default connect(mapStateToProps, {
    authReducerUpdate,
    helperToggleLoading
})(AuthEmailVerificationSuccessScreen);
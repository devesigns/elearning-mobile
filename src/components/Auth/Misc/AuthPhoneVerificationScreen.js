import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    authSendOTP,
    authVerifyOTP
} from '../../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension
} from '../../../styles/base';
import { moderateScale, isIphoneX } from '../../../utils';
import AuthOTPVerification from '../Misc/AuthOTPVerification';
import AuthButton from '../Misc/AuthButton';

const AuthPhoneVerificationScreen = (props) => {
    const [resendCD, setResendCD] = useState(30);
    const [cdActive, setCDActive] = useState(false);
    const [intervalId, setIntervalId] = useState(null);

    let onSubmitCode = null;

    if (!cdActive) {
        setCDActive(true);
    }

    const onSubmit = () => {
        let { code } = onSubmitCode();

        if (code) {
            props.authVerifyOTP({
                account: props.navigation.getParam('account'),
                code: code,
            });
        }
    }

    const startInterval = () => {
        clearInterval(intervalId);
        setResendCD(30);

        setIntervalId(setInterval(() => {
            setResendCD(_resendCD => {
                if (_resendCD > 0) {
                    return _resendCD - 1;
                }

                return _resendCD;
            });
        }, 1000));
    }

    useEffect(() => {
        if (cdActive) {
            startInterval();
        }

        return () => clearInterval(intervalId);
    }, [cdActive])

    useEffect(() => {
        if (props.verifyOTP === 'success') {
            props.navigation.goBack();
        }

        return () => clearInterval(intervalId);
    }, [props.verifyOTP])

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1
                }}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                        }}
                    >
                        <View
                            style={{
                                padding: basePadding.md,
                                width: dimension.width
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.xxl,
                                    fontFamily: baseFontFamily.bold,
                                    paddingBottom: basePadding.md
                                }}
                            >
                                Nhập mã xác nhận
                            </Text>
                            <View
                                style={{
                                    paddingBottom: basePadding.md
                                }}
                            >
                                <AuthOTPVerification
                                    active={props.active}
                                    setOnSubmit={(onSubmit) => {
                                        onSubmitCode = onSubmit;
                                    }}
                                />
                            </View>
                            <View
                                style={{
                                    flexDirection: 'row'
                                }}
                            >
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.regular,
                                    }}
                                >
                                    Không nhận được mã?&nbsp;
                                </Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (resendCD === 0) {
                                            props.authSendOTP({
                                                account: props.navigation.getParam('account'),
                                            });

                                            startInterval();
                                        } else {
                                            alert('Xin hãy chờ 30s!')
                                        }
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: baseColor.three,
                                            fontSize: baseFontSize.md,
                                            fontFamily: baseFontFamily.bold,
                                        }}
                                    >
                                        Gửi lại ({resendCD}s)
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <AuthButton
                        style={{
                            alignSelf: 'center',
                            marginTop: 'auto'
                        }}
                        onPress={onSubmit}
                    >
                        XÁC NHẬN
                    </AuthButton>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        verifyOTP: auth.verifyOTP
    }
};

export default connect(mapStateToProps, {
    authSendOTP,
    authVerifyOTP
})(AuthPhoneVerificationScreen);
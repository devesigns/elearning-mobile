import React, { useState } from 'react';
import {
    Text,
    TouchableOpacity,
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    dimension,
    baseMargin
} from '../../../styles/base';

const AuthButton = (props) => {
    return (
        <TouchableOpacity
            style={[
                {
                    backgroundColor: baseColor.three,
                    padding: basePadding.sm,
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: dimension.width * 0.7,
                    marginBottom: baseMargin.sm,
                    borderRadius: (basePadding.md * 2 + baseFontSize.md * 1.4) / 2
                },
                props.style
            ]}
            onPress={props.onPress}
        >
            <Text
                style={{
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.bold,
                    color: baseColor.one,
                }}
            >
                {props.children}
            </Text>
        </TouchableOpacity>
    )
}

export default AuthButton;
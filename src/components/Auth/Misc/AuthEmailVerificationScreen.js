import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    SafeAreaView
} from 'react-native';
import launchMailApp from 'react-native-mail-launcher';
import { connect } from 'react-redux';
import {
    authSendEmail
} from '../../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
} from '../../../styles/base';
import AuthButton from './AuthButton';

const AuthEmailVerificationScreen = (props) => {
    const [resendCD, setResendCD] = useState(30);
    const [cdActive, setCDActive] = useState(false);
    const [intervalId, setIntervalId] = useState(null);

    // if (!cdActive) {
    //     setCDActive(true);
    // }

    // const startInterval = () => {
    //     clearInterval(intervalId);
    //     setResendCD(30);

    //     setIntervalId(setInterval(() => {
    //         setResendCD(_resendCD => {
    //             if (_resendCD > 0) {
    //                 return _resendCD - 1;
    //             }

    //             return _resendCD;
    //         });
    //     }, 1000));
    // }

    /**
     * UseEffect
     */

    // useEffect(() => {
    //     if (cdActive) {
    //         startInterval();
    //     }

    //     return () => clearInterval(intervalId);
    // }, [cdActive])

    useEffect(() => {
        if (props.verifyEmail) {
            props.navigation.goBack();
        }
    }, [props.verifyEmail]);

    return (
        <SafeAreaView
            style={{
                flex: 1
            }}
        >
            <View
                style={{
                    flex: 1,
                    padding: basePadding.md,
                    backgroundColor: baseColor.one,
                    justifyContent: 'center',
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md
                    }}
                >
                    Xác nhận Email
                </Text>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                    }}
                >
                    Chúng tôi đã gửi cho bạn một email để xác nhận. Hãy kiểm tra hòm thư của bạn và làm theo hướng dẫn!
                </Text>
                <View
                    style={{
                        paddingTop: basePadding.md,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <AuthButton
                        onPress={() => launchMailApp()}
                    >
                        MỞ HÒM THƯ
                    </AuthButton>
                </View>
            </View>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        verifyEmail: auth.verifyEmail
    }
};

export default connect(mapStateToProps, {
    authSendEmail
})(AuthEmailVerificationScreen);
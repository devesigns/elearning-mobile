import React from 'react';
import {
    View,
    Text,
    SafeAreaView
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
} from '../../../styles/base';
import AuthButton from '../Misc/AuthButton';

const AuthForgetPasswordChangePasswordSuccessScreen = (props) => {
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one,
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    padding: basePadding.md
                }}
            >
                 <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md
                    }}
                >
                    Đổi mật khẩu thành công
                </Text>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                    }}
                >
                    Giờ bạn đã có thể đăng nhập với mật khẩu mới của mình!
                </Text>
            </View>
            <AuthButton
                style={{
                    alignSelf: 'center',
                    marginTop: 'auto'
                }}
                onPress={() => {
                    props.navigation.navigate('AuthSignIn', {
                        accountForm: props.navigation.getParam('accountForm')
                    });
                }}
            >
                ĐĂNG NHẬP
            </AuthButton>
        </SafeAreaView>
    )
}

export default AuthForgetPasswordChangePasswordSuccessScreen;
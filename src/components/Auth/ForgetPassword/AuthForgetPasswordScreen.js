import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    LayoutAnimation,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView
} from 'react-native';
import {
    NavigationActions,
    StackActions,
    withNavigationFocus
} from 'react-navigation';
import { connect } from 'react-redux';
import {
    authReducerUpdate,
    authSendOTP,
    authSendEmail,
    authResetPassword,
    helperReducerUpdate
} from '../../../actions';
import {
    baseColor,
    dimension
} from '../../../styles/base';
import { moderateScale, isIphoneX } from '../../../utils';
import AuthButton from '../Misc/AuthButton';
import AuthForgetPasswordBasicInfo from './AuthForgetPasswordBasicInfo';
import AuthForgetPasswordPasswordInfo from './AuthForgetPasswordPasswordInfo';

const AuthSignUpScreen = (props) => {
    // Step 0: Enter Basic Info
    // Step 1: Case of Phone, verify OTP
    // Step 2: Case of Email, wait for email verification 
    // Step 3: Enter Password, call change password API
    // Step 4: If succcess, move to success screen

    const [mounted, setMounted] = useState(false);
    const [formData, setFormData] = useState(null);
    const [step, setStep] = useState(0);

    if (!mounted) {
        props.authReducerUpdate('sendOTP', null);
        props.authReducerUpdate('verifyOTP', null);
        props.authReducerUpdate('sendEmail', null);
        props.authReducerUpdate('verifyEmail', null);
        props.authReducerUpdate('resetPassword', null);
        props.helperReducerUpdate('authValidation', {
            signIn: null,
            signUp: null,
            forgetPassword: null,
            otp: null
        })
        setMounted(true);
    }
    
    let onSubmitStep0 = null;
    let onSubmitStep3 = null;

    /**
     * Methods
     */

    const onChangePasswordSuccess = () => {
        props.navigation.dispatch(
            StackActions.reset({
                index: 1,
                key: null,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'AuthMain'
                    }),
                    NavigationActions.navigate({
                        routeName: 'AuthForgetPasswordChangePasswordSuccess',
                        params: {
                            accountForm: formData
                        }
                    })
                ],
            })
        )
    }

    const onSubmit = () => {
        // Submit basic info, depending on whether it's Email or Phone
        if (step === 0) {
            let data = onSubmitStep0();

            if (data) {
                let newFormData = {
                    ...data
                };

                setFormData(newFormData);

                // If it's email, skip step 1, move to step 2
                if (data.isEmail) {
                    props.authSendEmail({
                        account: data.account,
                        type: 'reset_password'
                    });
                } else {
                    props.authSendOTP({
                        account: data.account,
                        type: 'reset_password'
                    });
                }
            }
        }

        if (step === 3) {
            let data = onSubmitStep3();

            if (data) {
                let newFormData = {
                    ...formData,
                    ...data
                };

                setFormData(newFormData);

                props.authResetPassword(newFormData);
            }
        }
    }

    /**
     * UseEffect
     */

    // Upon receiving sendOTP success flag, move to step 1
    // Navigate to the Phone Verification Screen
    useEffect(() => {
        if (props.sendOTP === 'success') {
            props.authReducerUpdate('sendOTP', null);
            setStep(1);
            props.navigation.navigate('AuthPhoneVerification', {
                account: formData.account,
                hideBackButton: true
            });
        }
    }, [props.sendOTP])

    // Upon receiving verifyOTP success flag, move to step 3
    // Call anim -> password input screen
    useEffect(() => {
        if (props.verifyOTP === 'success') {
            props.authReducerUpdate('verifyOTP', null);
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            setStep(3);
        }
    }, [props.verifyOTP])

    // Upon receiving sendEmail success flag, move to step 2
    // Navigate to the Email Verification Screen
    useEffect(() => {
        if (props.sendEmail === 'success') {
            props.authReducerUpdate('sendEmail', null);
            setStep(2);
            props.navigation.navigate('AuthEmailVerification', {
                account: formData.account,
                hideBackButton: true
            });
        }
    }, [props.sendEmail])

    // Upon receiving verifyEmail success flag, move to step 3
    // Call anim -> password input screen
    useEffect(() => {
        if (props.verifyEmail === 'success') {
            props.authReducerUpdate('verifyEmail', null);
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            setStep(3);
        }
    }, [props.verifyEmail])

    // Upon receiving resetPassword success flag
    // Complete
    useEffect(() => {
        if (props.resetPassword === 'success') {
            props.authReducerUpdate('resetPassword', null);
            props.authReducerUpdate('accountForm', formData);

            onChangePasswordSuccess();
        }
    }, [props.resetPassword])

    // If user just back from the OTP/email verification
    // Move back to step 0
    useEffect(() => {
        if (props.isFocused) {
            if (step === 1) {
                setStep(0);
            }
        }
    }, [props.isFocused])

    /**
     * Render methods
     */

    if (!mounted) {
        return null;
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1
                }}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <View
                            style={[
                                {
                                    flexDirection: 'row',
                                    width: dimension.width * 2,
                                    alignItems: 'center',
                                },
                                step === 0 ?
                                {
                                    transform: [
                                        {
                                            translateX: 0
                                        }
                                    ]
                                }
                                :
                                null,
                                step === 3 ?
                                {
                                    transform: [
                                        {
                                            translateX: - dimension.width
                                        }
                                    ]
                                }
                                :
                                null
                            ]}
                        >
                            <AuthForgetPasswordBasicInfo
                                setOnSubmit={(onSubmit) => {
                                    onSubmitStep0 = onSubmit;
                                }}
                            />
                            <AuthForgetPasswordPasswordInfo
                                setOnSubmit={(onSubmit) => {
                                    onSubmitStep3 = onSubmit;
                                }}
                            />
                        </View>
                    </View>
                    <AuthButton
                        style={{
                            alignSelf: 'center',
                            marginTop: 'auto'
                        }}
                        onPress={onSubmit}
                    >
                        TIẾP TỤC
                    </AuthButton>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        sendOTP: auth.sendOTP,
        verifyOTP: auth.verifyOTP,
        sendEmail: auth.sendEmail,
        verifyEmail: auth.verifyEmail,
        resetPassword: auth.resetPassword
    }
}

export default connect(mapStateToProps, {
    authReducerUpdate,
    authSendOTP,
    authSendEmail,
    authResetPassword,
    helperReducerUpdate
})(withNavigationFocus(AuthSignUpScreen));
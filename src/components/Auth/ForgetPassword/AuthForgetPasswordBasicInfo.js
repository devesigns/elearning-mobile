import React, { useState } from 'react';
import {
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension
} from '../../../styles/base';
import {
    validate,
    getValidationMessage
} from '../../../utils';
import AuthTextInput from '../Misc/AuthTextInput';

const AuthForgetPasswordBasicInfo = (props) => {
    const [account, setAccount] = useState('dev@devesigns.com');
    const [accountValidation, setAccountValidation] = useState('');

    const resetForm = () => {
        setAccount('');
    }

    const resetValidation = () => {
        setAccountValidation('');
    }

    const validateForm = () => {
        let willResetForm = false;
        let isEmail = false;

        let accountValid = validate.validateAccount(account);
        if (accountValid != 'phone' && accountValid != 'email') {
            willResetForm = true;
            setAccountValidation(getValidationMessage(accountValid));
        } else {
            if (accountValid === 'email') {
                isEmail = true;
            }
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return {
            valid: true,
            isEmail
        };
    }

    props.setOnSubmit(() => {
        resetValidation();
        
        let validRes = validateForm();
        if (validRes.valid) {
            return {
                account,
                isEmail: validRes.isEmail
            }
        }

        return false;
    })

    return (
        <View
            style={{
                padding: basePadding.md,
                width: dimension.width
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.xxl,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                Xác nhận tài khoản
            </Text>
            {
                props.forgetPasswordValidation && (typeof props.forgetPasswordValidation) != 'object' ?
                <Text
                    style={{
                        color: 'red',
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingBottom: basePadding.xs
                    }}
                >
                    {props.forgetPasswordValidation}
                </Text>
                :
                null
            }
            <View
                style={{
                    paddingBottom: basePadding.md
                }}
            >
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        SỐ ĐIỆN THOẠI&nbsp;
                    </Text>
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        hoặc
                    </Text>
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        &nbsp;EMAIL
                    </Text>
                </View>
                <AuthTextInput
                    value={account}
                    onChangeText={(value) => setAccount(account => value)}
                    fieldValidation={accountValidation}
                />
            </View>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                }}
            >
                Mã xác nhận sẽ được gửi qua tin nhắn đến số điện thoại hoặc email mà bạn cung cấp.
            </Text>
        </View>
    )
}

const mapStateToProps = ({ helper }) => {
    return {
        forgetPasswordValidation: helper.authValidation.forgetPassword
    }
}

export default connect(mapStateToProps, {

})(AuthForgetPasswordBasicInfo);
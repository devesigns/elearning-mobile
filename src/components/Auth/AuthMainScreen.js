import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    authSignInWithFacebook,
    AuthSignInWithZalo
} from '../../actions';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    dimension,
    baseMargin
} from '../../styles/base';
import { moderateScale } from '../../utils';
import AuthButton from './Misc/AuthButton';

const AuthMainScreen = (props) => {
    const [hatOffset, setHatOffset] = useState(0);

    useEffect(() => {
        if (!_.isEmpty(props.accessToken) && props.accessToken != 'none') {
            props.navigation.navigate('Dashboard')
        }
    }, [props.accessToken])

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                    flexGrow: 1
                }}
            >
                <Text
                    style={{
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        color: baseColor.three,
                        position: 'absolute',
                        top: basePadding.md,
                        alignSelf: 'center'
                    }}
                >
                    SLA
                </Text>
                <View
                    style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: - baseMargin.md
                    }}
                >
                    <Image
                        source={require('../../assets/img/auth-main-illustration-bg.png')}
                        style={{
                            width: dimension.width * 0.7,
                            height: dimension.width * 0.7,
                            alignSelf: 'flex-end'
                        }}
                        resizeMode={'contain'}
                    />
                    <Image
                        source={require('../../assets/img/auth-main-illustration.png')}
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'center',
                            width: dimension.width * 0.7,
                            height: dimension.width * 0.5,
                            zIndex: 1
                        }}
                        resizeMode={'contain'}
                    />
                </View>
                <View
                    style={{
                        paddingHorizontal: basePadding.md,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 'auto'
                    }}
                >
                    <Image
                        source={require('../../assets/img/hat.png')}
                        style={{
                            height: moderateScale(36),
                            width: moderateScale(36),
                            position: 'absolute',
                            top: 0,
                            left: hatOffset - (moderateScale(36) + basePadding.xs)
                        }}
                    />
                    <Text
                        style={{
                            fontSize: baseFontSize.xxl,
                            fontFamily: baseFontFamily.bold,
                            color: baseColor.three,
                            textAlign: 'center'
                        }}
                        onLayout={(event) => {
                            setHatOffset(event.nativeEvent.layout.x);
                        }}
                    >
                        HỌC VIỆN ONLINE
                    </Text>
                    <Text
                        style={{
                            fontSize: baseFontSize.xxl,
                            fontFamily: baseFontFamily.bold,
                            color: baseColor.three,
                            textAlign: 'center'
                        }}
                    >
                        Nghề bảo hiểm nhân thọ
                    </Text>
                    <Text
                        style={{
                            paddingTop: basePadding.xs,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            color: baseColor.four,
                            textAlign: 'center'
                        }}
                    >
                        Kho kiến thức thực tế, video tình huống bán hàng, kỹ năng tuyển dụng, tư vấn chủ động, xử lý từ chối và chốt hợp đồng.
                    </Text>
                </View>
                <View
                    style={{
                        marginTop: 'auto',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <AuthButton
                        style={{
                            backgroundColor: baseColor.three
                        }}
                        onPress={() => {
                            props.navigation.navigate('AuthSignUp');
                        }}
                    >
                        ĐĂNG KÝ
                    </AuthButton>
                    <AuthButton
                        style={{
                            backgroundColor: '#3D5BD9'
                        }}
                        onPress={() => {
                            props.authSignInWithFacebook();
                        }}
                    >
                        ĐĂNG NHẬP BẰNG FACEBOOK
                    </AuthButton>
                    <AuthButton
                        style={{
                            backgroundColor: '#009DFF',
                            marginBottom: 0
                        }}
                        onPress={() => {
                            props.AuthSignInWithZalo();
                        }}
                    >
                        ĐĂNG NHẬP BẰNG ZALO
                    </AuthButton>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                paddingVertical: basePadding.md
                            }}
                            onPress={() => {
                                props.navigation.navigate('AuthSignIn');
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.three,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.regular
                                }}
                            >
                                Đã có tài khoản?&nbsp;
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    Đăng nhập
                                </Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        accessToken: auth.accessToken
    }
};

export default connect(mapStateToProps, {
    authSignInWithFacebook,
    AuthSignInWithZalo
})(AuthMainScreen);
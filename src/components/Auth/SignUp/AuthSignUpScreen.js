import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    LayoutAnimation,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView
} from 'react-native';
import {
    NavigationActions,
    StackActions,
    withNavigationFocus
} from 'react-navigation';
import { connect } from 'react-redux';
import {
    authReducerUpdate,
    authSendOTP,
    authSignUp,
    helperReducerUpdate
} from '../../../actions';
import {
    baseColor,
    dimension
} from '../../../styles/base';
import { moderateScale, isIphoneX } from '../../../utils';
import AuthButton from '../Misc/AuthButton';
import AuthSignUpBasicInfo from './AuthSignUpBasicInfo';
import AuthSignUpPasswordInfo from './AuthSignUpPasswordInfo';

const AuthSignUpScreen = (props) => {
    // Step 0: Enter Basic Info
    // Step 1: Case of Phone, verify OTP
    // Step 2: Enter Password, call SignUp API
    // Step 3: Case of Email, waiting for deeplink verification
    // Step 4: SignUp Success

    const [mounted, setMounted] = useState(false);
    const [formData, setFormData] = useState(null);
    const [step, setStep] = useState(0);

    if (!mounted) {
        props.authReducerUpdate('sendOTP', null);
        props.authReducerUpdate('verifyOTP', null);
        props.authReducerUpdate('register', null);
        props.helperReducerUpdate('authValidation', {
            signIn: null,
            signUp: null,
            forgetPassword: null,
            otp: null
        })
        setMounted(true);
    }
    
    let onSubmitStep0 = null;
    let onSubmitStep2 = null;

    /**
     * Methods
     */

    const onSignUpSuccess = () => {
        props.navigation.dispatch(
            StackActions.reset({
                index: 1,
                key: null,
                actions: [
                    NavigationActions.navigate({
                        routeName: 'AuthMain'
                    }),
                    NavigationActions.navigate({
                        routeName: 'AuthSignUpSuccess',
                        params: {
                            accountForm: formData
                        }
                    })
                ],
            })
        )
    }

    const onSubmit = () => {
        // Submit basic info, depending on whether it's Email or Phone
        if (step === 0) {
            let data = onSubmitStep0();

            if (data) {
                let newFormData = {
                    ...data
                };

                setFormData(newFormData);

                // If it's email, we skip step 1 (verify OTP)
                if (data.isEmail) {
                    setStep(2);
                } else {
                    console.log(data);
                    props.authSendOTP({
                        account: data.account,
                        type: 'registration'
                    });
                }
            }
        }

        // Call sign up API
        if (step === 2) {
            let data = onSubmitStep2();
            
            if (data) {
                let newFormData = {
                    ...formData,
                    ...data
                };

                setFormData(newFormData);

                props.authSignUp(newFormData);
            }
        }
    }

    /**
     * UseEffect
     */

    // Upon receiving sendOTP success flag, move to step 1
    // Navigate to the Phone Verification Screen
    useEffect(() => {
        if (props.sendOTP === 'success') {
            props.authReducerUpdate('sendOTP', null);
            setStep(1);
            props.navigation.navigate('AuthPhoneVerification', {
                account: formData.account,
                hideBackButton: true
            });
        }
    }, [props.sendOTP])

    // Upon receiving verifyOTP success flag, move to step 2
    // Call anim -> password input screen
    useEffect(() => {
        if (props.verifyOTP === 'success') {
            props.authReducerUpdate('verifyOTP', null);
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            setStep(2);
        }
    }, [props.verifyOTP])

    // Upon receiving register success flag, move to step 3
    useEffect(() => {
        if (props.register === 'success') {
            props.authReducerUpdate('register', null);
            props.authReducerUpdate('accountForm', formData);
            setStep(3);
            // If user is signing up with email, navigate to Email Verification Screen
            // else navigate to Sign Up Success Screen
            if (formData.isEmail) {
                props.navigation.navigate('AuthEmailVerification', {
                    hideBackButton: true
                });
            } else {
                onSignUpSuccess();
            }
        }
    }, [props.register])

    // Upon receiving verifyEmail success flag, call onSignUpSuccess
    useEffect(() => {
        if (props.verifyEmail === 'success') {
            props.authReducerUpdate('verifyEmail', null);
            onSignUpSuccess();
        }
    }, [props.verifyEmail])

    // ---
    useEffect(() => {
        if (!_.isEmpty(props.signUpValidation) && (typeof props.signUpValidation) != 'object') {
            let code = props.signUpValidation.split(/[\[\]]/g)[1];
            
            if (code === '1207') {
                setStep(0);
            }
        }
    }, [props.signUpValidation])

    // If user just back from the OTP/email verification
    // Move back to step 0
    useEffect(() => {
        if (props.isFocused) {
            if (step === 1) {
                setStep(0);
            }
        }
    }, [props.isFocused])

    /**
     * Render Methods
     */

    if (!mounted) {
        return null;
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1
                }}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'center'
                        }}
                    >
                        <View
                            style={[
                                {
                                    flexDirection: 'row',
                                    width: dimension.width * 2,
                                    alignItems: 'center',
                                },
                                step === 0 ?
                                {
                                    transform: [
                                        {
                                            translateX: 0
                                        }
                                    ]
                                }
                                :
                                null,
                                step === 2 ?
                                {
                                    transform: [
                                        {
                                            translateX: - dimension.width
                                        }
                                    ]
                                }
                                :
                                null
                            ]}
                        >
                            <AuthSignUpBasicInfo
                                setOnSubmit={(onSubmit) => {
                                    onSubmitStep0 = onSubmit;
                                }}
                            />
                            <AuthSignUpPasswordInfo
                                setOnSubmit={(onSubmit) => {
                                    onSubmitStep2 = onSubmit;
                                }}
                            />
                        </View>
                    </View>
                    <AuthButton
                        style={{
                            alignSelf: 'center',
                            marginTop: 'auto'
                        }}
                        onPress={onSubmit}
                    >
                        TIẾP TỤC
                    </AuthButton>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, helper }) => {
    return {
        sendOTP: auth.sendOTP,
        verifyOTP: auth.verifyOTP,
        verifyEmail: auth.verifyEmail,
        register: auth.register,
        signUpValidation: helper.authValidation.signUp
    }
}

export default connect(mapStateToProps, {
    authReducerUpdate,
    authSendOTP,
    authSignUp,
    helperReducerUpdate
})(withNavigationFocus(AuthSignUpScreen));
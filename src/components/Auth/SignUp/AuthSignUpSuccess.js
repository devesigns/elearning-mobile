import React, { useState } from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension
} from '../../../styles/base';
import AuthButton from '../Misc/AuthButton';

const AuthSignUpSuccessScreen = (props) => {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: baseColor.one,
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    flexDirection: 'column',
                    padding: basePadding.md
                }}
            >
                 <View
                    style={{
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: - basePadding.md
                    }}
                >
                    <Image
                        source={require('../../../assets/img/auth-signup-success-illustration.png')}
                        style={{
                            width: dimension.width * 0.7,
                            height: dimension.width * 0.7,
                            alignSelf: 'center'
                        }}
                        resizeMode={'contain'}
                    />
                </View>
                <Text
                    style={{
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.light,
                        color: baseColor.three,
                        textAlign: 'center'
                    }}
                >
                    CHÀO MỪNG BẠN TRẢI NGHIỆM
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        color: baseColor.three,
                        textAlign: 'center'
                    }}
                >
                    HỌC VIỆN ONLINE
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        color: baseColor.three,
                        textAlign: 'center'
                    }}
                >
                    Nghề Bảo Hiểm Nhân Thọ
                </Text>
                <Text
                    style={{
                        paddingTop: basePadding.xs,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        color: baseColor.four,
                        textAlign: 'center'
                    }}
                >
                    Miễn phí để trải nghiệm kho kiến thức video tình huống bán hàng, tuyển dụng, xử lý từ chối và chốt hợp đồng... 
                    dành riêng cho tư vấn BHNT chuyên nghiệp.
                </Text>
            </View>
            <AuthButton
                style={{
                    alignSelf: 'center',
                    marginTop: 'auto'
                }}
                onPress={() => {
                    props.navigation.navigate('AuthSignIn', {
                        accountForm: props.navigation.getParam('accountForm')
                    });
                }}
            >
                ĐĂNG NHẬP
            </AuthButton>
        </View>
    )
}

export default AuthSignUpSuccessScreen;
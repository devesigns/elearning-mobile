import React, { useState, useEffect } from 'react';
import {
    View,
    Text
} from 'react-native';
import { connect } from 'react-redux';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension
} from '../../../styles/base';
import {
    validate,
    getValidationMessage
} from '../../../utils';
import AuthTextInput from '../Misc/AuthTextInput';

const AuthSignUpBasicInfo = (props) => {
    const [name, setName] = useState('Shiina Mashiro');
    const [account, setAccount] = useState('dev@devesigns.com');
    const [nameValidation, setNameValidation] = useState('');
    const [accountValidation, setAccountValidation] = useState('');

    const resetForm = () => {
        setName('');
        setAccount('');
    }

    const resetValidation = () => {
        setNameValidation('');
        setAccountValidation('');
    }

    const validateForm = () => {
        let willResetForm = false;
        let isEmail = false;
        
        let nameValid = validate.validateName(name);
        if (nameValid != true) {
            willResetForm = true;
            setNameValidation(getValidationMessage(nameValid));
        }

        let accountValid = validate.validateAccount(account);
        if (accountValid != 'phone' && accountValid != 'email') {
            willResetForm = true;
            setAccountValidation(getValidationMessage(accountValid));
        } else {
            if (accountValid === 'email') {
                isEmail = true;
            }
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return {
            valid: true,
            isEmail
        };
    }

    props.setOnSubmit(() => {
        resetValidation();
        
        let validRes = validateForm();
        if (validRes.valid) {
            return {
                name,
                account,
                isEmail: validRes.isEmail
            }
        }

        return false;
    })

    return (
        <View
            style={{
                padding: basePadding.md,
                width: dimension.width
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.xxl,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                Cho chúng tôi biết đôi điều về bạn nhé!
            </Text>
            {
                props.signUpValidation && (typeof props.signUpValidation) != 'object' ?
                <Text
                    style={{
                        color: 'red',
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingBottom: basePadding.xs
                    }}
                >
                    {props.signUpValidation}
                </Text>
                :
                null
            }
            <View
                style={{
                    paddingBottom: basePadding.md
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    HỌ VÀ TÊN
                </Text>
                <AuthTextInput
                    value={name}
                    onChangeText={(value) => setName(name => value)}
                    fieldValidation={nameValidation}
                />
            </View>
            <View
                style={{
                    paddingBottom: basePadding.md
                }}
            >
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        SỐ ĐIỆN THOẠI&nbsp;
                    </Text>
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        hoặc
                    </Text>
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        &nbsp;EMAIL
                    </Text>
                </View>
                <AuthTextInput
                    value={account}
                    onChangeText={(value) => setAccount(account => value)}
                    fieldValidation={accountValidation}
                />
            </View>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                }}
            >
                Mã xác nhận sẽ được gửi qua tin nhắn đến số điện thoại hoặc email mà bạn cung cấp.
            </Text>
        </View>
    )
}

const mapStateToProps = ({ helper }) => {
    return {
        signUpValidation: helper.authValidation.signUp
    }
}

export default connect(mapStateToProps, {

})(AuthSignUpBasicInfo);
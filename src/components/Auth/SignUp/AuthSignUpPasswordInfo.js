import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension,
    baseIcon
} from '../../../styles/base';
import {
    validate,
    getValidationMessage
} from '../../../utils';
import AuthTextInput from '../Misc/AuthTextInput';
import CustomIcon from '../../common/CustomIcon';

const AuthSignUpPasswordInfo = (props) => {
    const [password, setPassword] = useState('');
    const [passwordValidation, setPasswordValidation] = useState('');
    const [passwordVisible, setPasswordVisible] = useState(false);

    const togglePasswordVisible = () => {
        setPasswordVisible(!passwordVisible);
    }

    const resetForm = () => {
        setPassword('');
    }

    const resetValidation = () => {
        setPasswordValidation('');
    }

    const validateForm = () => {
        let willResetForm = false;
        
        let passwordValid = validate.validatePassword(password);
        if (passwordValid != true) {
            willResetForm = true;
            setPasswordValidation(getValidationMessage(passwordValid));
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return true;
    }

    props.setOnSubmit(() => {
        resetValidation();
        
        if (validateForm()) {
            return {
                password
            }
        }

        return false;
    })

    return (
        <View
            style={{
                padding: basePadding.md,
                width: dimension.width
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.xxl,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                Tạo mật khẩu đăng nhập
            </Text>
            <View
                style={{
                    paddingBottom: basePadding.md
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    MẬT KHẨU
                </Text>
                <AuthTextInput
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                    fieldValidation={passwordValidation}
                    secureTextEntry={!passwordVisible}
                    renderRightIcon={() => {
                        return (
                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    zIndex: 999,
                                }}
                                onPress={togglePasswordVisible}
                            >
                                <CustomIcon
                                    name={passwordVisible ? 'eye-off' : 'eye'}
                                    size={baseIcon.md}
                                    style={{
                                        color: baseColor.three,
                                    }}
                                />
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                }}
            >
                Khi xác nhận tạo tài khoản, bạn đã đồng ý với
                <Text
                    style={{
                        fontFamily: baseFontFamily.bold
                    }}
                >
                &nbsp;Điều khoản và Điều kiện&nbsp;
                </Text>
                của chúng tôi.
                {'\n\n'}
                Để tìm hiểu thêm cách SLA bảo vệ thông tin cá nhân của bạn, vui lòng tham khảo
                <Text
                    style={{
                        fontFamily: baseFontFamily.bold
                    }}
                >
                &nbsp;Chính sách riêng tư&nbsp;
                </Text>
                .
            </Text>
        </View>
    )
}

export default AuthSignUpPasswordInfo;
import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text
} from 'react-native';
import {
    withNavigation
} from 'react-navigation';
import { connect } from 'react-redux';
import {
    coacherReducerUpdate,
    lessonReducerUpdate,
    lessonGetPopularLesson
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
} from '../../styles/base';
import HorizontalSlider from '../common/HorizontalSlider';
import VideoSliderItem from '../common/VideoSliderItem';

const HomePopularLesson = (props) => {

    /**
     * Methods
     */

    const onVideoPress = (item) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', item.id);
        props.lessonReducerUpdate('lessonType', 'normal');
        props.navigation.navigate('Lesson');
    }

    const onUserPress = (item) => {
        props.coacherReducerUpdate('coacherForm', {
            id: item.created_by.id
        });
        props.navigation.navigate('CoacherProfile');
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.popularLessons === null) {
            props.lessonGetPopularLesson(props.accessToken, {
                page: 1,
                perPage: 8
            })
        }
    }, [props.popularLessons])

    /**
     * Render methods
     */

    return (
        <View
            style={{
                paddingVertical: basePadding.md,
                backgroundColor: !_.isEmpty(props.courseSummaries) ? baseColor.two : baseColor.one
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.lg,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md,
                    paddingHorizontal: basePadding.md
                }}
            >
                BÀI HỌC NỔI BẬT
            </Text>
            <HorizontalSlider
                data={props.popularLessons}
                keyExtractor={(item, index) => {
                    return 'featured-lesson-' + index;
                }}
                renderItem={({item, index}) => {
                    return (
                        <VideoSliderItem
                            containerStyle={
                                _.isEmpty(props.courseSummaries) ?
                                {
                                    borderWidth: 0.5,
                                    borderColor: baseColor.two
                                }
                                :
                                null
                            }
                            data={item}
                            onPress={() => onVideoPress(item)}
                            onUserPress={() => onUserPress(item)}
                        />
                    )
                }}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md
                }}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, lesson, history }) => {
    return {
        accessToken: auth.accessToken,
        popularLessons: lesson.popularLessons,
        courseSummaries: history.courseSummaries
    }
};


export default connect(mapStateToProps, {
    coacherReducerUpdate,
    lessonReducerUpdate,
    lessonGetPopularLesson
})(withNavigation(HomePopularLesson));
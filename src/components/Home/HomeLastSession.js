import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding
} from '../../styles/base';
import CustomVideoTime from '../common/CustomVideoTime';
import CustomAbsoluteImage from '../common/CustomAbsoluteImage';

const HomeLastSession = (props) => {
    return (
        <View
            style={{
                padding: basePadding.md
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.lg,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                BÀI HỌC LẦN TRƯỚC
            </Text>
            <TouchableOpacity
                style={[
                    {
                        width: '100%'
                    },
                    props.containerStyle
                ]}
                onPress={props.onPress}
            >
                <CustomAbsoluteImage
                    source={{uri: props.data.session.thumbnail}}
                />
                {/* <CustomVideoTime
                    time={'Còn 1h12p'}
                /> */}
                <View
                    style={{
                        paddingTop: basePadding.xs
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        Video: {props.data.session.title}
                    </Text>
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.sm,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        Bài: {props.data.title}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default HomeLastSession;
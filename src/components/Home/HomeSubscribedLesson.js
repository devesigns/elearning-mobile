import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text
} from 'react-native';
import {
    withNavigation
} from 'react-navigation';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    lessonGetSubscribedLesson,
    coacherReducerUpdate
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
} from '../../styles/base';
import HorizontalSlider from '../common/HorizontalSlider';
import VideoSliderItem from '../common/VideoSliderItem';

const HomeSubscribedLesson = (props) => {

    /**
     * Methods
     */

    const onVideoPress = (item) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', item.id);
        props.lessonReducerUpdate('lessonType', item.course ? 'normal' : 'single');
        props.navigation.navigate('Lesson');
    }

    const onUserPress = (item) => {
        props.coacherReducerUpdate('coacherForm', {
            id: item.created_by.id
        });
        props.navigation.navigate('CoacherProfile');
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.subscribedLessons === null) {
            props.lessonGetSubscribedLesson(props.accessToken, {
                page: 1,
                perPage: 8
            })
        }
    }, [props.popularLessons])

    /**
     * Render methods
     */

    return (
        <View
            style={{
                paddingVertical: basePadding.md,
                backgroundColor: baseColor.two
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.lg,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md,
                    paddingHorizontal: basePadding.md
                }}
            >
                TỪ GIẢNG VIÊN CỦA BẠN
            </Text>
            <HorizontalSlider
                data={props.subscribedLessons}
                keyExtractor={(item, index) => {
                    return 'subscribed-lesson-' + index;
                }}
                renderItem={({item, index}) => {
                    return (
                        <VideoSliderItem
                            containerStyle={
                                _.isEmpty(props.courseSummaries) ?
                                {
                                    borderWidth: 0.5,
                                    borderColor: baseColor.two
                                }
                                :
                                null
                            }
                            data={item}
                            onPress={() => onVideoPress(item)}
                            onUserPress={() => onUserPress(item)}
                        />
                    )
                }}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md
                }}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        subscribedLessons: lesson.subscribedLessons
    }
};


export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonGetSubscribedLesson,
    coacherReducerUpdate
})(withNavigation(HomeSubscribedLesson));
import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    RefreshControl,
    SafeAreaView,
    Alert
} from 'react-native';
import { connect } from 'react-redux';
import {
    notificationFetch,
    notificationStatusUpdate,
    lessonReducerUpdate,
    courseReducerUpdate,
    historyReducerUpdate,
    accountReducerUpdate,
    accountGetUserInfo
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon
} from '../../styles/base';
import CustomSearchBar from '../common/CustomSearchBar';
import CourseSlider from '../Course/CourseSlider';
import { moderateScale } from '../../utils';
import HomeHeroCarousel from './HomeHeroCarousel';
import HomePopularLesson from './HomePopularLesson';
import HomeCourseSummary from './HomeCourseSummary';
import HomeSubscribedLesson from './HomeSubscribedLesson';

const HomeScreen = (props) => {
    const [refreshing, setRefreshing] = useState(false);

    /**
     * Methods
     */

    const onRefresh = () => {
        setRefreshing(true);
        setTimeout(() => {
            setRefreshing(false)
        }, 1000);

        props.courseReducerUpdate('courses', null);
        props.historyReducerUpdate('courseSummaries', null);
        props.lessonReducerUpdate('popularLessons', null);
        props.lessonReducerUpdate('pinnedLessons', null);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.navigation.state.params && props.navigation.state.params.plan_upgrade) {
            props.accountReducerUpdate('user', {});

            if (!_.isEmpty(props.lessonForm)) {
                props.lessonReducerUpdate('sessions', {});
                props.navigation.navigate('Lesson');
            }
        }
    }, [props.navigation])

    useEffect(() => {
        if (_.isEmpty(props.user)) {
            props.accountGetUserInfo(props.accessToken);
        }
    }, [props.user])

    useEffect(() => {
        if (!_.isEmpty(props.notifications)) {
            let noti = props.notifications[0];
            Alert.alert(
                'Thông báo',
                noti.content,
                [
                    {
                        text: 'Không hỏi lại',
                        onPress: () => {
                            props.notificationStatusUpdate(props.accessToken, {
                                notiId: noti.id
                            })
                        }
                    },
                    {
                        text: 'Hỏi sau',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Gia hạn',
                        onPress: () => {
                            props.navigation.navigate('AccountMain', {
                                extendSubscription: true
                            })
                        }
                    },
                ],
                {cancelable: false},
            );
        }
    }, [props.notifications])

    useEffect(() => {
        props.notificationFetch(props.accessToken);
    }, [])

    /**
     * Render methods
     */

    const renderRefreshControl = () => {
        return (
            <RefreshControl
                style={{
                    zIndex: 999,
                    elevation: 3
                }}
                refreshing={refreshing}
                onRefresh={ onRefresh}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps={'handled'}
                refreshControl={renderRefreshControl()}
            >
                <View
                    style={{
                        paddingVertical: basePadding.md,
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <View>
                        <Text
                            style={{
                                color: baseColor.three,
                                fontSize: baseFontSize.xxl + moderateScale(2),
                                fontFamily: baseFontFamily.bold
                            }}
                        >
                            SLA 
                        </Text>
                        <Image
                            source={require('../../assets/img/hat.png')}
                            style={{
                                position: 'absolute',
                                top: - basePadding.xs,
                                right: - (baseIcon.xl / 3),
                                height: baseIcon.xl,
                                width: baseIcon.xl,
                                transform: [
                                    {
                                        rotate: '30deg'
                                    }
                                ]
                            }}
                        />
                    </View>
                </View>
                <CustomSearchBar
                    placeholder={'Hôm nay bạn muốn học chủ đề nào?'}
                    containerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                    onSubmit={(query) => {
                        props.navigation.navigate('SearchResult', {
                            query
                        })
                    }}
                />
                <View
                    style={{
                        paddingVertical: basePadding.md
                    }}
                >
                    <CourseSlider />
                </View>
                <HomeHeroCarousel
                    onItemPress={(item) => {
                        props.lessonReducerUpdate('currentSession', {});
                        props.lessonReducerUpdate('lessonId', item.id);
                        props.lessonReducerUpdate('lessonType', 'single');
                        props.navigation.navigate('Lesson');
                    }}
                />
                <HomeCourseSummary />
                <HomePopularLesson />
                <HomeSubscribedLesson />
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, lesson, account, notification }) => {
    return {
        accessToken: auth.accessToken,
        lessonForm: lesson.lessonForm,
        user: account.user,
        notifications: notification.notifications
    }
};

export default connect(mapStateToProps, {
    notificationFetch,
    notificationStatusUpdate,
    lessonReducerUpdate,
    courseReducerUpdate,
    historyReducerUpdate,
    accountReducerUpdate,
    accountGetUserInfo
})(HomeScreen);
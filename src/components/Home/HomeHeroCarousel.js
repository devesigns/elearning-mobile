import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    lessonGetPinnedLesson
} from '../../actions';
import {
    baseColor,
    basePadding,
} from '../../styles/base';
import CustomCarousel from '../common/CustomCarousel';
import HeroCarouselItem from '../common/HeroCarouselItem';
import { HeroSliderPlaceholder } from '../common/Placeholder';

const HomeHeroCarousel = (props) => {

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.pinnedLessons === null) {
            props.lessonGetPinnedLesson(props.accessToken, {
                page: 1,
                perPage: 8
            })
        }
    }, [props.pinnedLessons])

    /**
     * Render methods
     */

    const renderPlaceholder = () => {
        return (
            <View
                style={{
                    padding: basePadding.md,
                    backgroundColor: baseColor.two
                }}
            >
                <HeroSliderPlaceholder />
            </View>
        )
    }

    if (_.isEmpty(props.pinnedLessons)) {
        return renderPlaceholder();
    }

    const renderCarouselItem = ({item, index}) => {
        return (
            <HeroCarouselItem
                data={item}
                onPress={() => props.onItemPress(item)}
            />
        )
    }

    return (
        <View
            style={{
                paddingVertical: basePadding.md,
                backgroundColor: baseColor.two
            }}
        >
            <CustomCarousel
                data={props.pinnedLessons}
                renderItem={renderCarouselItem}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        pinnedLessons: lesson.pinnedLessons
    }
};


export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonGetPinnedLesson
})(HomeHeroCarousel);
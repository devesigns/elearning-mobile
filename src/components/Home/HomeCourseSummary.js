import _ from 'lodash';
import React, { useState, useEffect, Fragment } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    courseReducerUpdate,
    historyReducerUpdate,
    historyGetCourseSummary
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension,
    baseBorderRadius,
    baseMargin,
} from '../../styles/base';
import HomeLastSession from './HomeLastSession';
import CourseOverview from '../Course/CourseOverview';
import HorizontalSlider from '../common/HorizontalSlider';

const HomeCourseSummary = (props) => {

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.courseSummaries === null) {
            props.historyGetCourseSummary(props.accessToken, {
                page: 1,
                perPage: 3
            })
        }
    }, [props.courseSummaries])

    /**
     * Render methods
     */

    const renderCourseSummaryItem = ({item, index}) => {
        return (
            <View
                style={{
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    borderWidth: 0.5,
                    borderRadius: baseBorderRadius.lg,
                    elevation: 3,
                    shadowOffset: {
                        width: 3,
                        height: 3
                    },
                    shadowOpacity: 0.5,
                    shadowColor: 'rgba(0, 0, 0, 0.3)',
                    backgroundColor: baseColor.one,
                    marginBottom: baseMargin.md,
                    marginHorizontal: baseMargin.md / 2,
                    width: dimension.width - basePadding.md * 3
                }}
            >
                <View
                    style={{
                        borderRadius: baseBorderRadius.lg,
                        overflow: 'hidden'
                    }}
                >
                    <CourseOverview
                        data={item.course}
                        padding={basePadding.md * 3}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => {
                        props.courseReducerUpdate('courseForm', item);
                        props.navigation.navigate('CourseDetail');
                    }}
                    style={{
                        paddingHorizontal: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold,
                        }}
                    >
                        Xem trang chi tiết lộ trình và video
                    </Text>
                </TouchableOpacity>
                <HomeLastSession
                    data={item.latest_lesson}
                    onPress={() => {
                        props.lessonReducerUpdate('lessonId', item.latest_lesson.id);
                        props.lessonReducerUpdate('lessonType', 'normal');
                        props.lessonReducerUpdate('continuedSession', {
                            id: item.latest_lesson.session.id,
                            arrayId: item.latest_lesson.session.order - 1,
                            src: item.latest_lesson.session.video,
                            pausedAt: item.latest_lesson.session.paused_at
                            // pausedAt: 30
                        })
                        props.navigation.navigate('Lesson');
                    }}
                />
            </View>
        )
    }

    if (_.isEmpty(props.courseSummaries)) {
        return null;
    }

    return (
        <View
            style={{
                backgroundColor: baseColor.one
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.lg,
                    fontFamily: baseFontFamily.bold,
                    padding: basePadding.md,
                }}
            >
                TIẾN ĐỘ LỘ TRÌNH
            </Text>
            <HorizontalSlider
                data={props.courseSummaries}
                keyExtractor={(item, index) => {
                    return 'featured-lesson-' + index;
                }}
                renderItem={renderCourseSummaryItem}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md / 2
                }}
                noSeparator
            />
        </View>            
    )
}

const mapStateToProps = ({ auth, history }) => {
    return {
        accessToken: auth.accessToken,
        courseSummaries: history.courseSummaries
    }
};

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    courseReducerUpdate,
    historyReducerUpdate,
    historyGetCourseSummary
})(withNavigation(HomeCourseSummary));
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    lessonSearchLesson,
    courseReducerUpdate
} from '../../actions';
import {
    baseColor,
    baseMargin,
    basePadding,
    baseIcon
} from '../../styles/base';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import CustomSearchBar from '../common/CustomSearchBar';
import SearchResultListItem from './SearchResultListItem';
import CustomIcon from '../common/CustomIcon';
import { moderateScale } from '../../utils';

const CoacherListScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    /**
     * Methods
     */

    const onRefresh = () => {
        props.lessonReducerUpdate('searchedLessons', null);
    }

    const onSearchSubmit = (query) => {
        if (query !== searchQuery && query != '') {
            setSearchQuery(query);
            onRefresh();
        }
    }

    const onCoursePress = (item) => {
        props.courseReducerUpdate('courseForm', {
            course: item
        });
        props.navigation.navigate('CourseDetail');
    }

    const onLessonPress = (lesson) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', lesson.id);
        props.lessonReducerUpdate('lessonType', lesson.course ? 'normal' : 'single');
        props.navigation.navigate('Lesson');
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }
    
    /**
     * UseEffect
     */

    useEffect(() => {
        onRefresh();
        setSearchQuery(props.navigation.getParam('query'));
    }, [])

    /**
     * Render Methods
     */

    if (!mounted) {
        return null;
    }

    const renderItem = ({item, index}) => {
        return (
            <SearchResultListItem
                data={item}
                onPress={() => onLessonPress(item)}
                onCoursePress={() => {
                    onCoursePress({
                        id: item.course
                    })
                }}
            />
        );
    }

    return (
        <SafeAreaView
            style={{
                flex: 1
            }}
        >
            <View
                style={{
                    flex: 1,
                    backgroundColor: baseColor.two
                }}
            >
                <View
                    style={{
                        backgroundColor: baseColor.one,
                        padding: basePadding.md,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <TouchableOpacity
                        onPress={() => {
                            props.navigation.goBack(null);
                        }}
                        style={{
                            paddingRight: basePadding.md
                        }}
                    >
                        <CustomIcon
                            name={'arrow-left'}
                            size={baseIcon.md}
                            style={{
                                color: baseColor.four
                            }}
                        />
                    </TouchableOpacity>
                    <CustomSearchBar
                        placeholder={'Tìm kiếm bài học...'}
                        onSubmit={onSearchSubmit}
                        containerStyle={{
                            flex: 1,
                            height: moderateScale(40)
                        }}
                    />
                </View>
                <CustomFlatListWithFetch
                    keyExtractor={(item, index) => {
                        return 'search-result-' + index;
                    }}
                    data={props.searchedLessons}
                    renderItem={renderItem}
                    perPage={16}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                    onRefresh={onRefresh}
                    fetchNextBatch={(page, perPage) => {
                        props.lessonSearchLesson(props.accessToken, {
                            page,
                            perPage,
                            searchQuery
                        })
                    }}
                />
            </View>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        searchedLessons: lesson.searchedLessons
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonSearchLesson,
    courseReducerUpdate
})(CoacherListScreen);
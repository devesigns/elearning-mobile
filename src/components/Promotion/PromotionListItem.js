import React from 'react';
import {
    View, 
    Text,
    TouchableOpacity
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontFamily,
    baseIcon,
    baseFontSize
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';

const PromotionListItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                borderBottomWidth: 0.5,
                borderBottomColor: baseColor.two,
                paddingVertical: basePadding.sm,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between'
            }}
            onPress={props.onPress}
        >
            <View>
                <Text
                    style={{
                        fontSize: baseFontSize.lg,
                        color: baseColor.four,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    {props.data.promo_code.code}
                </Text>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular
                    }}
                >
                    Khóa: {props.data.promo_code.course.title}
                </Text>
            </View>
            <CustomIcon
                name='arrow-right'
                size={baseIcon.sm}
                style={{
                    color: baseColor.four
                }}
            />
        </TouchableOpacity>
    )
}

export default PromotionListItem;
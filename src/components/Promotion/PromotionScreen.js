import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    TextInput,
    Text,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {
    courseReducerUpdate,
    promotionGetPromotion,
    promotionRedeemCode,
    promotionReducerUpdate
} from '../../actions';
import {
    baseColor,
    baseMargin,
    basePadding,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import PromotionListItem from './PromotionListItem';

const PromotionScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [code, setCode] = useState('');

    /**
     * Methods
     */

    const onRefresh = () => {
        props.promotionReducerUpdate('promotions', null);
    }

    const onItemPress = (item) => {
        props.courseReducerUpdate('courseForm', {
            course: item
        });
        props.navigation.navigate('CourseDetail');
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }
    
    /**
     * UseEffect
     */

    useEffect(() => {
        onRefresh();
    }, [])

    /**
     * Render Methods
     */

    if (!mounted) {
        return null;
    }

    const renderItem = ({item, index}) => {
        return (
            <PromotionListItem
                data={item}
                onPress={() => {
                    onItemPress({
                        id: item.promo_code.course.id
                    })
                }}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.two
            }}
        >
            <View>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingHorizontal: basePadding.md,
                        paddingVertical: basePadding.xs,
                    }}
                >
                    Nhập mã khuyến mãi
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        paddingHorizontal: basePadding.md,
                        paddingVertical: basePadding.xs,
                        backgroundColor: baseColor.one
                    }}
                >
                    <TextInput
                        value={code}
                        onChangeText={(value) => setCode(value)}
                        placeholder='ABC-XYZ-123'
                        style={{
                            flex: 1,
                            padding: 0,
                            fontSize: baseFontSize.lg,
                            color: baseColor.four
                        }}
                    />
                    <TouchableOpacity
                        onPress={() => {
                            props.promotionRedeemCode(props.accessToken, {
                                code
                            })
                            setCode('');
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.three,
                                fontSize: baseFontSize.md,
                                fontFamily: baseFontFamily.bold
                            }}
                        >
                            NHẬN
                        </Text>
                    </TouchableOpacity>
                </View>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingHorizontal: basePadding.md,
                        paddingVertical: basePadding.xs,
                    }}
                >
                    Mã đã nhận
                </Text>
            </View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: baseColor.one
                }}
            >
                <CustomFlatListWithFetch
                    keyExtractor={(item, index) => {
                        return 'promotion-' + index;
                    }}
                    data={props.promotions}
                    renderItem={renderItem}
                    perPage={16}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                    onRefresh={onRefresh}
                    fetchNextBatch={(page, perPage) => {
                        props.promotionGetPromotion(props.accessToken, {
                            page,
                            perPage
                        })
                    }}
                    renderEmptyPlaceholder={() => {
                        return (
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.regular,
                                    padding: basePadding.md
                                }}
                            >
                                Chưa có.
                            </Text>
                        )
                    }}
                />
            </View>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, promotion }) => {
    return {
        accessToken: auth.accessToken,
        promotions: promotion.promotions
    }
}

export default connect(mapStateToProps, {
    courseReducerUpdate,
    promotionGetPromotion,
    promotionRedeemCode,
    promotionReducerUpdate
})(PromotionScreen);
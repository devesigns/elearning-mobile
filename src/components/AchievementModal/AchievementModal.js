import _ from 'lodash';
import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    Text,
    Modal,
    Animated,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Image
} from 'react-native';
import LottieView from 'lottie-react-native';
import { connect } from 'react-redux';
import {
    accountReducerUpdate,
    accountUpdateReward
} from '../../actions';
import {
    dimension,
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseMargin
} from '../../styles/base';
import { moderateScale, navigate } from '../../utils';

const AchievementModal = (props) => {
    const animationRef = useRef(null);
    const [visible, setVisible] = useState(false);
    const [visibleAnim, setVisibleAnim] = useState(new Animated.Value(0));

    /**
     * Methods
     */

    const onSeen = () => {
        setVisible(false);
        for (let i = 0; i < props.latestAchievements.length; i++) {
            props.accountUpdateReward(props.accessToken, {
                rewardId: props.latestAchievements[i].id
            })
        }
        props.accountReducerUpdate('recentAchievements', null);
        props.accountReducerUpdate('latestAchievements', null);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (visible) {
            setTimeout(() => {
                Animated.timing(
                    visibleAnim,
                    {
                        toValue: 1,
                        useNativeDriver: true,
                        duration: 1000
                    }
                ).start();
                animationRef.current.play();
            }, 500);
        } else {
            visibleAnim.setValue(0);
        }
    }, [visible])

    useEffect(() => {
        if (!_.isEmpty(props.latestAchievements)) {
            setVisible(true);
        }
    }, [props.latestAchievements])

    /**
     * Render methods
     */

    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={visible}
            onRequestClose={() => {
                setVisible(false);
            }}
        >
            <TouchableWithoutFeedback
                onPress={onSeen}
            >
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.3)'
                    }}
                >
                    <View
                        style={{
                            width: dimension.width - basePadding.md,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <LottieView
                            ref={animationRef}
                            source={require('../../assets/lottie/achievement.json')}
                            loop={false}
                            style={{
                                zIndex: 999,
                                transform: [
                                    {
                                        translateY: dimension.width * 0.075
                                    }
                                ]
                            }}
                        />
                        <Animated.View
                            style={{
                                opacity: visibleAnim,
                                width: dimension.width * 0.5,
                                height: dimension.width * 0.5,
                                borderRadius: dimension.width * 0.5 / 2,
                                backgroundColor: baseColor.one,
                                borderWidth: moderateScale(5),
                                borderColor: baseColor.three,
                                justifyContent: 'center',
                                alignItems: 'center',
                                paddingBottom: dimension.width * 0.075 + basePadding.sm
                            }}
                        >
                            {
                                !_.isEmpty(props.latestAchievements) ?
                                <View
                                    style={{
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                >
                                    <Image
                                        source={{uri: props.latestAchievements[0].image}}
                                        style={{
                                            height: dimension.width * 0.175,
                                            width: dimension.width * 0.175
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: baseColor.four,
                                            fontSize: baseFontSize.md,
                                            fontFamily: baseFontFamily.bold
                                        }}
                                    >
                                        {props.latestAchievements[0].title}
                                    </Text>
                                </View>
                                :
                                null
                            }
                        </Animated.View>
                    </View>
                    {
                        props.latestAchievements && props.latestAchievements.length > 1 ?
                        <Animated.View
                            style={{
                                opacity: visibleAnim,
                                zIndex: 999
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    padding: basePadding.sm,
                                    paddingHorizontal: basePadding.xl,
                                    backgroundColor: baseColor.one,
                                    borderRadius: basePadding.md + baseFontSize.md * 1.4,
                                    marginTop: baseMargin.md
                                }}
                                onPress={() => {
                                    onSeen();
                                    navigate('Account');
                                }}
                            >
                                <Text
                                    style={{
                                        color: baseColor.four,
                                        fontSize: baseFontSize.xs,
                                        fontFamily: baseFontFamily.regular
                                    }}
                                >
                                    Bạn có {props.latestAchievements.length - 1} thành tựu mới khác..
                                </Text>
                            </TouchableOpacity>
                        </Animated.View>
                        :
                        null
                    }
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        latestAchievements: account.latestAchievements
    }
};

export default connect(mapStateToProps, {
    accountReducerUpdate,
    accountUpdateReward
})(AchievementModal);
import _ from 'lodash';
import React, { Fragment, useEffect } from 'react';
import {
    ActivityIndicator,
    View
} from 'react-native';
import { connect } from 'react-redux';
import {
    
} from '../actions';
import {
    dimension,
    baseColor,
    baseIcon
} from '../styles/base';
import AppNavigator from '../routes/AppNavigator';
import { setTopLevelNavigator } from '../utils';
import AchievementModal from './AchievementModal/AchievementModal';
import NotificationHandler from './NotificationHandler';

const AppWrapper = (props) => {
    return (
        <Fragment>
            <AppNavigator 
                uriPrefix={'sla://'}
                ref={(navigatorRef) => {
                    setTopLevelNavigator(navigatorRef);
                }}
            />
            <AchievementModal />
            {
                props.globalLoading ?
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        width: '100%',
                        backgroundColor: 'rgba(255, 255, 255, 0.5)',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <View
                        style={{
                            width: dimension.width * 0.3,
                            height: dimension.width * 0.3,
                            backgroundColor: 'rgba(255, 255, 255, 0.8)',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <ActivityIndicator
                            color={baseColor.three}
                            size={baseIcon.xxxl}
                        />
                    </View>
                </View>
                :
                null
            }
            {/* Notification Handler */}
            <NotificationHandler />
        </Fragment>
    )
}

const mapStateToProps = ({ helper }) => {
    return {
        globalLoading: helper.loading.globalLoading
    }
}

export default connect(mapStateToProps, {

})(AppWrapper);
import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    dimension
} from '../../styles/base';

const AchievementGridItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                width: (dimension.width - basePadding.md * 2) / 2 - basePadding.xs,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                padding: basePadding.sm,
                borderColor: 'rgba(173, 178, 202, 0.5)',
                borderWidth: 0.5,
            }}
        >
            {
                !props.data.achieved ?
                <View>
                    <Image
                        source={{uri: props.data.image}}
                        style={{
                            width: dimension.width * 0.3,
                            height: dimension.width * 0.3,
                            tintColor: baseColor.four
                        }}
                        resizeMode={'contain'}
                    />
                    <Image
                        source={{uri: props.data.image}}
                        style={{
                            width: dimension.width * 0.3,
                            height: dimension.width * 0.3,
                            position: 'absolute',
                            opacity: 0.3
                        }}
                        resizeMode={'contain'}
                    />
                </View>
                :
                <Image
                    source={{uri: props.data.image}}
                    style={{
                        width: dimension.width * 0.3,
                        height: dimension.width * 0.3
                    }}
                    resizeMode={'contain'}
                />
            }
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    paddingTop: basePadding.sm
                }}
            >
                <Text
                    style={{
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold,
                        textAlign: 'center'
                    }}
                >
                    {props.data.title}
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular,
                        textAlign: 'center'
                    }}
                >
                    {props.data.description}
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.light,
                        color: baseColor.five,
                        textAlign: 'center'
                    }}
                >
                    {props.data.achieved ? 'Đã đạt được' : 'Chưa đạt được'}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default AchievementGridItem;
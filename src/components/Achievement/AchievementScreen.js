import React, { useState } from 'react';
import {
    SafeAreaView,
    View
} from 'react-native';
import { connect } from 'react-redux';
import {
    
} from '../../actions';
import {
    basePadding,
    baseColor
} from '../../styles/base';
import AchievementGrid from './AchievementGrid';

const AchievementScreen = (props) => {
    
    /**
     * Methods
     */

    /**
     * UseEffect
     */

    /**
     * Render Methods
     */
    
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    flex: 1,
                    backgroundColor: baseColor.one,
                    paddingHorizontal: basePadding.md
                }}
            >
                <AchievementGrid />
            </View>
        </SafeAreaView>
    )
}

const mapStateToProps = null;

export default connect(mapStateToProps, {
    
})(AchievementScreen);
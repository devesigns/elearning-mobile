import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
    accountReducerUpdate,
    accountGetAchievement
} from '../../actions';
import {
    baseMargin,
} from '../../styles/base';
import AchievementGridItem from './AchievementGridItem';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import { AchievementPlaceholder } from '../common/Placeholder';

const AchievementGrid = (props) => {

    const [mounted, setMounted] = useState(false);
    
    /**
     * Methods
     */

    const onRefresh = () => {
        props.accountReducerUpdate('achievements', null);
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }
    
    /**
     * UseEffect
     */

    /**
     * Render Methods
     */

    return (
        <CustomFlatListWithFetch
            keyExtractor={(item, index) => {
                return 'achievement-grid-item-' + item.id;
            }}
            data={props.achievements}
            renderItem={({ item, index }) => {
                if (item.type != 'view-all') {
                    return (
                        <AchievementGridItem
                            data={item}
                        />
                    )
                } else {
                    if (props.renderViewAllButton) {
                        return props.renderViewAllButton();
                    }
                }
            }}
            numColumns={2}
            perPage={16}
            columnWrapperStyle={{
                marginBottom: baseMargin.md,
                justifyContent: 'space-between'
            }}
            onRefresh={onRefresh}
            fetchNextBatch={(page, perPage) => {
                props.accountGetAchievement(props.accessToken, {
                    page,
                    perPage
                })
            }}
            renderPlaceholder={() => {
                return (
                    <AchievementPlaceholder />
                )
            }}
        />
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        achievements: account.achievements
    }
}

export default connect(mapStateToProps, {
    accountReducerUpdate,
    accountGetAchievement
})(AchievementGrid);
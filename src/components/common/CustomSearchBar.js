import React, { useState } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import CustomIcon from './CustomIcon';
import { moderateScale } from '../../utils';
import { 
    baseIcon,
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';

const CustomSearchBar = (props) => {
    const [isFocused, setIsFocused] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    const onCancel = () => {
        Keyboard.dismiss();
        setSearchQuery('');
        setIsFocused(false);
    }

    return (
        <View
            style={[
                {
                    width: '100%'
                },
                props.containerStyle
            ]}
        >
            <View
                style={[
                    {
                        height: moderateScale(40),
                        borderRadius: moderateScale(40 / 2),
                        paddingHorizontal: basePadding.sm,
                        borderWidth: 0.5,
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderColor: baseColor.five
                    },
                    isFocused ? {
                        borderColor: baseColor.three
                    }
                    :
                    null,
                    props.containerStyle
                ]}
            >
                <CustomIcon
                    name={'search'}
                    size={baseIcon.md}
                    style={[
                        {
                            color: baseColor.five
                        },
                        isFocused ?
                        {
                            color: baseColor.three
                        }
                        :
                        null
                    ]}
                />
                <TextInput
                    value={searchQuery}
                    style={[
                        {
                            flex: 1,
                            padding: 0,
                            paddingHorizontal: basePadding.xs,
                            color: baseColor.five,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular
                        },
                        isFocused ?
                        {
                            color: baseColor.four
                        }
                        :
                        null
                    ]}
                    onChangeText={(value) => {
                        setSearchQuery(value);
                    }}
                    placeholder={props.placeholder}
                    onSubmitEditing={() => {
                        if (props.onSubmit) {
                            props.onSubmit(searchQuery);
                        }
                    }}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                />
                {
                    searchQuery ?
                    <TouchableOpacity
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '100%',
                        }}
                        onPress={onCancel}
                    >
                        <CustomIcon
                            name={'x'}
                            size={baseIcon.md}
                            style={[
                                {
                                    color: baseColor.five
                                },
                                isFocused ?
                                {
                                    color: baseColor.four
                                }
                                :
                                null
                            ]}
                        />
                    </TouchableOpacity>
                    :
                    null
                }
            </View>
        </View>
    )
}

export default CustomSearchBar;

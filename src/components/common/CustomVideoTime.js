import React from 'react';
import {
    View,
    Text
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseBorderRadius
} from '../../styles/base';

const CustomVideoTime = (props) => {
    return (
        <View
            style={{
                position: 'absolute',
                top: basePadding.sm,
                right: basePadding.sm,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Text
                style={{
                    padding: basePadding.xs,
                    paddingHorizontal: basePadding.sm,
                    color: baseColor.one,
                    backgroundColor: 'rgba(173, 178, 202, 0.7)',
                    fontSize: baseFontSize.xs,
                    fontFamily: baseFontFamily.regular,
                    textAlign: 'center',
                    borderRadius: baseBorderRadius.lg
                }}
            >
                {props.time}
            </Text>
        </View>
    )
}

export default CustomVideoTime;
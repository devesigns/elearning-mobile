import React from 'react';
import {
    Text,
    TouchableOpacity,
    SafeAreaView,
    View
} from 'react-native';
import {
    baseIcon,
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';

const BackButton = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                position: 'absolute',
                left: basePadding.md,
            }}
        >
            <CustomIcon
                name={'arrow-left'}
                size={baseIcon.xl}
                style={{
                    color: baseColor.three
                }}
            />
        </TouchableOpacity>
    )
}

const Header = (props) => {
    let backButtonVisible = true;

    if (
        !(props.scene.descriptor.options.showBackButton) &&
        (props.scene.index === 0 ||
        (props.scene.route.params && props.scene.route.params.hideBackButton))
    ) {
        backButtonVisible = false;
    }

    return (
        <SafeAreaView
            style={{
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.md,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                {
                    backButtonVisible ?
                    <BackButton
                        onPress={() => {
                            props.navigation.goBack(null);
                        }}
                    />
                    :
                    null
                }
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    {props.scene.descriptor.options.title}
                </Text>
            </View>
        </SafeAreaView>
    )
}

export default Header;
import React from 'react';
import {
    View,
    Image
} from 'react-native';
import {
    baseBorderRadius, baseColor
} from '../../styles/base';

const CustomAbsoluteImage = (props) => {
    return (
        <View
            style={[
                {
                    paddingBottom: '56.25%',
                    borderRadius: baseBorderRadius.lg,
                    backgroundColor: baseColor.nine,
                    overflow: 'hidden'
                },
                props.containerStyle
            ]}
        >
            <Image
                source={props.source}
                style={[
                    {
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        width: '100%'
                    },
                    props.style
                ]}
                resizeMode={'cover'}
            />
            {
                props.renderOverlay ?
                props.renderOverlay()
                :
                null
            }
        </View>
    )
}

export default CustomAbsoluteImage;
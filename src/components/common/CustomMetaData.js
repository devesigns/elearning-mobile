import React from 'react';
import {
    View,
    Text
} from 'react-native';
import CustomIcon from './CustomIcon';
import {
    baseColor,
    baseIcon,
    baseFontSize,
    baseFontFamily,
    basePadding
} from '../../styles/base';

export const CustomMetaDataBreaker = (props) => {
    return (
        <View
            style={{
                paddingHorizontal: basePadding.sm,
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            <CustomIcon
                name={'circle'}
                size={3}
                style={{
                    color: props.color ? props.color : baseColor.five
                }}
            />
        </View>
    )
}

const CustomMetaData = (props) => {
    return (
        <View
            style={{
                flexDirection: 'row',
                alignItems: 'center',
            }}
        >
            <CustomIcon
                name={props.iconName}
                size={baseIcon.xxs}
                style={{
                    color: baseColor.five
                }}
            />
            <Text
                style={{
                    paddingLeft: basePadding.xs,
                    color: baseColor.five,
                    fontSize: baseFontSize.sm,
                    fontFamily: baseFontFamily.regular
                }}
            >
                {props.label}
            </Text>
        </View>
    )
}

export default CustomMetaData;
import React from 'react';
import { 
    View,
    Modal,
    ScrollView,
    Text,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import {
    dimension,
    basePadding,
    baseColor,
    baseFontFamily,
    baseFontSize
} from '../../styles/base';

const CustomModal = (props) => {
    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={props.visible}
            onRequestClose={props.onRequestClose}
        >
            <TouchableWithoutFeedback
                onPress={props.onRequestClose}
            >
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                        backgroundColor: 'rgba(0, 0, 0, 0.3)'
                    }}
                >
                    <View
                        style={[
                            {
                                width: dimension.width - basePadding.md * 2,
                                maxHeight: dimension.height  - basePadding.md * 2,
                                backgroundColor: baseColor.one,
                                padding: basePadding.md
                            },
                            props.containerStyle
                        ]}
                    >
                        <ScrollView
                            showsVerticalScrollIndicator={false}
                        >
                            {props.children}
                        </ScrollView>
                        <View
                            style={[
                                {
                                    paddingTop: basePadding.md,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end'
                                },
                                props.footerStyle
                            ]}
                        >
                            <TouchableOpacity
                                onPress={props.onRequestClose}
                            >
                                <Text
                                    style={{
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.bold,
                                        color: baseColor.five,
                                    }}
                                >
                                    QUAY LẠI
                                </Text>
                            </TouchableOpacity>
                            {
                                props.hideConfirm ? 
                                null
                                :
                                <TouchableOpacity
                                    onPress={props.onConfirm ? props.onConfirm : props.onRequestClose}
                                >
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.md,
                                            fontFamily: baseFontFamily.bold,
                                            color: baseColor.three,
                                            paddingLeft: basePadding.md
                                        }}
                                    >
                                        XÁC NHẬN
                                    </Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
}

export default CustomModal;

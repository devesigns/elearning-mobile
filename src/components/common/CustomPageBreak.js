import React from 'react';
import {
    View
} from 'react-native';
import {
    basePadding,
    baseColor
} from '../../styles/base';

const CustomPageBreak = (props) => {
    return (
        <View
            style={{
                height: basePadding.xs,
                backgroundColor: baseColor.two
            }}
        />
    )
}

export default CustomPageBreak;
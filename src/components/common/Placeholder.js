import React, { useState, useEffect } from 'react';
import {
    View,
    Animated
} from 'react-native';
import {
    baseColor,
    baseFontSize,
    basePadding,
    dimension,
    baseMargin,
    baseBorderRadius
} from '../../styles/base';


export const ProfilePlaceholder = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.two

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    padding: basePadding.md,
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%'
                }}
            >
                <View
                    style={{
                        height: dimension.width * 0.2,
                        width: dimension.width * 0.2,
                        borderRadius: dimension.width * 0.2 / 2,
                        backgroundColor
                    }}
                />
                <View
                    style={{
                        flexDirection: 'column',
                        flex: 1,
                        paddingLeft: basePadding.md
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: baseFontSize.md * 1.4,
                            backgroundColor,
                            marginBottom: baseMargin.sm
                        }}
                    />
                    <View
                        style={{
                            width: dimension.width * 0.3,
                            height: baseFontSize.md * 1.4,
                            backgroundColor,
                            marginBottom: baseMargin.sm
                        }}
                    />
                    <View
                        style={{
                            width: dimension.width * 0.5,
                            height: baseFontSize.md * 1.4,
                            backgroundColor,
                        }}
                    />
                </View>
            </View>
        </PlaceholderWrapper>
    )
}

export const VideoListPlaceholder = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.two

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    padding: basePadding.md,
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%'
                }}
            >
                <View
                    style={{
                        height: dimension.width * 0.3 * 9 / 16,
                        width: dimension.width * 0.3,
                        borderRadius: baseBorderRadius.md,
                        backgroundColor
                    }}
                />
                <View
                    style={{
                        flexDirection: 'column',
                        flex: 1,
                        paddingLeft: basePadding.md
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: baseFontSize.md,
                            backgroundColor,
                            marginBottom: baseMargin.sm
                        }}
                    />
                    <View
                        style={{
                            width: dimension.width * 0.3,
                            height: baseFontSize.md,
                            backgroundColor,
                            marginBottom: baseMargin.sm
                        }}
                    />
                </View>
            </View>
        </PlaceholderWrapper>
    )
}

export const VideoListPlaceholderType2 = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.two

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    height: (dimension.width - basePadding.md * 2) * 9 / 16,
                    borderRadius: baseBorderRadius.md,
                    backgroundColor
                }}
            />
            <View
                style={{
                    width: '100%',
                    height: baseFontSize.md,
                    backgroundColor,
                    marginTop: baseMargin.sm
                }}
            />
            <View
                style={{
                    width: '40%',
                    height: baseFontSize.md,
                    backgroundColor,
                    marginTop: baseMargin.sm
                }}
            />
        </PlaceholderWrapper>
    )
}

export const AchievementPlaceholder = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.two

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    padding: basePadding.md,
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    width: '100%'
                }}
            >
                <View
                    style={{
                        width: '49%',
                        borderRadius: baseBorderRadius.lg,
                        borderColor: baseColor.two,
                        padding: basePadding.sm,
                        flexDirection: 'column'
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: dimension.width * 0.25,
                            marginBottom: basePadding.md,
                            backgroundColor
                        }}
                    />
                    <View
                        style={{
                            width: '100%',
                            height: 25,
                            marginBottom: basePadding.sm,
                            backgroundColor
                        }}
                    />
                    <View
                        style={{
                            width: '60%',
                            height: 25,
                            backgroundColor
                        }}
                    />
                </View>
                <View
                    style={{
                        width: '49%',
                        borderRadius: baseBorderRadius.lg,
                        borderColor: baseColor.two,
                        padding: basePadding.sm,
                        flexDirection: 'column'
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: dimension.width * 0.25,
                            marginBottom: basePadding.md,
                            backgroundColor
                        }}
                    />
                    <View
                        style={{
                            width: '100%',
                            height: 25,
                            marginBottom: basePadding.sm,
                            backgroundColor
                        }}
                    />
                    <View
                        style={{
                            width: '60%',
                            height: 25,
                            backgroundColor
                        }}
                    />
                </View>
            </View>
        </PlaceholderWrapper>
    )
}

export const HorizontalSliderPlaceholder = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.two

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <View
                    style={{
                        width: props.itemWidth ? props.itemWidth : dimension.width * 0.6,
                        height: props.itemHeight ? props.itemHeight : dimension.width * 0.6 * 9 / 16,
                        borderRadius: baseBorderRadius.lg,
                        backgroundColor,
                        marginRight: baseMargin.md
                    }}
                />
                <View
                    style={{
                        width: props.itemWidth ? props.itemWidth : dimension.width * 0.6,
                        height: props.itemHeight ? props.itemHeight : dimension.width * 0.6 * 9 / 16,
                        borderRadius: baseBorderRadius.lg,
                        backgroundColor,
                        marginRight: baseMargin.md
                    }}
                />
            </View>
        </PlaceholderWrapper>
    )
}

export const HeroSliderPlaceholder = (props) => {
    let backgroundColor = props.backgroundColor ? props.backgroundColor : baseColor.one

    return (
        <PlaceholderWrapper>
            <View
                style={{
                    width: dimension.width - basePadding.md * 2,
                    height: dimension.width * 9 / 16,
                    borderRadius: baseBorderRadius.lg,
                    backgroundColor,
                    marginRight: baseMargin.md
                }}
            />
        </PlaceholderWrapper>
    )
}

const PlaceholderWrapper = (props) => {
    const [anim, setAnim] = useState(new Animated.Value(0.5));

    const loadAnim = () => {
        Animated.sequence([
            Animated.timing(
                anim,
                {
                    toValue: 1,
                    duration: 300,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                anim,
                {
                    toValue: 0.5,
                    duration: 300,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                anim,
                {
                    toValue: 1,
                    duration: 300,
                    useNativeDriver: true
                }
            ),
            Animated.timing(
                anim,
                {
                    toValue: 0.5,
                    duration: 0,
                    useNativeDriver: true
                }
            )
        ]).start(loadAnim);
    }

    loadAnim();

    return (
        <Animated.View
            style={{
                opacity: anim
            }}
        >
            {props.children}
        </Animated.View>
    )
}

export default PlaceholderWrapper;
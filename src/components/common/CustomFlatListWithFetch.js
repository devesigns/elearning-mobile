import _ from 'lodash';
import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    FlatList,
    ActivityIndicator,
    RefreshControl,
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseIcon,
} from '../../styles/base';

const CustomFlatListWithFetch = (props) => {
    const viewRef = useRef(null);
    const [page, setPage] = useState(0);
    const [refreshing, setRefreshing] = useState(false);
    const [isFetching, setIsFetching] = useState(false);
    const [endOfList, setEndOfList] = useState(false);
    const [fetchNext, setFetchNext] = useState(false);

    /**
     * Methods
     */

    const onRefresh = () => {
        if (props.fetchNextBatch) {
            setRefreshing(true);
            setTimeout(() => {
                setRefreshing(false)
            }, 1000);

            setEndOfList(false);
            setFetchNext(false);
            setPage(-1);
        }
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (fetchNext) {
            if (endOfList) {
                setFetchNext(false);
                return;
            }

            setPage(page + 1);
        }
    }, [fetchNext])

    useEffect(() => {
        if (props.data != null) {
            setFetchNext(false);
            setIsFetching(false);

            if (props.data.length < page * props.perPage) {
                setEndOfList(true);
            }
        } else {
            onRefresh();
        }
    }, [props.data])

    useEffect(() => {
        if (page === -1) {
            setPage(1);
        } else if (page >= 1) {
            setIsFetching(true);
            props.fetchNextBatch(page, props.perPage);
        }
    }, [page])

    /**
     * Render methods
     */

    const renderRefreshControl = () => {
        return (
            <RefreshControl
                style={{
                    zIndex: 999,
                    elevation: 3
                }}
                refreshing={refreshing}
                onRefresh={() => {
                    onRefresh();
                    if (props.onRefresh) {
                        props.onRefresh();
                    }
                }}
                progressViewOffset={props.contentContainerStyle ? props.contentContainerStyle.paddingTop : 0}
            />
        )
    }

    if (props.data === null && props.renderPlaceholder) {
        return props.renderPlaceholder();
    }

    if (_.isEmpty(props.data)) {
        if (props.renderEmptyPlaceholder) {
            return props.renderEmptyPlaceholder();
        }
        
        return null;
    }

    return (
        <FlatList
            ref={viewRef}
            showsVerticalScrollIndicator={false}
            keyExtractor={props.keyExtractor}
            data={props.data}
            renderItem={props.renderItem}
            onEndReached={() => {
                if (!fetchNext) {
                    setFetchNext(true);
                }
            }}
            refreshing={refreshing}
            ListHeaderComponent={(() => {
                if (props.renderListHeader) {
                    return props.renderListHeader();
                }
            })()}
            onEndReachedThreshold={0.5}
            contentContainerStyle={[
                {
                    paddingVertical: basePadding.md
                },
                props.contentContainerStyle
            ]}
            ListFooterComponent={(() => {
                if (!isFetching) {
                    return null;
                }

                return (
                    <View
                        style={{
                            padding: basePadding.md,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <ActivityIndicator
                            color={baseColor.three}
                            size={baseIcon.xl}
                        />
                    </View>
                )

            })()}
            scrollEventThrottle={props.scrollEventThrottle}
            refreshControl={renderRefreshControl()}
            numColumns={props.numColumns}
            columnWrapperStyle={props.columnWrapperStyle}
        />
    )
}

export default CustomFlatListWithFetch;
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import {
    baseColor,
    baseBorderRadius,
    dimension,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseIcon
} from '../../styles/base';
import CustomAbsoluteImage from './CustomAbsoluteImage';
import CustomMetaData, { CustomMetaDataBreaker } from './CustomMetaData';

const VideoSliderItem = (props) => {
    return (
        <TouchableOpacity
            style={[
                {
                    backgroundColor: baseColor.one,
                    borderRadius: baseBorderRadius.lg,
                    width: dimension.width * 0.7,
                    overflow: 'hidden'
                },
                props.containerStyle
            ]}
            onPress={props.onPress}
            delayPressIn={50}
        >
            <CustomAbsoluteImage
                source={{uri: props.data.thumbnail}}
                containerStyle={{
                    borderBottomStartRadius: 0,
                    borderBottomEndRadius: 0
                }}
            />
            <View
                style={{
                    padding: basePadding.sm,
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    flex: 1
                }}
            >
                <View
                    style={{
                        flexDirection: 'column'
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                        numberOfLines={2}
                        ellipsizeMode={'tail'}  
                    >
                        {props.data.title}
                    </Text>
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.sm,
                            fontFamily: baseFontFamily.regular
                        }}
                        numberOfLines={2}
                        ellipsizeMode={'tail'}
                    >
                        {props.data.description}
                    </Text>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingTop: basePadding.xs
                    }}
                >
                    <CustomMetaData
                        iconName={'clock'}
                        label={props.data.total_time}
                    />
                    <CustomMetaDataBreaker />
                    <CustomMetaData
                        iconName={'video'}
                        label={props.data.total_video}
                    />
                </View>
            </View>
            {
                !props.hideUserInfo ?
                <TouchableOpacity
                    style={{
                        borderTopWidth: 0.5,
                        borderTopColor: 'rgba(0, 0, 0, 0.1)',
                        flexDirection: 'row',
                        alignItems: 'center',
                        padding: basePadding.sm
                    }}
                    onPress={props.onUserPress}
                >
                    <Image
                        source={{uri: props.data.created_by.avatar}}
                        style={{
                            height: baseIcon.xl,
                            width: baseIcon.xl,
                            backgroundColor: baseColor.two,
                            borderRadius: baseIcon.xl / 2
                        }}
                    />
                    <View
                        style={{
                            paddingLeft: basePadding.sm,
                            flex: 1
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.four,
                                fontSize: baseFontSize.sm,
                                fontFamily: baseFontFamily.bold
                            }}
                            numberOfLines={1}
                            ellipsizeMode={'tail'}
                        >
                            {props.data.created_by.fullname}
                        </Text>
                        <Text
                            style={{
                                color: baseColor.five,
                                fontSize: baseFontSize.xs,
                                fontFamily: baseFontFamily.regular
                            }}
                            numberOfLines={1}
                            ellipsizeMode={'tail'}
                        >
                            {props.data.created_by.job_description ? props.data.created_by.job_description : 'Chưa có thông tin công việc'}
                        </Text>
                    </View>
                </TouchableOpacity>
                :
                null
            }
        </TouchableOpacity>
    )
}

export default VideoSliderItem;
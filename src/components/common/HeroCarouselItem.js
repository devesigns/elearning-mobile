import _ from 'lodash';
import React, { memo } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import {
    baseColor,
    baseBorderRadius,
    basePadding,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';
import CustomAbsoluteImage from './CustomAbsoluteImage';

const HeroCarouselItem = memo((props) => {
    return (
        <TouchableOpacity
            style={{
                width: '100%',
                backgroundColor: baseColor.one,
                borderRadius: baseBorderRadius.lg,
                overflow: 'hidden'
            }}
            onPress={props.onPress}
        >
            <CustomAbsoluteImage
                source={{uri: props.data.thumbnail}}
                containerStyle={{
                    borderBottomStartRadius: 0,
                    borderBottomEndRadius: 0
                }}
            />
            <View
                style={{
                    flexDirection: 'column',
                    padding: basePadding.sm
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold
                    }}
                    numberOfLines={1}
                    ellipsizeMode={'tail'}
                >
                    {props.data.title}
                </Text>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular
                    }}
                    numberOfLines={1}
                    ellipsizeMode={'tail'}
                >
                    {props.data.description}
                </Text>
            </View>
        </TouchableOpacity>
    )
}, (prevProps, nextProps) => {
    return _.isEqual(prevProps, nextProps);
})

export default HeroCarouselItem;
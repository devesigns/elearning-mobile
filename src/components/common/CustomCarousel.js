import React, { useState } from 'react';
import {
    View
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {
    dimension,
    baseColor,
    basePadding
} from '../../styles/base';

const CustomCarousel = (props) => {
    const [activeSlideIndex, setActiveSlideIndex] = useState(0);

    return (
        <Carousel
            data={props.data}
            renderItem={props.renderItem}
            sliderWidth={dimension.width}
            itemWidth={dimension.width * 0.8}
            onSnapToItem={(index) => {
                setActiveSlideIndex(index);
            }}
            layout={'default'}
            autoplay
            loop
        />
    )
}

export default CustomCarousel;
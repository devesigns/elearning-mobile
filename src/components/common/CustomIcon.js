import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { baseColor } from '../../styles/base';

const CustomIcon = (props) => {
    return (
        <Icon
            style={[
                styles.iconStyle,
                props.style
            ]}
            name={props.name}
            size={props.size}
            light
        />
    );
}

const styles = {
    iconStyle: {
        color: baseColor.four
    }
};

export default CustomIcon;
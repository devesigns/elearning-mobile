import React from 'react';
import {
    View,
    FlatList
} from 'react-native';
import {
    basePadding
} from '../../styles/base';
import { HorizontalSliderPlaceholder } from './Placeholder';

const ItemSeparator = (props) => {
    return (
        <View
            style={{
                width: basePadding.md
            }}
        />
    )
}

const HorizontalSlider = (props) => {
    if (props.data === null) {
        return (
            <View
                style={props.contentContainerStyle}
            >
                <HorizontalSliderPlaceholder />
            </View>
        )
    }

    return (
        <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={props.keyExtractor}
            data={props.data}
            renderItem={props.renderItem}
            ItemSeparatorComponent={props.noSeparator ? null : ItemSeparator}
            contentContainerStyle={props.contentContainerStyle}
        />
    )
}

export default HorizontalSlider;
import _ from 'lodash';
import React, { useState, useEffect, Fragment } from 'react';
import { 
    Platform
} from 'react-native';
import YouTube from 'react-native-youtube';
import { dimension } from '../../styles/base';

const YouTubeVideoPlayer = (props) => {
    const [vidRef, setVidRef] = useState(vidRef);
    const [status, setStatus] = useState(null);
    const [willRender, setWillRender] = useState(false);

    /**
     * Methods
     */

    const getCurrentTime = async () => {
        let currentTime = await vidRef.getCurrentTime();
        return currentTime;
    }

    if (props.setGetCurrentTime) {
        props.setGetCurrentTime(getCurrentTime)
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (status === 'ended') {
            props.onNextVideo(getCurrentTime());
        }

        if (status === 'playing' && props.startTime && props.startTime > 0) {
            vidRef.seekTo(props.startTime);
        }

        // if (status === 'playing') {
        //     vidRef.seekTo(30);
        // }
    }, [status])

    useEffect(() => {
        if (props.source.uri) {
            setTimeout(() => {
                setWillRender(true);
            }, 500);
        }
    }, [props.source])

    /**
     * Render methods
     */

    if (!willRender) {
        return null;
    }

    return (
        <Fragment>
            <YouTube
                ref={(ref) => {
                    setVidRef(ref);
                }}
                apiKey={'AIzaSyCxbWXS6FFISuNTlvWaWnHTptolnEZhexw'}
                videoId={props.source.uri} // The YouTube video ID
                play={true} // control playback of video with true/false
                fullscreen={false} // control whether the video should play in fullscreen or inline
                loop={false} // control whether the video should loop when ended
                // onReady={e => this.setState({ isReady: true })}
                onChangeState={(event) => {
                    setStatus(event.state);
                }}
                // onChangeQuality={e => this.setState({ quality: e.quality })}
                onError={e => console.log(e)}
                style={{ 
                    alignSelf: 'stretch',
                    width: dimension.width,
                    height: dimension.width * 9 / 16
                }}
            />
        </Fragment>
    )
}

export default YouTubeVideoPlayer;
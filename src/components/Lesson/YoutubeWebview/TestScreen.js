import React, { useState, useEffect } from 'react';
import {
    View
} from 'react-native';
import { WebView } from 'react-native-webview';
import {
    baseColor,
    dimension
} from '../../../styles/base';
import { YouTubeHTML } from './TestHtml';

const TestScreen = (props) => {
    const [videoId, setVideoId] = useState('esFNw3YHE5A');

    /**
     * UseEffect
     */

    /**
     * Render methods
     */

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    width: dimension.width,
                    height: dimension.width * 9 / 16
                }}
            >
                <WebView
                    style={{
                        width: '100%',
                        height: '100%',
                        backgroundColor: baseColor.two
                    }}
                    source={{html: YouTubeHTML(videoId)}}
                    originWhitelist={['*']}
                    onMessage={(event) => {
                        console.log(event.nativeEvent);

                        // Video end
                        if (event.nativeEvent.data === '0') {
                            setVideoId('9vktwC456Po');
                        }
                    }}
                />
            </View>
        </View>
    )
}

export default TestScreen;
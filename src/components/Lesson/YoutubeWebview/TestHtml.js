import { dimension } from "../../../styles/base"

export const YouTubeHTML = (videoId) => {
    return (
        `
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
            </head>
            <body>
                <div>
                    <style>
                        html, body {
                            margin: 0;
                            padding: 0;
                        }
                    </style>
                    <div id="player"></div>
                    <script>
                        // 2. This code loads the IFrame Player API code asynchronously.
                        var tag = document.createElement('script');
                        console.log('Hello');
                        tag.src = "https://www.youtube.com/iframe_api";
                        setTimeout(() => {
                            var firstScriptTag = document.getElementsByTagName('script')[0];
                            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                        }, 1000);

                        // 3. This function creates an <iframe> (and YouTube player)
                        //    after the API code downloads.
                        var player;
                        function onYouTubeIframeAPIReady() {
                            player = new YT.Player('player', {
                                height: '100%',
                                width: '100%',
                                videoId: '${videoId}',
                                events: {
                                    'onReady': onPlayerReady,
                                    'onStateChange': onPlayerStateChange
                                },
                                playerVars: {
                                    autoplay: 1,
                                    mute: 1,
                                    modestbranding: 1,
                                }
                            });
                        }

                        // 4. The API will call this function when the video player is ready.
                        function onPlayerReady(event) {
                            event.target.playVideo();
                        }

                        // 5. The API calls this function when the player's state changes.
                        function onPlayerStateChange(event) {
                            window.ReactNativeWebView.postMessage(event.data);
                        }

                        function stopVideo() {
                            player.stopVideo();
                        }
                    </script>
                </div>
            </body>
        `
    )
}
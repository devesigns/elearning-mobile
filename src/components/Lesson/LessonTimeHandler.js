import _ from 'lodash';
import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    historyUpdateLessonTime
} from '../../actions';

const LessonTimeHandler = (props) => {
    const [startTime, setStartTime] = useState(0);

    // const startTimer = () => {
    //     console.log('Start timer');
    //     setStartTime(Date.now());
    // }

    const endTimer = async (currentTime) => {
        let videoTime = await currentTime;
        console.log('Hi', videoTime);

        if (!props.currentSession.id || !videoTime) {
            return;
        }

        console.log('End timer');
        // let endTime = Date.now();
        // let videoTime = (endTime - startTime) / 1000; // in seconds

        // if (videoTime > 36000) {
        //     return;
        // }

        // if (videoTime > props.currentSession.duration) {
        //     videoTime = props.currentSession.duration;
        // }

        props.historyUpdateLessonTime(props.accessToken, {
            sessionId: props.currentSession.id,
            videoTime
        })
    }

    // if (props.setStartTimer) {
    //     props.setStartTimer(startTimer);
    // }

    if (props.setEndTimer) {
        props.setEndTimer(endTimer);
    }

    return null;
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        currentSession: lesson.currentSession
    }
};

export default connect(mapStateToProps, {
    historyUpdateLessonTime,
    lessonReducerUpdate
})(LessonTimeHandler);
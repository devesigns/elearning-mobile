import _ from 'lodash';
import React, { useState, useEffect, useRef, Fragment } from 'react';
import {
    View,
    PanResponder,
    Animated,
    TouchableOpacity,
    Text,
    StatusBar,
    ActivityIndicator
} from 'react-native';
import Video from 'react-native-video';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon,
    dimension
} from '../../styles/base';
import { moderateScale } from '../../utils';
import CustomIcon from '../common/CustomIcon';
import Orientation from 'react-native-orientation-locker';

// https://github.com/itsnubix/react-native-video-controls/blob/master/VideoPlayer.js
const VideoPlayer = (props) => {
    const vidRef = useRef(null);
    const [showOnStart, setShowOnStart] = useState(false);
    const [paused, setPaused] = useState(false);
    const [repeat, setRepeat] = useState(false);
    const [volume, setVolume] = useState(1);
    const [muted, setMuted] = useState(false);
    const [title, setTitle] = useState('');
    const [rate, setRate] = useState(1);
    const [isFullscreen, setIsFullscreen] = useState(false);
    const [showTimeRemaining, setShowTimeRemaining] = useState(true);
    const [lastScreenPress, setLastScreenPress] = useState(0);
    const [seekerFillWidth, setSeekerFillWidth] = useState(0);
    const [showControls, setShowControls] = useState(showOnStart);
    const [volumePosition, setVolumePosition] = useState(0);
    const [volumeOffset, setVolumeOffset] = useState(0);
    const [seeking, setSeeking] = useState(false);
    const [loading, setLoading] = useState(false);
    const [currentTime, setCurrentTime] = useState(0);
    const [error, setError] = useState(false);

    const [duration, setDuration] = useState(0);
    const [seekerWidth, setSeekerWidth] = useState(0);
    const [controlTimeout, setControlTimeout] = useState(null);
    const [animControl, setAnimControl] = useState(new Animated.Value(0));

    let player = {
        controlTimeoutDelay: props.controlTimeout || 30000,
        volumePanResponder: PanResponder,
        // seekPanResponder: PanResponder,
        volumeWidth: 150,
        iconOffset: 0,
        ref: Video,
    }
    
    let mounted = false;

    /**
     * When load starts we display a loading icon
     * and show the controls.
     */
    const onLoadStart = () => {
        console.log('Load start');
        setLoading(true);
    }

    /**
     * When load is finished we hide the load icon
     * and hide the controls. We also set the
     * video duration.
     *
     * @param {object} data The video meta data
     */
    const onLoad = (data = {}) => {
        console.log('Load done');
        setLoading(false);
        setPaused(false);
        setDuration(data.duration);
            
        if (props.startTime && props.startTime > 0) {
            seekTo(props.startTime);
        }
        
        if (showControls) {
            _setControlTimeout();
        }
    }

    /**
     * For onprogress we fire listeners that
     * update our seekbar and timer.
     *
     * @param {object} data The video meta data
     */
    const onProgress = (data = {}) => {
        setCurrentTime(data.currentTime);

        if (!seeking) {
            const targetFillWidth = calculateFillWidthWithTime(data.currentTime);
            setSeekerPosition(targetFillWidth);
        }
    }

    /**
     * On Buffer
     */

    const onBuffer = () => {
        // setLoading(true);
        console.log('Buffer');
    }

    /**
     * On Playback change
     */

    const onPlaybackStalled = () => {
        setLoading(true);
    }

    const onPlaybackResume = () => {
        setLoading(false);
    }

    /**
     * It is suggested that you override this
     * command so your app knows what to do.
     * Either close the video or go to a
     * new page.
     */
    const onEnd = () => {
        console.log('Video end');
        props.onNextVideo(currentTime);
    }

    /**
     * Set the error state to true which then
     * changes our renderError function
     *
     * @param {object} error  Err obj returned from <Video> component
     */
    const onError = (error) => {
        setError(true);
        setLoading(false);
    }

    /**
     * This is a single and double tap listener
     * when the user taps the screen anywhere.
     * One tap toggles controls, two toggles
     * fullscreen mode.
     */
    const onScreenTouch = () => {
        const time = new Date().getTime();
        const delta = time - lastScreenPress;

        if (delta < 300) {
            toggleFullscreen(!isFullscreen);
        }

        toggleControls();
        setLastScreenPress(time);
    }

    /**
    | -------------------------------------------------------
    | Methods
    | -------------------------------------------------------
    |
    | These are all of our functions that interact with
    | various parts of the class. Anything from
    | calculating time remaining in a video
    | to handling control operations.
    |
    */

    /**
     * Animation to hide controls. We fade the
     * display to 0 then move them off the
     * screen so they're not interactable
     */
    const hideControlAnimation = () => {
        Animated.timing(
            animControl,
            {
                toValue: 0,
                useNativeDriver: true
            }
        ).start();
    }

    /**
     * Animation to show controls...opposite of
     * above...move onto the screen and then
     * fade in.
     */
    const showControlAnimation = () => {
        Animated.timing(
            animControl,
            {
                toValue: 1,
                useNativeDriver: true
            }
        ).start();
    }

    /**
     * Set a timeout when the controls are shown
     * that hides them after a length of time.
     * Default is 15s
     */
    const _setControlTimeout = () => {
        return setTimeout(() => {
            if (!seeking) {
                hideControls();
            } else {
                resetControlTimeout();
            }
        }, player.controlTimeoutDelay)
    }
    
    /**
     * Clear the hide controls timeout.
     */
    const clearControlTimeout = () => {
        clearTimeout(controlTimeout);
        setControlTimeout(null);
    }

    /**
     * Reset the timer completely
     */
    const resetControlTimeout = () => {
        clearControlTimeout();
        setControlTimeout();
    }

    /**
     * Function to hide the controls. Sets our
     * state then calls the animation.
     */
    const hideControls = () => {
        if (mounted) {
            hideControlAnimation();
            setShowControls(false);
        }
    }

    /**
     * Function to toggle controls based on
     * current state.
     */
    const toggleControls = () => {
        if (!showControls) {
            showControlAnimation();
            setControlTimeout(_setControlTimeout());
        }
        else {
            hideControlAnimation();
            clearControlTimeout();
        }

        setShowControls(!showControls);
    }

    /**
     * Start seeking
     */

    const startSeeking = (event, gestureState) => {
        clearControlTimeout();
        setSeeking(true);
    }

    /**
     * On seeking (onPanResponderMove)
     */

    const onSeeking = (event, gestureState) => {
        clearControlTimeout();
        let { targetFillWidth } = calculateFillWidthWithPageX(event.nativeEvent.pageX);
        setSeekerPosition(targetFillWidth);
    }
    
    /**
     * Toggle fullscreen changes resizeMode on
     * the <Video> component then updates the
     * isFullscreen state.
     */
    const toggleFullscreen = (nextVal, orientation) => {
        if (nextVal) {
            if (!orientation) {
                Orientation.lockToLandscape();
            }
            typeof props.onEnterFullscreen === 'function' && props.onEnterFullscreen();
        } else {
            if (!orientation) {
                Orientation.lockToPortrait();
            }
            typeof props.onEnterFullscreen === 'function' && props.onExitFullscreen();
        }

        setIsFullscreen(nextVal);
    }

    /**
     * Toggle playing state on <Video> component
     */
    const togglePlayPause = () => {
        if (!paused) {
            typeof props.onPause === 'function' && props.onPause();
        }
        else {
            typeof props.onPlay === 'function' && props.onPlay();
        }

        setPaused(!paused);
    }

    /**
     * Toggle between showing time remaining or
     * video duration in the timer control
     */
    const toggleTimer = () => {
        let state = this.state;
        state.showTimeRemaining = ! state.showTimeRemaining;
        this.setState( state );
    }

    /**
     * Calculate the time to show in the timer area
     * based on if they want to see time remaining
     * or duration. Formatted to look as 00:00.
     */
    const calculateTime = () => {
        if (showTimeRemaining) {
            const time = duration - currentTime;
            return `-${formatTime(time)}`;
        }

        return formatTime(currentTime);
    }
    
    /**
     * Format a time string as mm:ss
     *
     * @param {int} time time in milliseconds
     * @return {string} formatted time string in mm:ss format
     */
    const formatTime = (time = 0) => {
        time = Math.min(
            Math.max(time, 0),
            duration
        );

        let formattedHours = null;
        if (time / 3600 > 0) {
            formattedHours = _.padStart(Math.floor(time / 3600).toFixed(0), 2, 0);
        }
        let formattedMinutes = _.padStart(Math.floor(time / 60).toFixed(0), 2, 0);
        let formattedSeconds = _.padStart(Math.floor(time % 60).toFixed(0), 2, 0);

        if (time > 3600) {
            return `${formattedHours}:${formattedMinutes}:${formattedSeconds}`;
        }
        
        return `${formattedMinutes}:${formattedSeconds}`;
    }

    /**
     * Set the position of the seekbar's components
     * (both fill and handle) according to the
     * position supplied.
     *
     * @param {float} position position in px of seeker handle}
     */
    const setSeekerPosition = (targetFillWidth = 0) => {
        setSeekerFillWidth(targetFillWidth);
    }

    /**
     * Calculate the position that the seeker should be
     * at along its track.
     *
     * @return {float} position of seeker handle in px based on currentTime
     */
    const calculateFillWidthWithTime = (currentTime) => {
        if (!(currentTime && duration && seekerWidth)) {
            return 0;
        }

        return currentTime / duration * seekerWidth;
    }

    const calculateFillWidthWithPageX = (pageX) => {
        if (!(pageX && duration && seekerWidth)) {
            return 0;
        }

        let percent = (pageX - baseFontSize.xs * 6) / seekerWidth;
        if (percent < 0) {
            percent = 0;
        } else if (percent > 1) {
            percent = 1;
        }

        return {
            targetFillWidth:  percent * seekerWidth,
            targetTime: percent * duration
        }
    }

    /**
     * Seek to a time in the video.
     *
     * @param {float} time time to seek to in ms
     */
    const seekTo = (time = 0) => {
        vidRef.current.seek(time);
        setCurrentTime(time);
    }

    /**
     * Set the position of the volume slider
     *
     * @param {float} position position of the volume handle in px
     */
    const _setVolumePosition = (position = 0) => {
        position = constrainToVolumeMinMax(position);
        const volumePosition = position + player.iconOffset;
        const volumeFillWidth = position;

        const volumeTrackWidth = player.volumeWidth - volumeFillWidth;

        if (volumeFillWidth < 0) {
            state.volumeFillWidth = 0;
        }

        if (volumeTrackWidth > 150) {
            volumeTrackWidth = 150;
        }

        setVolumePosition(volumePosition);
        setVolumnFillWidth(volumeFillWidth);
        setVolumeTrackWidth(volumeTrackWidth);
    }

    /**
     * Constrain the volume bar to the min/max of
     * its track's width.
     *
     * @param {float} val position of the volume handle in px
     * @return {float} contrained position of the volume handle in px
     */
    const constrainToVolumeMinMax = (val = 0) => {
        if (val <= 0) {
            return 0;
        }
        else if (val >= player.volumeWidth + 9) {
            return player.volumeWidth + 9;
        }
        return val;
    }

    /**
     * Get the volume based on the position of the
     * volume object.
     *
     * @return {float} volume level based on volume handle position
     */
    const calculateVolumeFromVolumePosition = () => {
        return volumePosition / volumeWidth;
    }

    /**
     * Get the position of the volume handle based
     * on the volume
     *
     * @return {float} volume handle position in px based on volume
     */
    const calculateVolumePositionFromVolume = () => {
        return player.volumeWidth * volume;
    }

    /**
    | -------------------------------------------------------
    | React Component functions
    | -------------------------------------------------------
    |
    | Here we're initializing our listeners and getting
    | the component ready using the built-in React
    | Component methods
    |
    */

    useEffect(() => {
        if (props.isDismissed) {
            setPaused(true);
        }
    }, [props.isDismissed]);

    useEffect(() => {
        if (isFullscreen) {
            StatusBar.setHidden(true);
        } else {
            StatusBar.setHidden(false);
        }
    }, [isFullscreen])

    useEffect(() => {
        if (duration && seekerWidth) {
            setSeekPanResponder(PanResponder.create({
                // Ask to be the responder.
                onStartShouldSetPanResponder: (event, gestureState) => true,
                onMoveShouldSetPanResponder: (event, gestureState) => true,
                
                /**
                 * When we start the pan tell the machine that we're
                 * seeking. This stops it from updating the seekbar
                 * position in the onProgress listener.
                 */
                onPanResponderGrant: startSeeking,
        
                /**
                 * When panning, update the seekbar position, duh.
                 */
                onPanResponderMove: onSeeking,
        
                /**
                 * On release we update the time and seek to it in the video.
                 * If you seek to the end of the video we fire the
                 * onEnd callback
                 */
                onPanResponderRelease: (event, gestureState) => {
                    let { targetFillWidth, targetTime } = calculateFillWidthWithPageX(event.nativeEvent.pageX);
                    setSeekerPosition(targetFillWidth);
                    seekTo(targetTime);
                    setSeeking(false);
                }
            }))
        }
    }, [duration, seekerWidth])

    /**
     * To allow basic playback management from the outside
     * we have to handle possible props changes to state changes
     */
    // useEffect(() => {
    //     if (paused !== props.paused ) {
    //         setPaused(props.paused);
    //         // if (currentTime > 0) {
    //         //     setSeek(currentTime);
    //         // }
    //     }
    // }, [props.paused])

    /**
     * Upon mounting, calculate the position of the volume
     * bar based on the volume property supplied to it.
     */
    const onLayout = () => {
        const position = calculateVolumePositionFromVolume();
        _setVolumePosition(position);

        setVolumeOffset(position);
    }
    
    /**
     * Initialize the volume pan responder.
     */
    const initVolumePanResponder = () => {
        player.volumePanResponder = PanResponder.create({
            onStartShouldSetPanResponder: (event, gestureState) => true,
            onMoveShouldSetPanResponder: (event, gestureState) => true,
            onPanResponderGrant: (event, gestureState) => {
                clearControlTimeout();
            },

            /**
             * Update the volume as we change the position.
             * If we go to 0 then turn on the mute prop
             * to avoid that weird static-y sound.
             */
            onPanResponderMove: (event, gestureState) => {
                const position = volumeOffset + gestureState.dx;

                _setVolumePosition(position);
                const volume = calculateVolumeFromVolumePosition();
                const muted = false;

                if (volume <= 0) {
                    muted = true;
                }

                setVolume(volume);
                setMuted(muted);
            },

            /**
             * Update the offset...
             */
            onPanResponderRelease: (event, gestureState) => {
                const volumeOffset = volumePosition;
                setControlTimeout();
                setVolumeOffset(volumeOffset);
            }
        });
    }
    initVolumePanResponder();

    /**
     * Groups the top bar controls together in an animated
     * view and spaces them out.
     */
    const renderTopControls = () => {
        return (
            <View
                style={{
                    zIndex: 999,
                    position: 'absolute',
                    top: 0,
                    width: '100%',
                }}
            >
                {renderTopNavControl()}
            </View>
        );
    }

    /**
     * Back button control
     */
    const renderBack = () => {
        return renderControl(
            <Text>
                Back
            </Text>,
            onBack,
            {
                
            }
        );
    }

    const renderCenterControls = () => {
        return (
            <View
                pointerEvents={'box-none'}
                style={{
                    zIndex: 999,
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        width: '100%',
                        backgroundColor: 'rgba(0, 0, 0, 0.3)',
                        zIndex: -1
                    }}
                    onPress={onScreenTouch}
                    activeOpacity={1}
                />
                <TouchableOpacity
                    onPress={() => {
                        setPaused(!paused);
                    }}
                >
                    {
                        paused ?
                        <CustomIcon
                            name={'play'}
                            size={baseIcon.xxxxl}
                            style={{
                                color: baseColor.one
                            }}
                        />
                        :
                        <CustomIcon
                            name={'pause'}
                            size={baseIcon.xxxxl}
                            style={{
                                color: baseColor.one
                            }}
                        />
                    }
                </TouchableOpacity>
            </View>
        );
    }

    /**
     * Render bottom control group and wrap it in a holder
     */
    const renderBottomControls = () => {
        mounted = true;

        return (
            <View
                style={{
                    zIndex: 999,
                    position: 'absolute',
                    bottom: 0,
                    width: '100%'
                }}
            >
                {renderSeekbar()}
                {renderBottomNavControl()}
            </View>
        );
    }

    /**
     * Render the seekbar and attach its handlers
     */
    const renderSeekbar = () => {
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingHorizontal: basePadding.xs,
                    paddingVertical: basePadding.xs
                }}
            >
                <Text
                    style={{
                        color: baseColor.one,
                        fontSize: baseFontSize.xs,
                        fontFamily: baseFontFamily.regular,
                        width: baseFontSize.xs * 6,
                        textAlign: 'center'
                    }}
                >
                    {formatTime(currentTime)}
                </Text>
                <View
                    pointerEvents={'box-only'}
                    style={{
                        paddingVertical: basePadding.sm,
                        flex: 1
                    }}
                    { ...seekPanResponder.panHandlers }
                >
                    <View
                        style={{
                            backgroundColor: baseColor.one,
                            height: moderateScale(1),
                            width: '100%'
                        }}
                        onLayout={(event) => {
                            setSeekerWidth(event.nativeEvent.layout.width);
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: baseColor.four,
                                height: moderateScale(1),
                                width: seekerFillWidth,
                            }}
                        />
                        <View
                            style={[
                                {
                                    borderRadius: moderateScale(8 / 2),
                                    height: moderateScale(8),
                                    width: moderateScale(8),
                                    position: 'absolute',
                                    left: seekerFillWidth - moderateScale(4),
                                    backgroundColor: baseColor.four,
                                    top: moderateScale(- 8 / 2 + 0.5)
                                },
                                seeking ?
                                {
                                    borderRadius: moderateScale(16 / 2),
                                    height: moderateScale(16),
                                    width: moderateScale(16),
                                    position: 'absolute',
                                    left: seekerFillWidth - moderateScale(8),
                                    backgroundColor: baseColor.four,
                                    top: moderateScale(- 16 / 2 + 0.5)
                                }
                                :
                                null
                            ]}
                        />
                    </View>
                </View>
                <Text
                    style={{
                        color: baseColor.one,
                        fontSize: baseFontSize.xs,
                        fontFamily: baseFontFamily.regular,
                        width: baseFontSize.xs * 6,
                        textAlign: 'center'
                    }}
                >
                    {formatTime(duration)}
                </Text>
            </View>
        );
    }

    /**
     * Render the play/pause button and show the respective icon
     */
    const renderBottomNavControl = () => {
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingHorizontal: basePadding.md,
                    paddingBottom: basePadding.sm
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <TouchableOpacity
                        style={{
                            paddingRight: basePadding.md
                        }}
                        onPress={() => props.onPrevVideo(currentTime)}
                    >
                        <CustomIcon
                            name={'skip-back'}
                            size={baseIcon.xs}
                            style={{
                                color: baseColor.one
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            setPaused(!paused);
                        }}
                        style={{
                            paddingHorizontal: basePadding.md
                        }}
                    >
                        {
                            paused ?
                            <CustomIcon
                                name={'play'}
                                size={baseIcon.xs}
                                style={{
                                    color: baseColor.one
                                }}
                            />
                            :
                            <CustomIcon
                                name={'pause'}
                                size={baseIcon.xs}
                                style={{
                                    color: baseColor.one
                                }}
                            />
                        }
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            paddingLeft: basePadding.md
                        }}
                        onPress={() => props.onNextVideo(currentTime)}
                    >
                        <CustomIcon
                            name={'skip-forward'}
                            size={baseIcon.xs}
                            style={{
                                color: baseColor.one
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        toggleFullscreen(!isFullscreen);
                    }}
                >
                    {
                        isFullscreen ?
                        <CustomIcon
                            name={'minimize'}
                            size={baseIcon.xs}
                            style={{
                                color: baseColor.one
                            }}
                        />
                        :
                        <CustomIcon
                            name={'maximize'}
                            size={baseIcon.xs}
                            style={{
                                color: baseColor.one
                            }}
                        />
                    }
                </TouchableOpacity>
            </View>
        )
    }

    /**
     * Render the play/pause button and show the respective icon
     */
    const renderTopNavControl = () => {
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingHorizontal: basePadding.md,
                    paddingTop: basePadding.sm
                }}
            >
                <View
                    style={{
                        width: 10
                    }}
                />
                <TouchableOpacity
                    
                >
                    <CustomIcon
                        name={'list'}
                        size={baseIcon.xs}
                        style={{
                            color: baseColor.one
                        }}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    /**
     * Render our title...if supplied.
     */
    const renderTitle = () => {
        if (props.title) {
            return (
                <View style={{

                }}>
                    <Text
                        style={{

                        }}
                        numberOfLines={1}
                    >
                        {props.title || '' }
                    </Text>
                </View>
            );
        }

        return null;
    }

    /**
     * Show our timer.
     */
    const renderTimer = () => {
        return renderControl(
            <Text>
                {calculateTime()}
            </Text>,
            toggleTimer,
            {
                
            }
        );
    }

    /**
     * Show loading icon
     */
    const renderLoader = () => {
        if (loading) {
            return (
                <View
                    style={{
                        position: 'absolute',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        width: '100%',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <ActivityIndicator color={baseColor.three} />
                </View>
            );
        }
        return null;
    }

    const renderError = () => {
        if (error) {
            return (
                <View
                    style={{
                        
                    }}
                >
                    <Text
                        style={{
                        
                        }}
                    >
                        Video unavailable
                    </Text>
                </View>
            );
        }
        return null;
    }

    /**
     * Get our seekbar responder going
     */
    const [seekPanResponder, setSeekPanResponder] = useState({});

    if (props.setGetCurrentTime) {
        props.setGetCurrentTime(() => {
            return currentTime;
        })
    }

    if (!props.source) {
        return;
    }

    return (
        <View
            style={{
                zIndex: 999,
            }}
        >
            <TouchableOpacity
                style={[
                    {
                        paddingBottom: '56.25%', // by default ratio 16:9. This can be overridden on demand,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'transparent'
                    },
                    isFullscreen ? 
                    {
                        width: '100%',
                        height: dimension.width,
                        paddingBottom: 0
                    }
                    :
                    null,
                    props.style
                ]}    
                onPress={onScreenTouch}
                activeOpacity={1}
            >
                <Video
                    ref={vidRef}
                    source={props.source}
                    style={[
                        {
                            position: 'absolute',
                            top: 0,
                            bottom: 0,
                            left: 0,
                            right: 0,
                            width: '100%',
                            backgroundColor: baseColor.two
                        },
                        props.videoStyle
                    ]}
                    posterResizeMode={props.resizeMode ? props.resizeMode : 'contain'}
                    resizeMode={props.resizeMode ? props.resizeMode : 'contain'}
                    paused={paused}
                    muted={false}
                    rate={rate}
                    onLoadStart={onLoadStart}
                    onProgress={onProgress}
                    onError={onError}
                    onLoad={onLoad}
                    onEnd={onEnd}
                    onBuffer={onBuffer}
                    onPlaybackStalled={onPlaybackStalled}
                    onPlaybackResume={onPlaybackResume}
                />
            </TouchableOpacity>
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    width: '100%'
                }}
                pointerEvents={'box-none'}
            >
                {/* {renderError()} */}
                <Animated.View
                    style={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        width: '100%',
                        opacity: animControl
                    }}
                    pointerEvents={showControls ? 'auto' : 'none'}
                >
                    {
                        props.hideControls ?
                        null
                        :
                        <Fragment>
                            {renderCenterControls()}
                            {renderTopControls()}
                            {renderBottomControls()}
                        </Fragment>
                    }
                </Animated.View>
                {renderLoader()}
            </View>
        </View>
    )
}

export default VideoPlayer;

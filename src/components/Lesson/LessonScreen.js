import _ from 'lodash';
import React, { PureComponent } from 'react';
import {
    View,
    ActivityIndicator,
    SafeAreaView,
    TouchableOpacity,
    Platform,
    KeyboardAvoidingView
} from 'react-native';
import { connect } from 'react-redux';
import { SceneMap, TabView, TabBar } from 'react-native-tab-view';
import Orientation from 'react-native-orientation-locker';
import {
    lessonReducerUpdate,
    lessonGetLessonDetail,
    exerciseReducerUpdate,
    exerciseGetExercise,
    historyUpdateStatus
} from '../../actions';
import {
    baseColor,
    dimension,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon,
    baseMargin
} from '../../styles/base';
import LessonVideoPlayer from './LessonVideoPlayer';
import LessonVideoTab from './VideoTab/LessonVideoTab';
import LessonDiscussionTab from './DiscussionTab/LessonDiscussionTab';
import YouTubeVideoPlayer from './YoutubeVideoPlayer';
import LessonTimeHandler from './LessonTimeHandler';
import CustomIcon from '../common/CustomIcon';

class LessonScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            renderVideo: false,
            index: 0,
            videoSource: {
                type: 'normal',
                uri: ''
            }
        }

        this.startTimer = () => null;
        this.endTimer = () => null;
        this.getCurrentTime = () => null;
    }

    nextSession = (currentTime) => {
        this.endTimer(currentTime);

        if (!this.props.currentSession.id) {
            if (!_.isEmpty(this.props.sessions)) {
                this.props.lessonReducerUpdate('currentSession', {
                    id: this.props.sessions[0].id,
                    arrayId: 0,
                    src: this.props.sessions[0].video
                });
            }

            return;
        }

        if (!_.isEmpty(this.props.exercises)) {
            this.props.exerciseReducerUpdate('currentExerciseArrayId', 0);
            this.props.navigation.navigate('LessonExercise');
        } else {
            if (!this.props.sessions[this.props.currentSession.arrayId].completed) {
                this.props.historyUpdateStatus(this.props.accessToken, {
                    courseId: this.props.lessonForm.course,
                    materialType: 'session',
                    materialId: this.props.currentSession.id,
                    status: 'completed'
                })
            } else {
                console.log('Session already completed');
            }

            let nextSession = this.props.sessions[this.props.currentSession.arrayId + 1];
            if (nextSession) {
                console.log('Moving to next session', nextSession);
                this.props.lessonReducerUpdate('currentSession', {
                    id: nextSession.id,
                    arrayId: this.props.currentSession.arrayId + 1,
                    src: nextSession.video
                });
            }
        }
    }

    prevSession = (currentTime) => {
        this.endTimer(currentTime);
        let prevSession = this.props.sessions[this.props.currentSession.arrayId - 1];
        
        if (prevSession) {
            this.props.lessonReducerUpdate('currentSession', {
                id: prevSession.id,
                arrayId: this.props.currentSession.arrayId - 1,
                src: prevSession.video
            });
        }
    }

    setVideoURI = (uri) => {
        // Get video source
        let videoSource = {};
        if (uri && uri.includes('youtube')) {
            videoSource = {
                type: 'youtube',
                uri: uri.split('=')[1]
            }
        } else if (uri && uri.includes('youtu')) {
            videoSource = {
                type: 'youtube',
                uri: uri.split('/youtu.be/')[1]
            }
        } else {
            videoSource = {
                type: 'normal',
                uri
            }
        }

        console.log('Video source', videoSource);
        this.startTimer();

        this.setState({
            videoSource
        })
    }

    UNSAFE_componentWillMount = () => {
        this.props.lessonGetLessonDetail(this.props.accessToken, {
            lessonId: this.props.lessonId
        }, this.props.lessonType);
    }

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        if (this.props.currentSession.id != nextProps.currentSession.id) {
            this.setVideoURI(nextProps.currentSession.src);
            this.props.exerciseGetExercise(this.props.accessToken, {
                sessionId: nextProps.currentSession.id
            });
        }

        if (this.props.lessonId != nextProps.lessonId) {
            this.props.lessonGetLessonDetail(this.props.accessToken, {
                lessonId: nextProps.lessonId
            })
        }

        // If user just access a lesson, there's no currentSession
        // Load the introduction_video first
        // If there's no introduction_video, load the next session
        if (!nextProps.currentSession.id) {
            if (!_.isEmpty(nextProps.lessonForm.introduction_video)) {
                this.setVideoURI(nextProps.lessonForm.introduction_video);
            } else {
                this.nextSession();
            }
        }
        
        // If continued session from homepage
        if (nextProps.continuedSession.id) {
            let continuedSession = { ...nextProps.continuedSession };
            this.props.lessonReducerUpdate('continuedSession', {});
            this.props.lessonReducerUpdate('currentSession', continuedSession)
        }
    }

    componentDidUpdate = () => {
        if (this.props.exerciseComplete) {
            this.props.exerciseReducerUpdate('exerciseComplete', false);
            this.nextSession();
        }
    }

    componentWillUnmount = () => {
        this.props.lessonReducerUpdate('lessonForm', {});
        this.props.lessonReducerUpdate('currentSession', {
            id: 0,
            src: '',
        })
        console.log('Unmounting...');
        this.endTimer(this.getCurrentTime());
        Orientation.lockToPortrait();
        
    }

    /**
     * Render methods
     */

    renderTabBar = (props) => {
        return (
            <View>
                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.goBack(null);
                    }}
                    style={{
                        position: 'absolute',
                        left: 0,
                        top: 0,
                        bottom: 0,
                        height: '100%',
                        width: basePadding.sm * 2 + baseIcon.md,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: baseColor.three
                    }}
                >
                    <CustomIcon
                        name={'chevron-left'}
                        size={baseIcon.md}
                        style={{
                            color: baseColor.one,
                            width: '100%',
                            textAlign: 'center',
                            borderBottomWidth: 1,
                            borderBottomColor: baseColor.three
                        }}
                    />
                </TouchableOpacity>
                <TabBar
                    {...props}
                    labelStyle={{
                        textTransform: 'capitalize',
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.bold,
                        margin: 0
                    }}
                    activeColor={baseColor.three}
                    inactiveColor={baseColor.four}
                    indicatorStyle={{
                        backgroundColor: baseColor.three
                    }}
                    tabStyle={{
                        width: 'auto',
                        height: '100%',
                        minHeight: 0,
                        paddingHorizontal: basePadding.md,
                        paddingVertical: basePadding.sm
                    }}
                    contentContainerStyle={{
                        alignItems: 'center'
                    }}
                    style={{
                        backgroundColor: 'transparent',
                        marginLeft: basePadding.sm * 2 + baseIcon.md
                    }}
                />
                <View
                    style={{
                        borderBottomWidth: 0.5,
                        borderBottomColor: baseColor.two,
                        zIndex: 1
                    }}
                />
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    backgroundColor: baseColor.one
                }}
            >
                <KeyboardAvoidingView
                    behavior={Platform.OS === 'ios' ? 'padding' : null}
                    style={{
                        flex: 1
                    }}
                >
                    {
                        this.state.videoSource.type === 'normal' ?
                        <LessonVideoPlayer
                            source={this.state.videoSource}
                            onNextVideo={this.nextSession}
                            onPrevVideo={this.prevSession}
                            startTime={this.props.currentSession.pausedAt}
                            setGetCurrentTime={(func) => {
                                this.getCurrentTime = func;
                            }}
                        />
                        :
                        <YouTubeVideoPlayer
                            source={this.state.videoSource}
                            onNextVideo={this.nextSession}
                            onPrevVideo={this.prevSession}
                            startTime={this.props.currentSession.pausedAt}
                            setGetCurrentTime={(func) => {
                                this.getCurrentTime = func;
                            }}
                        />
                    }
                    {
                        !_.isEmpty(this.props.lessonForm) ?
                        <TabView
                            navigationState={{
                                index: this.state.index,
                                routes: [
                                    {
                                        key: 'video',
                                        title: 'Bài học'
                                    },
                                    {
                                        key: 'discussion',
                                        title: 'Thảo luận' 
                                    }
                                ]
                            }}
                            onIndexChange={(nextIndex) => {
                                this.setState({
                                    index: nextIndex
                                })
                            }}
                            renderScene={SceneMap({
                                video: LessonVideoTab,
                                discussion: LessonDiscussionTab
                            })}
                            initialLayout={{
                                width: dimension.width
                            }}
                            style={{
            
                            }}
                            tabStyle={{
                                backgroundColor: 'red'
                            }}
                            renderTabBar={this.renderTabBar}
                        />
                        :
                        <ActivityIndicator
                            color={baseColor.three}
                            size={baseIcon.xxxl}
                            style={{
                                marginTop: baseMargin.md
                            }}
                        />
                    }
                    <LessonTimeHandler
                        // setStartTimer={(func) => {
                        //     this.startTimer = func;
                        // }}
                        setEndTimer={(func) => {
                            this.endTimer = func;
                        }}
                    />
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = ({ auth, lesson, exercise }) => {
    return {
        accessToken: auth.accessToken,
        lessonId: lesson.lessonId,
        lessonType: lesson.lessonType,
        lessonForm: lesson.lessonForm,
        sessions: lesson.sessions,
        exercises: exercise.exercises,
        exerciseComplete: exercise.exerciseComplete,
        currentSession: lesson.currentSession,
        continuedSession: lesson.continuedSession
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonGetLessonDetail,
    exerciseReducerUpdate,
    exerciseGetExercise,
    historyUpdateStatus
})(LessonScreen);
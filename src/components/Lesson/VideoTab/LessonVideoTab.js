import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    FlatList,
    TouchableOpacity,
    ActivityIndicator,
    Image
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    lessonGetSession,
    exerciseReducerUpdate,
    coacherReducerUpdate,
    accountUpdatePlan
} from '../../../actions';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseIcon,
    baseMargin
} from '../../../styles/base';
import CustomMetaData, { CustomMetaDataBreaker } from '../../common/CustomMetaData';
import LessonActionMenu from './LessonActionMenu';
import LessonVideoListItem from './LessonVideoListItem';
import CustomPageBreak from '../../common/CustomPageBreak';
import { moderateScale } from '../../../utils';
import SubscriptionModal from '../../Account/SubscriptionModal';

const LessonVideoTab = (props) => {
    const [lessonId, setLessonId] = useState(0);
    const [subscriptionModalVisible, setSubscriptionModalVisible] = useState(false);

    /**
     * Methods
     */

    const onRefresh = () => {
        props.lessonReducerUpdate('sessions', null);
    }

    const onSessionPress = (item, index) => {
        props.lessonReducerUpdate('currentSession', {
            id: item.id,
            arrayId: index,
            src: item.video
        });
    }

    const openSubscriptionModal = () => {
        setSubscriptionModalVisible(true);
    }

    const nextLesson = () => {
        props.lessonReducerUpdate('lessonId', props.lessonForm.next.id);
    }

    const prevLesson = () => {
        props.lessonReducerUpdate('lessonId', props.lessonForm.prev.id);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.lessonId != lessonId) {
            setLessonId(props.lessonId);
            onRefresh();
        }
    }, [props.lessonId, lessonId])

    useEffect(() => {
        if (lessonId) {
            props.lessonGetSession(props.accessToken, {
                lessonId,
                page: 1,
                perPage: 16
            });
        }
    }, [lessonId])

    /**
     * Render methods
     */
    
    const renderSessionItem = ({item, index}) => {
        return (
            <LessonVideoListItem
                data={item}
                onPress={() => onSessionPress(item, index)}
                openSubscriptionModal={() => openSubscriptionModal()}
                isPlaying={props.currentSession.id === item.id}
            />
        )
    }

    if (!lessonId) {
        return null;
    }

    return (
        <>
            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                {/* Header */}
                <View
                    style={{
                        padding: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.xxl,
                            fontFamily: baseFontFamily.bold,
                            paddingBottom: basePadding.xs
                        }}
                    >
                        {props.lessonForm.title}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingBottom: basePadding.md
                        }}
                    >
                        <CustomMetaData
                            iconName={'clock'}
                            label={props.lessonForm.total_time}
                        />
                        <CustomMetaDataBreaker />
                        <CustomMetaData
                            iconName={'video'}
                            label={props.lessonForm.total_video}
                        />
                    </View>
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            paddingBottom: basePadding.md
                        }}
                    >
                        {props.lessonForm.description}
                    </Text>
                    <LessonActionMenu />
                </View>
                {/* Coacher Info */}
                <CustomPageBreak />
                <View
                    style={{
                        padding: basePadding.md
                    }}
                >
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            props.coacherReducerUpdate('coacherForm', {
                                id: props.lessonForm.created_by.id
                            });
                            props.navigation.navigate('CoacherProfile');
                        }}
                    >
                        <Image
                            source={{uri: props.lessonForm.created_by.avatar}}
                            style={{
                                height: moderateScale(36),
                                width: moderateScale(36),
                                backgroundColor: baseColor.two,
                                borderRadius: moderateScale(36) / 2
                            }}
                        />
                        <View
                            style={{
                                paddingLeft: basePadding.sm,
                                flex: 1
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.bold
                                }}
                            >
                                {props.lessonForm.created_by.fullname}
                            </Text>
                            <Text
                                style={{
                                    color: baseColor.five,
                                    fontSize: baseFontSize.sm,
                                    fontFamily: baseFontFamily.regular
                                }}
                            >
                                {props.lessonForm.created_by.job_description ? props.lessonForm.created_by.job_description : 'Chưa có thông tin công việc'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <CustomPageBreak />
                {/* Session List */}
                {
                    props.sessions === null || props.sessions.length ?
                    <View
                        style={{
                            padding: basePadding.md
                        }}
                    >
                        {
                            _.isEmpty(props.sessions) ?
                            <ActivityIndicator
                                color={baseColor.three}
                                size={baseIcon.xxxl}
                                style={{
                                    marginBottom: baseMargin.md
                                }}
                            />
                            :
                            <FlatList
                                keyExtractor={(item, index) => {
                                    return 'session-' + item.id;
                                }}    
                                data={props.sessions}
                                renderItem={renderSessionItem}
                                extraData={props.currentSession}
                            />
                        }
                    </View>
                    :
                    null
                }
                {/* Footer */}
                {
                    !_.isEmpty(props.lessonForm.next) || !_.isEmpty(props.lessonForm.prev) ?
                    <>
                        <CustomPageBreak />
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                padding: basePadding.md
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'column',
                                    alignItems: 'flex-start',
                                    width: '45%',
                                    opacity: !_.isEmpty(props.lessonForm.prev) ? 1 : 0
                                }}
                                onPress={prevLesson}
                                disabled={!_.isEmpty(props.lessonForm.prev) ? false : true}
                            >
                                <Text
                                    style={{
                                        color: baseColor.five,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    Bài trước
                                </Text>
                                <Text
                                    style={{
                                        color: baseColor.four,
                                        fontSize: baseFontSize.sm,
                                        fontFamily: baseFontFamily.regular
                                    }}
                                    numberOfLines={2}
                                    ellipsizeMode={'tail'}
                                >
                                    {!_.isEmpty(props.lessonForm.prev) ? props.lessonForm.prev.title : ''}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'column',
                                    alignItems: 'flex-end',
                                    width: '45%',
                                    opacity: !_.isEmpty(props.lessonForm.next) ? 1 : 0
                                }}
                                onPress={nextLesson}
                                disabled={!_.isEmpty(props.lessonForm.next) ? false : true}
                            >
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    Bài kế
                                </Text>
                                <Text
                                    style={{
                                        color: baseColor.four,
                                        fontSize: baseFontSize.sm,
                                        fontFamily: baseFontFamily.regular
                                    }}
                                    numberOfLines={2}
                                    ellipsizeMode={'tail'}
                                >
                                    {!_.isEmpty(props.lessonForm.next) ? props.lessonForm.next.title : ''}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </>
                    :
                    null
                }
            </ScrollView>
            <SubscriptionModal 
                plan={props.user.plan}
                visible={subscriptionModalVisible}
                onRequestClose={() => {
                    setSubscriptionModalVisible(false);
                }}
                onSubmit={(type) => {
                    if (type !== props.user.plan.type) {
                        props.accountUpdatePlan(props.accessToken, {
                            type
                        })
                    }

                    setSubscriptionModalVisible(false);
                }}
            />
        </>
    )
}

const mapStateToProps = ({ auth, lesson, account }) => {
    return {
        accessToken: auth.accessToken,
        lessonId: lesson.lessonId,
        lessonForm: lesson.lessonForm,
        sessions: lesson.sessions,
        currentSession: lesson.currentSession,
        user: account.user
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonGetSession,
    exerciseReducerUpdate,
    coacherReducerUpdate,
    accountUpdatePlan
})(withNavigation(LessonVideoTab));

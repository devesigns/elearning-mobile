import React from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseBorderRadius,
    baseIcon
} from '../../../styles/base';
import CustomAbsoluteImage from '../../common/CustomAbsoluteImage';
import CustomMetaData from '../../common/CustomMetaData';
import CustomIcon from '../../common/CustomIcon';

const LessonVideoListItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'row',
                paddingBottom: basePadding.md,
            }}
            onPress={props.data.video ? props.onPress : props.openSubscriptionModal}
            delayPressIn={50}
        >
            <View
                style={{
                    width: '33.33%'
                }}
            >
                <CustomAbsoluteImage
                    source={{uri: props.data.thumbnail}}
                    containerStyle={{
                        borderRadius: baseBorderRadius.md
                    }}
                    renderOverlay={() => {
                        return (
                            <>
                            {
                                props.data.video && props.isPlaying ?
                                <View
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        bottom: 0,
                                        width: '100%',
                                        backgroundColor: 'rgba(0, 0, 0, 0.7)',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: baseBorderRadius.md
                                    }}
                                >
                                    <CustomIcon
                                        name={'play-circle'}
                                        size={baseIcon.xxl}
                                        style={{
                                            color: baseColor.one
                                        }}
                                    />
                                </View>
                                :
                                null
                            }
                            {
                                !props.data.video ?
                                <View
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        right: 0,
                                        bottom: 0,
                                        width: '100%',
                                        backgroundColor: 'rgba(0, 0, 0, 0.7)',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: baseBorderRadius.md
                                    }}
                                >
                                    <CustomIcon
                                        name={'lock'}
                                        size={baseIcon.xxl}
                                        style={{
                                            color: baseColor.one
                                        }}
                                    />
                                </View>
                                :
                                null
                            }
                            </>
                        )
                    }}
                />
            </View>
            <View
                style={{
                    width: '66.67%',
                    paddingLeft: basePadding.sm,
                    flexDirection: 'column'
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.xs
                    }}
                >
                    {props.data.title}
                </Text>
                <CustomMetaData
                    iconName={'clock'}
                    label={props.data.video_time}
                />
            </View>
        </TouchableOpacity>
    )
}

export default LessonVideoListItem;
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Share
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonLikeLesson,
    lessonBookmarkLesson
} from '../../../actions';
import CustomIcon from '../../common/CustomIcon';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    baseIcon,
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily
} from '../../../styles/base';

const LessonActionItem = (props) => {
    return (
        <TouchableOpacity
            style={[
                {
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                },
                props.containerStyle
            ]}
            onPress={props.onPress}
        >
            <CustomIcon
                name={props.iconName}
                size={baseIcon.xxl}
                style={{
                    color: baseColor.four
                }}
            />
            <Text
                style={{
                    paddingTop: basePadding.xs,
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular
                }}
            >
                {props.label}
            </Text>
        </TouchableOpacity>
    )
}

const LessonLikeButton = (props) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'
            }}
            onPress={props.onPress}
        >
            <Icon
                name={props.active ? 'thumbs-up' : 'thumbs-o-up'}
                size={baseIcon.xxl}
                style={{
                    color: props.active ? baseColor.three : baseColor.four
                }}
            />
            <Text
                style={{
                    paddingTop: basePadding.xs,
                    color: props.active ? baseColor.three : baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular
                }}
            >
                {props.label}
            </Text>
        </TouchableOpacity>
    )
}

const LessonBookmarkButton = (props) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'
            }}
            onPress={props.onPress}
        >
            <Icon
                name={props.active ? 'bookmark' : 'bookmark-o'}
                size={baseIcon.xxl}
                style={{
                    color: props.active ? baseColor.three : baseColor.four
                }}
            />
            <Text
                style={{
                    paddingTop: basePadding.xs,
                    color: props.active ? baseColor.three : baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular
                }}
            >
                Lưu lại
            </Text>
        </TouchableOpacity>
    )
}

const LessonActionMenu = (props) => {

    /**
     * Methods
     */

    const onShare = async () => {
        try {
            const result = await Share.share({
                message: 'https://google.com.vn'
            });
        
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    /**
     * Render methods
     */

    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'space-between'
            }}
        >
            <LessonLikeButton
                active={props.lessonForm.liked}
                label={props.lessonForm.total_like}
                onPress={() => {
                    props.lessonLikeLesson(props.accessToken, {
                        lessonId: props.lessonForm.id
                    })
                }}
            />
            <LessonBookmarkButton
                active={props.lessonForm.bookmarked}
                onPress={() => {
                    props.lessonBookmarkLesson(props.accessToken, {
                        lessonId: props.lessonForm.id
                    })
                }}
            />
            <LessonActionItem
                label={'Chia sẻ'}
                iconName={'share-2'}
                onPress={onShare}
            />
            <LessonActionItem
                label={'Tải xuống'}
                iconName={'download'}
                containerStyle={{
                    opacity: 0
                }}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        lessonForm: lesson.lessonForm
    }
}

export default connect(mapStateToProps, {
    lessonLikeLesson,
    lessonBookmarkLesson
})(LessonActionMenu);
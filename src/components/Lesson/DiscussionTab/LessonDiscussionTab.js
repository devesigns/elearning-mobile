import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    lessonGetComment,
    lessonPostComment
} from '../../../actions';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily,
} from '../../../styles/base';
import LessonCommentListItem from './LessonCommentListItem';
import LessonCommentInput from './LessonCommentInput';
import CustomFlatListWithFetch from '../../common/CustomFlatListWithFetch';

const LessonDiscussionTab = (props) => {
    const [lessonId, setLessonId] = useState(0);

    /**
     * Methods
     */

    const onRefresh = () => {
        props.lessonReducerUpdate('comments', null);
    }

    /**
     * UseEffect
     */
    
    useEffect(() => {
        if (props.lessonId != lessonId) {
            setLessonId(props.lessonId);
            onRefresh();
        }
    }, [props.lessonId, lessonId])

    /**
     * Render Methods
     */
    
    const renderCommentItem = ({item, index}) => {
        return (
            <LessonCommentListItem
                data={item}
                noBorder={index === props.comments.length - 1}
            />
        )
    }

    if (!lessonId) {
        return null;
    }

    return (
        <View
            style={{
                flex: 1
            }}
        >
            <CustomFlatListWithFetch
                keyExtractor={(item, index) => {
                    return 'comment-item-' + index
                }}
                data={props.comments}
                renderItem={renderCommentItem}
                fetchNextBatch={(page, perPage) => {
                    props.lessonGetComment(props.accessToken, {
                        lessonId,
                        page,
                        perPage
                    });
                }}
                onRefresh={onRefresh}
                renderListHeader={() => {
                    return (
                        <View
                            style={{
                                paddingBottom: basePadding.md,
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.lg,
                                    fontFamily: baseFontFamily.bold,
                                    paddingBottom: basePadding.xs
                                }}
                            >
                                {props.comments.length} bình luận
                            </Text>
                        </View>
                    )
                }}
                renderEmptyPlaceholder={() => {
                    return (
                        <ScrollView
                            contentContainerStyle={{
                                flexGrow: 1
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.regular,
                                    color: baseColor.four,
                                    padding: basePadding.md
                                }}
                            >
                                Chưa có bình luận nào!
                            </Text>
                        </ScrollView>
                    )
                }}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md
                }}
                perPage={16}
            />
            <LessonCommentInput
                onSubmit={(newComment) => {
                    props.lessonPostComment(props.accessToken, {
                        lessonId: props.lessonId,
                        content: newComment
                    })
                }}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, lesson }) => {
    return {
        accessToken: auth.accessToken,
        lessonId: lesson.lessonId,
        comments: lesson.comments
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    lessonGetComment,
    lessonPostComment
})(LessonDiscussionTab);
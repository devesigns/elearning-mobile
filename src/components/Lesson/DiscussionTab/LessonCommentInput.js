import React, { useState } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseBorderRadius,
    baseFontSize,
    baseFontFamily,
    baseIcon,
    dimension
} from '../../../styles/base';
import CustomIcon from '../../common/CustomIcon';

const LessonCommentInput = (props) => {
    const [newComment, setNewComment] = useState('');

    return (
        <View
            style={{
                width: dimension.width,
                padding: basePadding.sm,
                backgroundColor: baseColor.two,
            }}
        >
            <View
                style={{
                    borderRadius: baseBorderRadius.lg,
                    backgroundColor: baseColor.one,
                    padding: basePadding.xs,
                    paddingHorizontal: basePadding.sm,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <TextInput
                    value={newComment}
                    onChangeText={(value) => setNewComment(value)}
                    placeholder={'Viết bình luận...'}
                    style={{
                        color: baseColor.five,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        flex: 1,
                        padding: 0,
                        maxHeight: (baseFontSize.md * 1.4 * 3)
                    }}
                    multiline
                    scrollEnabled={true}
                />
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss();
                        setNewComment('');
                        props.onSubmit(newComment);
                    }}
                    style={{
                        paddingLeft: basePadding.sm
                    }}
                >
                    <CustomIcon
                        name={'send'}
                        style={{
                            color: baseColor.three
                        }}
                        size={baseIcon.md}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default LessonCommentInput;
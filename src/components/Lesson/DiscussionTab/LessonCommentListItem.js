import React, { memo } from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontFamily,
    baseFontSize,
    baseIcon,
    baseMargin
} from '../../../styles/base';
import CustomMetaData from '../../common/CustomMetaData';

const LessonCommentListItem = memo((props) => {
    return (
        <View
            style={{
                flexDirection: 'row',
                borderBottomWidth: props.noBorder ? 0 : 0.5,
                borderBottomColor: baseColor.five,
                paddingBottom: basePadding.md,
                marginBottom: baseMargin.md
            }}
        >
            <Image
                source={{uri: props.data.commenter.avatar}}
                style={{
                    height: baseIcon.xxxxl,
                    width: baseIcon.xxxxl,
                    borderRadius: baseIcon.xxxxl / 2,
                    backgroundColor: baseColor.two
                }}
            />
            <View
                style={{
                    paddingLeft: basePadding.md,
                    flexDirection: 'column',
                    flex: 1
                }}
            >
                <View
                    style={{
                        paddingBottom: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        {props.data.commenter.fullname}
                    </Text>
                    <Text
                        style={{
                            color: baseColor.five,
                            fontSize: baseFontSize.sm,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        Thành viên
                    </Text>
                </View>
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        paddingBottom: basePadding.md
                    }}
                >
                    {props.data.content}
                </Text>
                <CustomMetaData
                    iconName={'clock'}
                    label={props.data.commented_at}
                />
            </View>
        </View>
    )
})

export default LessonCommentListItem;
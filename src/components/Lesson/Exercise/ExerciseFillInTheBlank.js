import _ from 'lodash';
import React, { useState, useEffect, useRef, Fragment } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput
} from 'react-native';
import {
    baseColor,
    basePadding,
    dimension,
    baseFontSize,
    baseFontFamily,
    baseMargin
} from '../../../styles/base';

const ExerciseFillInTheBlank = (props) => {
    const inputRef = useRef(null);
    const [questionChunks, setQuestionChunks] = useState([]);
    const [answers, setAnswers] = useState(new Array(Object.keys(props.data.content.slots).length).fill(''));
    const [selectedInputFieldId, setSelectedInputFieldId] = useState(0);

    /**
     * Methods
     */

    const getQuestionChunks = () => {
        let res = props.data.question.split(' ');
        let questionChunks = [];
        let chunk = '';
        for (let i = 0; i < res.length; i++) {
            if (res[i][0] === '@') {
                questionChunks.push(chunk);
                chunk = '';
            } else {
                chunk += res[i] + ' ';
            }
        }

        setQuestionChunks(questionChunks);
    }

    props.setGetAnswer(() => {
        return answers;
    })

    /**
     * UseEffect
     */

    useEffect(() => {
        if (!_.isEmpty(props.data)) {
            getQuestionChunks();
        }
    }, [props.data]);

    /**
     * Render methods
     */

    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center'
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.xxl,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                Điền vào chỗ còn trống
            </Text>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                    marginBottom: baseMargin.md
                }}
            >
                {(() => {
                    return questionChunks.map((item, index) => {
                        return (
                            <Fragment
                                key={'question-fill-in-the-blank-' + index}
                            >
                                {item}
                                <Text
                                    style={{
                                        backgroundColor: baseColor.three,
                                    }}
                                    onPress={() => {
                                        setSelectedInputFieldId(index);
                                        inputRef.current.focus();
                                    }}
                                >
                                    {answers[index] ? answers[index] : '          '}
                                </Text>
                                {' '}
                            </Fragment>
                        )
                    })
                })()}
            </Text>
            <TextInput
                ref={inputRef}
                value={answers[selectedInputFieldId]}
                onChangeText={(value) => {
                    let newAnswers = [ ...answers ];
                    newAnswers[selectedInputFieldId] = value;

                    setAnswers(newAnswers);
                }}
                style={{
                    position: 'absolute',
                    opacity: 0,
                    top: 0,
                    left: 0
                }}
            />
        </View>
    )
}

export default ExerciseFillInTheBlank;
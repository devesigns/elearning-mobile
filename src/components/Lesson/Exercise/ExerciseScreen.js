import _ from 'lodash';
import React, { useState, useEffect, Fragment } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    LayoutAnimation,
    KeyboardAvoidingView,
    SafeAreaView,
    Platform
} from 'react-native';
import { connect } from 'react-redux';
import {
    exerciseReducerUpdate,
    exerciseSetComplete,
    historyUpdateStatus
} from '../../../actions';
import {
    baseColor,
    basePadding,
    baseFontFamily,
    baseFontSize,
} from '../../../styles/base';
import { isIphoneX, moderateScale } from '../../../utils';
import ExerciseMultipleChoice from './ExerciseMultipleChoice';
import ExerciseAnswer from './ExerciseAnswer';
import ExerciseFillInTheBlank from './ExerciseFillInTheBlank';

const ExerciseScreen = (props) => {
    const [step, setStep] = useState(0);
    const [answer, setAnswer] = useState(null);
    let getAnswer = null;

    let exerciseForm = {};
    if (props.exercises) {
        exerciseForm = props.exercises[props.currentExerciseArrayId];
    }

    /**
     * Methods
     */

    const checkAnswer = () => {
        setAnswer(getAnswer());
        setStep(1);
    }

    const onNext = () => {
        if (!exerciseForm.completed) {
            props.historyUpdateStatus(props.accessToken, {
                courseId: props.lessonForm.course,
                materialType: 'exercise',
                materialId: exerciseForm.id,
                status: 'completed'
            })
        } else {
            console.log('Exercise already completed');
        }

        let nextExercise = props.exercises[props.currentExerciseArrayId + 1];

        if (nextExercise) {
            props.exerciseReducerUpdate('currentExerciseArrayId', props.currentExerciseArrayId + 1);
        } else {
            props.exerciseSetComplete();
            props.navigation.goBack();
        }
    }

    const onBackToQuestion = () => {
        setStep(0);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        setStep(0);
    }, [props.currentExerciseArrayId]);

    useEffect(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    }, [step])

    /**
     * Render Methods
     */

    const renderHeader = () => {
        return (
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.md,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    BÀI TẬP {props.currentExerciseArrayId + 1}
                </Text>
            </View>
        )
    }

    const renderExerciseContent = () => {
        console.log(exerciseForm);
        switch (exerciseForm.type) {
            case 'choices': {
                return (
                    <ExerciseMultipleChoice
                        data={exerciseForm}
                        setGetAnswer={(func) => {
                            getAnswer = func;
                        }}
                    />
                )
            }
            case 'slots_filling': {
                return (
                    <ExerciseFillInTheBlank
                        data={exerciseForm}
                        setGetAnswer={(func) => {
                            getAnswer = func;
                        }}
                    />
                )
            }
            default: break;
        }
    }

    const renderAnswerContent = () => {
        let correct = true;

        switch (exerciseForm.type) {
            case 'choices': {
                correct = answer && answer.toString() === exerciseForm.content.right_answer;

                break;
            }
            case 'slots_filling': {
                let keys = Object.keys(exerciseForm.content.slots);
                for (let i = 0; i < keys.length; i++) {
                    if (answer[i] !== exerciseForm.content.slots[keys[i]]) {
                        correct = false;
                    }
                }

                break;
            }
            default: break;
        }

        return (
            <ExerciseAnswer
                correct={correct}
                explanation={exerciseForm.explanation}
            />
        )
    }

    const renderFooter = () => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'flex-end'
                }}
            >
                <TouchableOpacity
                    onPress={checkAnswer}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        Xem đáp án
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    const renderAnswerFooter = () => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}
            >
                <TouchableOpacity
                    onPress={onBackToQuestion}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        Trả lời lại
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={onNext}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        Tiếp tục
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    if (_.isEmpty(exerciseForm)) {
        return null;
    }

    return (
        <SafeAreaView
            style={{
                flex: 1
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1,
                    backgroundColor: baseColor.one
                }}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1,
                        padding: basePadding.md
                    }}
                >
                    {renderHeader()}
                    {
                        step === 0 ?
                        <Fragment>
                            {renderExerciseContent()}
                            {renderFooter()}
                        </Fragment>
                        :
                        <Fragment>
                            {renderAnswerContent()}
                            {renderAnswerFooter()}
                        </Fragment>
                    }
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, lesson, exercise }) => {
    return {
        accessToken: auth.accessToken,
        lessonForm: lesson.lessonForm,
        exercises: exercise.exercises,
        currentExerciseArrayId: exercise.currentExerciseArrayId
    }
}

export default connect(mapStateToProps, {
    exerciseReducerUpdate,
    exerciseSetComplete,
    historyUpdateStatus
})(ExerciseScreen);
import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { moderateScale } from '../../../utils';
import {
    baseColor,
    basePadding,
    dimension,
    baseFontSize,
    baseFontFamily,
    baseMargin
} from '../../../styles/base';

const OptionItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'row',
                width: '100%',
                marginBottom: baseMargin.md
            }}
            onPress={props.onPress}
        >
            <View
                style={[
                    {
                        height: moderateScale(36),
                        width: moderateScale(36),
                        borderRadius: moderateScale(36 /2),
                        borderColor: baseColor.five,
                        borderWidth: 0.5,
                        justifyContent: 'center',
                        alignItems: 'center'
                    },
                    props.selected ?
                    {
                        borderWidth: 0,
                        backgroundColor: baseColor.three
                    }
                    :
                    null
                ]}
            >
                <Text
                    style={{
                        color: props.selected ? baseColor.one : baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold,
                    }}
                >
                    {props.data.key}
                </Text>
            </View>
            <Text
                style={{
                    flex: 1,
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                    paddingLeft: basePadding.md,
                    paddingTop: (moderateScale(36) - baseFontSize.md * 1.4) / 2
                }}
            >
                {props.data.value}
            </Text>
        </TouchableOpacity>
    )
}

const ExerciseMultipleChoice = (props) => {
    const [selectedAnswer, setSelectedAnswer] = useState(null);

    props.setGetAnswer(() => {
        return selectedAnswer;
    })

    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center'
            }}
        >
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.xxl,
                    fontFamily: baseFontFamily.bold,
                    paddingBottom: basePadding.md
                }}
            >
                Chọn đáp án đúng
            </Text>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                    marginBottom: baseMargin.md
                }}
            >
                {props.data.question}
            </Text>
            {(() => {
                let choicesArray = Object.keys(props.data.content.choices).map(key => ({ key, value: props.data.content.choices[key] }));
                
                return choicesArray.map((item, index) => {
                    return (
                        <OptionItem
                            key={'exercise-option-' + index}
                            onPress={() => setSelectedAnswer(item.key)}
                            selected={item.key === selectedAnswer}
                            data={item}
                        />
                    )
                })
            })()}
        </View>
    )
}

export default ExerciseMultipleChoice;
import React, { useState } from 'react';
import {
    View,
    Text,
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseMargin,
    baseIcon,
    dimension
} from '../../../styles/base';
import CustomIcon from '../../common/CustomIcon';

const ExerciseAnswer = (props) => {
    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center'
            }}
        >
            <View
                style={{
                    flexDirection: 'column',
                    paddingBottom: basePadding.md,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <CustomIcon
                    name={props.correct ? 'check-circle' : 'x-circle'}
                    size={dimension.width * 0.3}
                    style={{
                        color: props.correct ? baseColor.six : baseColor.ten
                    }}
                />
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold,
                        paddingTop: basePadding.md
                    }}
                >
                    {props.correct ? 'Chính xác!' : 'Chưa chính xác!'}
                </Text>
            </View>
            <Text
                style={{
                    color: baseColor.four,
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.regular,
                    marginBottom: baseMargin.md
                }}
            >
                {props.explanation}
            </Text>
        </View>
    )
}

export default ExerciseAnswer;
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    TouchableOpacity,
    Text,
    KeyboardAvoidingView,
    Platform
} from 'react-native';
import { connect } from 'react-redux';
import {
    messageReducerUpdate,
    messageGetConversation
} from '../../actions';
import {
    baseColor,
    basePadding,
    baseIcon,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';
import { isIphoneX, moderateScale } from '../../utils';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import { VideoListPlaceholder } from '../common/Placeholder';
import MessageInboxItem from './MessageInboxItem';
import CustomIcon from '../common/CustomIcon';

const BackButton = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                position: 'absolute',
                left: basePadding.md,
            }}
        >
            <CustomIcon
                name={'arrow-left'}
                size={baseIcon.xl}
                style={{
                    color: baseColor.three
                }}
            />
        </TouchableOpacity>
    )
}

const MessageInboxScreen = (props) => {

    const [data, setData] = useState(null);

    /**
     * Methods
     */

    const onRefresh = () => {
        
    }

    /**
     * UseEffect
     */

    /**
     * Render methods
     */

    const renderInboxItem = ({ item, index }) => {
        return (
            <MessageInboxItem
                data={item}
                onPress={() => {
                    props.navigation.navigate('MessageConversation', {
                        screenName: 'Shiina Mashiro'
                    })
                }}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.md,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <BackButton
                    onPress={() => {
                        props.navigation.goBack(null);
                    }}
                />
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.xxl,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    TRÒ CHUYỆN
                </Text>
            </View>
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1
                }}
            >
                <CustomFlatListWithFetch
                    keyExtractor={(item, index) => {
                        return 'conversation-item-' + index
                    }}
                    data={data}
                    renderItem={renderInboxItem}
                    fetchNextBatch={(page, perPage) => {
                        setData([1, 2, 3]);
                    }}
                    onRefresh={onRefresh}
                    renderPlaceholder={() => {
                        return (
                            <VideoListPlaceholder />
                        )
                    }}
                    perPage={16}
                />
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, message }) => {
    return {
        accessToken: auth.accessToken,
        messages: message.messages
    }
}

export default connect(mapStateToProps, {
    messageReducerUpdate,
    messageGetConversation
})(MessageInboxScreen);
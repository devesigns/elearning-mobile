import React from 'react';
import {
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseBorderRadius,
    baseFontSize,
    baseFontFamily,
    dimension,
    baseMargin
} from '../../styles/base';

const MessageItem = (props) => {
    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: props.isOwnMessage ? 'flex-end' : 'flex-start',
                marginBottom: baseMargin.md
            }}
        >
            <View
                style={{
                    backgroundColor: props.isOwnMessage ? baseColor.three : baseColor.one,
                    padding: basePadding.sm,
                    borderRadius: baseBorderRadius.md,
                    maxWidth: dimension.width * 0.6667,
                    elevation: 3,
                    shadowOffset: {
                        width: 3,
                        height: 3
                    },
                    shadowOpacity: 0.2,
                    shadowColor: 'rgba(0, 0, 0, 0.3)',
                }}
            >
                <Text
                    style={{
                        color:  baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular
                    }}
                >
                    {props.data}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: props.isOwnMessage ? 'flex-start' : 'flex-end',
                        paddingTop: basePadding.md
                    }}
                >
                    <Text
                        style={{
                            color: props.isOwnMessage ? baseColor.one : baseColor.five,
                            fontSize: baseFontSize.xs,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        3 phút trước
                    </Text>
                </View>
            </View>
        </View>
    )
}

export default MessageItem;
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    KeyboardAvoidingView,
    SafeAreaView,
    Platform
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseIcon
} from '../../styles/base';
import { moderateScale, isIphoneX } from '../../utils';
import MessageItem from './MessageItem';
import MessageInput from './MessageInput';

const MessageConversationScreen = (props) => {
    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.two
            }}
        >
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={moderateScale(55 + (isIphoneX() ? 44 : 20))}
                style={{
                    flex: 1
                }}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow: 1,
                        paddingHorizontal: basePadding.sm,
                        paddingVertical: basePadding.md
                    }}
                >
                    <MessageItem
                        data={'Chào anh! Em thấy những khóa học của anh thật sự rất hữu ích. Không biết là anh có nhận kèm 1-1 không ạ?'}
                    />
                    <MessageItem
                        isOwnMessage
                        data={'Không em nhé.'}
                    />
                </ScrollView>
                <MessageInput />
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default MessageConversationScreen;
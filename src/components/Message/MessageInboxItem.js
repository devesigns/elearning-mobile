import React from 'react';
import {
    View,
    Image,
    TouchableOpacity,
    Text
} from 'react-native';
import {
    dimension,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseIcon,
    basePadding
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';

const MessageInboxItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                flexDirection: 'row',
                alignItems: 'center',
                padding: basePadding.md,
                paddingTop: 0
            }}
            onPress={props.onPress}
        >
            <Image
                source={{uri: `https://picsum.photos/640/360/?random=${Math.round(Math.random() * 500)}`}}
                style={{
                    height: dimension.width * 0.15,
                    width: dimension.width * 0.15,
                    borderRadius: dimension.width * 0.15 / 2,
                    backgroundColor: baseColor.two
                }}
            />
            <View
                style={{
                    flex: 1,
                    paddingLeft: basePadding.md
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    Shiina Mashiro
                </Text>
                <Text
                    style={{
                        color: baseColor.five,
                        fontSize: baseFontSize.xs,
                        fontFamily: baseFontFamily.regular
                    }}
                >
                    Hello! Can you help me...
                </Text>
            </View>
            <TouchableOpacity>
                <CustomIcon
                    name='more-vertical'
                    style={{
                        color: baseColor.five
                    }}
                    size={baseIcon.md}
                />
            </TouchableOpacity>
        </TouchableOpacity>
    )
}

export default MessageInboxItem;
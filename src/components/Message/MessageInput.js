import React, { useState } from 'react';
import {
    View,
    TextInput,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import {
    basePadding,
    baseColor,
    baseBorderRadius,
    baseFontSize,
    baseFontFamily,
    baseIcon,
    dimension
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';

const MessageInput = (props) => {
    const [newMessage, setNewMessage] = useState('');

    return (
        <View
            style={{
                width: dimension.width,
                backgroundColor: baseColor.two,
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.sm,
                    paddingHorizontal: basePadding.md,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <TextInput
                    value={newMessage}
                    onChangeText={(value) => setNewMessage(value)}
                    placeholder={'Aa'}
                    style={{
                        color: baseColor.five,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.regular,
                        flex: 1,
                        padding: 0,
                        maxHeight: (baseFontSize.md * 1.4 * 3)
                    }}
                    multiline
                    scrollEnabled={true}
                />
                <TouchableOpacity
                    onPress={() => {
                        
                    }}
                    style={{
                        paddingLeft: basePadding.sm
                    }}
                >
                    <CustomIcon
                        name={'camera'}
                        style={{
                            color: baseColor.three
                        }}
                        size={baseIcon.md}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        Keyboard.dismiss();
                        setNewMessage('');
                        props.onSubmit(newMessage);
                    }}
                    style={{
                        paddingLeft: basePadding.sm
                    }}
                >
                    <CustomIcon
                        name={'send'}
                        style={{
                            color: baseColor.three
                        }}
                        size={baseIcon.md}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default MessageInput;
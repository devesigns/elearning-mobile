import _ from 'lodash';
import React, { useEffect } from 'react';
import {
    View,
    Text,
    Image
} from 'react-native';
import { connect } from 'react-redux';
import {
    authGetToken
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon
} from '../../styles/base';
import { moderateScale } from '../../utils';

const SplashScreen = (props) => {
    if (_.isEmpty(props.accessToken)) {
        props.authGetToken();
    }

    useEffect(() => {
        if (!_.isEmpty(props.accessToken) && props.accessToken != 'none') {
            props.navigation.navigate('Dashboard');
        }

        if (!_.isEmpty(props.accessToken) && props.accessToken == 'none') {
            props.navigation.navigate('Auth');
        }
    }, [props.accessToken])

    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            <View>
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.xxl + moderateScale(2),
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    SLA                    
                </Text>
                <Image
                    source={require('../../assets/img/hat.png')}
                    style={{
                        position: 'absolute',
                        top: - basePadding.xs,
                        right: - (baseIcon.xl / 3),
                        height: baseIcon.xl,
                        width: baseIcon.xl,
                        transform: [
                            {
                                rotate: '30deg'
                            }
                        ]
                    }}
                />
            </View>
        </View>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        accessToken: auth.accessToken
    }
}

export default connect(mapStateToProps, {
    authGetToken
})(SplashScreen);
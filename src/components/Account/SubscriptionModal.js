import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    Modal,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import {
    accountGetAvailablePlan
} from '../../actions';
import {
    basePadding,
    baseFontFamily,
    baseColor,
    baseFontSize,
    dimension,
    baseIcon,
    baseMargin
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';

const SubscriptionModal = (props) => {

    const [selectedPlan, setSelectedPlan] = useState(
        props.plan.type === 'basic' ? 2 :
        props.plan.type === 'monthly' ? 1 :
        2
    );

    const plans = [
        {
            type: 'basic',
            label: 'Miễn phí',
            price: 0
        },
        {
            type: 'monthly',
            label: 'Hằng tháng',
            price: 50000,
            unit: 'tháng'
        },
        {
            type: 'yearly',
            label: 'Hằng năm',
            price: 300000,
            unit: 'năm'
        }
    ]

    /**
     * Methods
     */

    const onSelect = (index) => {
        setSelectedPlan(index);
    }

    const onSubmit = () => {
        props.onSubmit(props.availablePlans[selectedPlan].type);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (props.availablePlans === null) {
            props.accountGetAvailablePlan(props.accessToken);
        }
    }, [])

    /**
     * Render methods
     */

    const renderPlanItem = (item, index) => {
        return (
            <TouchableOpacity
                key={'subscription-' + index}
                style={{
                    height: (dimension.width - basePadding.md * 3) / 2,
                    width: (dimension.width - basePadding.md * 3) / 2,
                    marginBottom: baseMargin.md
                }}
                onPress={() => onSelect(index)}
            >
                {
                    item.type === 'yearly' ?
                    <Text
                        style={{
                            width: (dimension.width - basePadding.md * 3) / 2,
                            textAlign: 'center',
                            padding: basePadding.xs,
                            color: baseColor.one,
                            backgroundColor: baseColor.three,
                            fontSize: baseFontSize.xs,
                            fontFamily: baseFontFamily.bold,
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            zIndex: 999
                        }}
                    >
                        Tiết kiệm
                    </Text>
                    :
                    null
                }
                <View
                    style={{
                        backgroundColor: baseColor.one,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: basePadding.sm,
                        flex: 1,
                        borderColor: selectedPlan === index ? 'rgba(255, 203, 69, 1)' : 'rgba(255, 203, 69, 0)',
                        borderWidth: 3
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontFamily: baseFontFamily.bold,
                            fontSize: baseFontSize.md
                        }}
                    >
                        {item.name}
                    </Text>
                    <Text
                        style={{
                            color: baseColor.three,
                            fontFamily: baseFontFamily.bold,
                            fontSize: baseFontSize.xl,
                            paddingBottom: basePadding.md,
                            textAlign: 'center'
                        }}
                    >
                        {item.price + 'đ\n'}
                        {
                            item.unit ?
                            <Text
                                style={{
                                    fontSize: baseFontSize.md,
                                }}
                            >
                                {'/' + item.unit}
                            </Text>
                            :
                            ''
                        }
                    </Text>
                    <Text
                        style={{
                            fontSize: baseFontSize.xs,
                            fontFamily: baseFontSize.regular,
                            color: baseColor.four,
                            textAlign: 'center'
                        }}
                    >
                        {'Thanh toán ' + item.name}
                    </Text>
                    {
                        props.plan.type === item.type ?
                        <Text
                            style={{
                                fontSize: baseFontSize.xs,
                                fontFamily: baseFontSize.regular,
                                color: baseColor.five
                            }}
                        >
                            Đang dùng
                        </Text>
                        :
                        null
                    }
                </View>
            </TouchableOpacity>
        )
    }

    if (_.isEmpty(props.plan)) {
        return null;
    }

    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={props.visible}
            onRequestClose={props.onRequestClose}
        >
            <SafeAreaView
                style={{
                    backgroundColor: baseColor.two,
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                }}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        flexGrow: 1
                    }}
                >
                    <View
                        style={{
                            paddingVertical: basePadding.sm,
                            paddingHorizontal: basePadding.md,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: baseColor.three
                        }}
                    >
                        <Text
                            style={{
                                fontFamily: baseFontFamily.bold,
                                fontSize: baseFontSize.md,
                                color: baseColor.one
                            }}
                        >
                            {
                                props.plan && props.plan.type === 'basic' ?
                                'Nâng cấp'
                                :
                                'Tài khoản Premium'
                            }
                        </Text>
                        <TouchableOpacity
                            onPress={props.onRequestClose}
                            style={{
                                position: 'absolute',
                                right: basePadding.md
                            }}
                        >
                            <CustomIcon
                                name='x'
                                size={baseIcon.md}
                                style={{
                                    color: baseColor.one
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            padding: basePadding.md,
                            flex: 1
                        }}
                    >
                        <View
                            style={{
                                paddingBottom: basePadding.md
                            }}
                        >
                            <Text
                                style={{
                                    fontFamily: baseFontFamily.regular,
                                    fontSize: baseFontSize.sm,
                                    color: baseColor.four
                                }}
                            >
                                Bạn có thể bỏ tiền uống cafe 19k/mỗi ngày, tại sao bạn không bỏ ra 2k/mỗi ngày để có thêm kiến thức, kỹ năng trong lĩnh vực bạn đang theo đuổi?{'\n\n'}
                                Hãy đăng ký ngay gói Premium với chỉ&nbsp;
                                <Text
                                    style={{
                                        fontFamily: baseFontFamily.bold,
                                        fontSize: baseFontSize.sm,
                                        color: baseColor.four
                                    }}
                                >
                                    2 ngàn đồng/mỗi ngày.
                                </Text>
                            </Text>
                        </View>
                        {
                            props.availablePlans != null ?
                            <View
                                style={{
                                    flexWrap: 'wrap',
                                    alignItems: 'flex-start',
                                    justifyContent: 'space-between',
                                    flexDirection: 'row'
                                }}
                            >
                                {(() => {
                                    return props.availablePlans.map((item, index) => {
                                        if (item.type === 'basic') {
                                            return null;
                                        }

                                        return renderPlanItem(item, index);
                                    })
                                })()}
                            </View>
                            :
                            null
                        }
                        <View
                            style={{
                                paddingBottom: basePadding.xl
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: baseFontSize.sm,
                                    fontFamily: baseFontFamily.bold,
                                    lineHeight: baseFontSize.sm * 1.5,
                                    color: baseColor.four,
                                    marginBottom: baseMargin.md,
                                    textAlign: 'center'
                                }}
                            >
                                Lợi ích của gói Premium
                            </Text>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                    marginBottom: baseMargin.xl
                                }}
                            >
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: '50%'
                                    }}
                                >
                                    <CustomIcon
                                        name={'play-circle'}
                                        size={baseIcon.xxl}
                                        style={{
                                            marginBottom: baseMargin.xs
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.sm,
                                            fontFamily: baseFontFamily.regular,
                                            color: baseColor.four,
                                            textAlign: 'center'
                                        }}
                                    >
                                        Xem kho video bài học không giới hạn.
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: '50%',
                                        marginBottom: baseMargin.xl
                                    }}
                                >
                                    <CustomIcon
                                        name={'airplay'}
                                        size={baseIcon.xxl}
                                        style={{
                                            marginBottom: baseMargin.xs
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.sm,
                                            fontFamily: baseFontFamily.regular,
                                            color: baseColor.four,
                                            textAlign: 'center'
                                        }}
                                    >
                                        Cơ hội xem các video khóa học chuyên sâu từ các chuyên gia và diễn giả hàng đầu trong lĩnh vực.
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: '50%'
                                    }}
                                >
                                    <CustomIcon
                                        name={'star'}
                                        size={baseIcon.xxl}
                                        style={{
                                            marginBottom: baseMargin.xs
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.sm,
                                            fontFamily: baseFontFamily.regular,
                                            color: baseColor.four,
                                            textAlign: 'center'
                                        }}
                                    >
                                        Đặc quyền xem các video bài học mới nhất và luôn cập nhật không giới hạn.
                                    </Text>
                                </View>
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: '50%',
                                        opacity: 0.3
                                    }}
                                >
                                    <CustomIcon
                                        name={'download'}
                                        size={baseIcon.xxl}
                                        style={{
                                            marginBottom: baseMargin.xs
                                        }}
                                    />
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.sm,
                                            fontFamily: baseFontFamily.regular,
                                            color: baseColor.four,
                                            textAlign: 'center'
                                        }}
                                    >
                                        Tải các video bài học để xem offline.
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <TouchableOpacity
                    style={{
                        padding: basePadding.sm,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: baseColor.three
                    }}
                    onPress={onSubmit}
                >
                    <Text
                        style={{
                            color: baseColor.one,
                            fontFamily: baseFontFamily.bold,
                            fontSize: baseFontSize.md
                        }}
                    >
                        {
                            props.availablePlans && props.plan.type === props.availablePlans[selectedPlan].type ?
                            'GIA HẠN'
                            :
                            props.availablePlans && props.plan.type === 'yearly' && props.availablePlans[selectedPlan].type === 'monthly' ?
                            'CHUYỂN GÓI'
                            :
                            'NÂNG CẤP'
                        }
                    </Text>
                </TouchableOpacity>
            </SafeAreaView>
        </Modal>
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        availablePlans: account.availablePlans
    }
}

export default connect(mapStateToProps, {
    accountGetAvailablePlan
})(SubscriptionModal);
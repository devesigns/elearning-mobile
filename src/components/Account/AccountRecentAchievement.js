import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    FlatList,
    View,
    TouchableOpacity,
    Text
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    accountGetRecentAchievement
} from '../../actions';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseMargin,
    dimension
} from '../../styles/base';
import { AchievementPlaceholder } from '../common/Placeholder';
import AchievementGridItem from '../Achievement/AchievementGridItem';

const AccountRecentAchievement = (props) => {
    
    /**
     * Methods
     */

    /**
     * UseEffects
     */

    useEffect(() => {
        if (props.recentAchievements === null) {
            props.accountGetRecentAchievement(props.accessToken, {
                page: 1,
                perPage: 3
            })
        }
    }, [props.recentAchievements])

    /**
     * Render Methods
     */

    const renderPlaceholder = () => {
        return (
            <AchievementPlaceholder />
        )
    }

    const renderViewAllButton = () => {
        return (
            <TouchableOpacity
                style={{
                    width: (dimension.width - basePadding.md * 2) / 2 - basePadding.xs,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    padding: basePadding.sm,
                    borderColor: baseColor.five,
                    borderWidth: 0.5,
                }}
                onPress={() => {
                    props.navigation.navigate('AccountAchievement');
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    Xem tất cả
                </Text>
            </TouchableOpacity>
        )
    }

    const renderItem = ({item, index}) => {
        if (item.type != 'view-all') {
            return (
                <AchievementGridItem
                    data={item}
                />
            )
        } else {
            return renderViewAllButton();
        }
    }

    if (_.isEmpty(props.recentAchievements)) {
        return renderPlaceholder();
    }

    return (
        <View
            style={{
                padding: basePadding.md,
                paddingBottom: 0
            }}
        >
            <FlatList
                keyExtractor={(item, index) => {
                    return 'achievement-recent-' + index;
                }}
                data={[
                    ...props.recentAchievements,
                    {
                        type: 'view-all'
                    }
                ]}
                renderItem={renderItem}
                numColumns={2}
                columnWrapperStyle={{
                    marginBottom: baseMargin.md,
                    justifyContent: 'space-between'
                }}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        recentAchievements: account.recentAchievements
    }
}

export default connect(mapStateToProps, {
    accountGetRecentAchievement
})(withNavigation(AccountRecentAchievement));
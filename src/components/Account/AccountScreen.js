import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    accountGetUserInfo
} from '../../actions';
import {
    baseColor,
    basePadding,
} from '../../styles/base';
import CustomPageBreak from '../common/CustomPageBreak';
import AccountMenu from './AccountMenu';
import { ProfilePlaceholder } from '../common/Placeholder';
import AccountProfileHeader from './AccountProfileHeader';
import AccountRecentAchievement from './AccountRecentAchievement';

const AccountScreen = (props) => {

    /**
     * UseEffect
     */

    useEffect(() => {
        if (!props.accessToken) {
            props.navigation.navigate('Auth');
        }
    }, [props.accessToken])

    useEffect(() => {
        if (_.isEmpty(props.user)) {
            props.accountGetUserInfo(props.accessToken);
        }
    }, [props.user])

    /**
     * Render methods
     */

    if (_.isEmpty(props.user)) {
        return (
            <ProfilePlaceholder />
        )
    }

    console.log(props.navigation);

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                <AccountProfileHeader
                    user={props.user}
                    extendSubscription={props.navigation.getParam('extendSubscription')}
                />
                <CustomPageBreak />
                <AccountRecentAchievement />
                <CustomPageBreak />
                <AccountMenu />
                <View
                    style={{
                        paddingBottom: basePadding.xxxl
                    }}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        user: account.user
    }
};

export default connect(mapStateToProps, {
    accountGetUserInfo
})(AccountScreen);
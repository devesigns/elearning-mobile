import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    authSignOut
} from '../../actions';
import {
    baseColor,
    baseFontSize,
    baseFontFamily,
    basePadding,
    baseIcon
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';
import CustomPageBreak from '../common/CustomPageBreak';

const AccountMenu = (props) => {
    useEffect(() => {
        if (!props.accessToken) {
            props.navigation.navigate('Auth');
        }
    }, [props.accessToken])

    return (
        <>
            <View
                style={{
                    paddingHorizontal: basePadding.md
                }}
            >
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingVertical: basePadding.sm
                    }}
                    onPress={() => {
                        props.navigation.navigate('Promotion');
                    }}
                >
                    <CustomIcon
                        name={'bookmark'}
                        size={baseIcon.md}
                    />
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            paddingLeft: basePadding.md
                        }}
                    >
                        Mã khuyến mãi
                    </Text>
                </TouchableOpacity>
            </View>
            <CustomPageBreak />
            <View
                style={{
                    paddingHorizontal: basePadding.md
                }}
            >
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: baseColor.five,
                        borderBottomWidth: 0.5,
                        paddingVertical: basePadding.sm
                    }}
                >
                    <CustomIcon
                        name={'info'}
                        size={baseIcon.md}
                    />
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            paddingLeft: basePadding.md
                        }}
                    >
                        Về Smart Life Agent (SLA)
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        borderBottomColor: baseColor.five,
                        borderBottomWidth: 0.5,
                        paddingVertical: basePadding.sm
                    }}
                    onPress={() => {
                        props.navigation.navigate('PrivacyPolicy');
                    }}
                >
                    <CustomIcon
                        name={'file-text'}
                        size={baseIcon.md}
                    />
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            paddingLeft: basePadding.md
                        }}
                    >
                        Chính sách bảo mật
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingVertical: basePadding.sm
                        
                    }}
                    onPress={() => props.authSignOut()}
                >
                    <CustomIcon
                        name={'log-out'}
                        size={baseIcon.md}
                    />
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular,
                            paddingLeft: basePadding.md
                        }}
                    >
                        Đăng xuất
                    </Text>
                </TouchableOpacity>
            </View>
        </>
    )
}

const mapStateToProps = ({ auth }) => {
    return {
        accessToken: auth.accessToken
    }
};

export default connect(mapStateToProps, {
    authSignOut
})(withNavigation(AccountMenu));
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image
} from 'react-native';
import { withNavigation } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import {
    accountEditUserAvatar,
    accountEditUserInfo,
    accountUpdatePlan
} from '../../actions';
import {
    basePadding,
    dimension,
    baseColor,
    baseFontSize,
    baseFontFamily,
    baseIcon,
    baseMargin
} from '../../styles/base';
import CustomIcon from '../common/CustomIcon';
import AccountEditModal from './AccountEditModal';
import SubscriptionModal from './SubscriptionModal';

const AccountProfileHeader = (props) => {
    const [editModalVisible, setEditModalVisible] = useState(false);
    const [subscriptionModalVisible, setSubscriptionModalVisible] = useState(false);

    /**
     * Methods
     */

    const openImagePicker = () => {
        const imagePickerOptions = {
            title: 'Chọn ảnh avatar',
            cancelButtonTitle: 'Quay lại',
            takePhotoButtonTitle: 'Chụp ảnh...',
            chooseFromLibraryButtonTitle: 'Chọn ảnh từ bộ sưu tập...',
            maxHeight: 256,
            maxWidth: 256
        };

        ImagePicker.showImagePicker(imagePickerOptions, (response) => {
            console.log('Response = ', response);
            
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {                 
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
                props.accountEditUserAvatar(props.accessToken, {
                    avatar: response
                })
            }
        });
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        console.log(props);
        if (props.extendSubscription) {
            setSubscriptionModalVisible(true);
        }
    }, []);

    /**
     * Render methods
     */

    return (
        <View>
            {/* <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    paddingHorizontal: basePadding.md,
                    paddingVertical: basePadding.sm,
                    backgroundColor: baseColor.five
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        props.navigation.navigate('MessageInbox')
                    }}
                >
                    <CustomIcon
                        name='message-square'
                        style={{
                            color: baseColor.one
                        }}
                        size={baseIcon.md}
                    />
                </TouchableOpacity>
                <TouchableOpacity>
                    <CustomIcon
                        name='settings'
                        style={{
                            color: baseColor.one
                        }}
                        size={baseIcon.md}
                    />
                </TouchableOpacity>
            </View> */}
            <View
                style={{
                    padding: basePadding.md,
                    flexDirection: 'row'
                }}
            >
                <TouchableOpacity
                    style={{
                        position: 'absolute',
                        top: basePadding.md,
                        right: basePadding.md,
                        paddingLeft: basePadding.xs,
                        paddingBottom: basePadding.xs,
                        zIndex: 999,
                        elevation: 3
                    }}
                    onPress={() => {
                        setEditModalVisible(true);
                    }}
                >
                    <CustomIcon 
                        name={'edit-3'}
                        size={baseIcon.md}
                        style={{
                            color: baseColor.three
                        }}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{
                        height: dimension.width * 0.2,
                        width: dimension.width * 0.2,
                        borderRadius: dimension.width * 0.2 / 2,
                        overflow: 'hidden'
                    }}
                    onPress={openImagePicker}
                >
                    <Image
                        source={{uri: props.user.avatar}}
                        style={{
                            height: dimension.width * 0.2,
                            width: dimension.width * 0.2,
                            backgroundColor: baseColor.nine
                        }}
                    />
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            right: 0,
                            width: '100%',
                            padding: basePadding.xs,
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <CustomIcon 
                            name={'camera'}
                            size={baseIcon.xs}
                            style={{
                                color: baseColor.one
                            }}
                        />
                    </View>
                </TouchableOpacity>
                <View
                    style={{
                        flexDirection: 'column',
                        paddingLeft: basePadding.md,
                        flex: 1
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.lg,
                            fontFamily: baseFontFamily.bold
                        }}
                    >
                        {props.user.fullname}
                    </Text>
                    <Text
                        style={{
                            color: baseColor.four,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.regular
                        }}
                    >
                        {
                            props.user.job_description ?
                            props.user.job_description
                            :
                            'Bổ sung thông tin công việc...'
                        }
                    </Text>
                    {
                        props.user.plan ?
                        <View
                            style={{
                                marginTop: baseMargin.md
                            }}
                        >
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.regular
                                }}
                            >
                                Gói:&nbsp;
                                <Text
                                    style={{
                                        color: baseColor.four,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.bold
                                    }}
                                >
                                    {props.user.plan.name}
                                </Text>
                            </Text>
                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                }}
                                onPress={() => {
                                    setSubscriptionModalVisible(true);
                                }}
                            >
                                <Text
                                    style={{
                                        color: baseColor.three,
                                        fontSize: baseFontSize.md,
                                        fontFamily: baseFontFamily.regular
                                    }}
                                >
                                    {
                                        props.user.plan && props.user.plan.type !== 'basic' ?
                                        'T.Khoản Premium'
                                        :
                                        'Nâng cấp'
                                    }
                                </Text>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                    }
                    {
                        props.user.plan && props.user.plan.type !== 'basic' ?
                        <Text
                            style={{
                                color: baseColor.four,
                                fontSize: baseFontSize.md,
                                fontFamily: baseFontFamily.regular
                            }}
                        >
                            Hết hạn:&nbsp;
                            <Text
                                style={{
                                    color: baseColor.four,
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.bold
                                }}
                            >
                                {props.user.plan.expire_at.split('T')[0]}
                            </Text>
                        </Text>
                        :
                        null
                    }
                </View>
                <AccountEditModal
                    userForm={props.user}
                    visible={editModalVisible}
                    onRequestClose={() => {
                        setEditModalVisible(false);
                    }}
                    onSubmit={(data) => {
                        props.accountEditUserInfo(props.accessToken, data);
                        setEditModalVisible(false);
                    }}
                />
                {
                    props.user.plan ?
                    <SubscriptionModal 
                        plan={props.user.plan}
                        visible={subscriptionModalVisible}
                        onRequestClose={() => {
                            setSubscriptionModalVisible(false);
                        }}
                        onSubmit={(type) => {
                            if (type !== props.user.plan.type) {
                                props.accountUpdatePlan(props.accessToken, {
                                    type
                                })
                            }

                            setSubscriptionModalVisible(false);
                        }}
                    />
                    :
                    null
                }
            </View>
        </View>
    )
}

const mapStateToProps = ({ auth, account }) => {
    return {
        accessToken: auth.accessToken,
        user: account.user
    }
}

export default connect(mapStateToProps, {
    accountEditUserAvatar,
    accountEditUserInfo,
    accountUpdatePlan
})(withNavigation(AccountProfileHeader));
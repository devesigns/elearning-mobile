import React, { useState } from 'react';
import {
    View,
    Text
} from 'react-native';
import {
    validate,
    getValidationMessage
} from '../../utils';
import {
    basePadding,
    baseColor,
    baseFontSize,
    baseFontFamily
} from '../../styles/base';
import AuthTextInput from '../Auth/Misc/AuthTextInput';
import CustomModal from '../common/CustomModal';

const AccountEditModal = (props) => {
    const [name, setName] = useState(props.userForm.fullname);
    const [jobDesc, setJobDesc] = useState(props.userForm.job_description);

    const [nameValidation, setNameValidation] = useState('');

    const resetValidation = () => {
        setNameValidation('');
    }

    const validateForm = () => {
        let willResetForm = false;
        
        let nameValid = validate.validateName(name);
        if (nameValid != true) {
            willResetForm = true;
            setNameValidation(getValidationMessage(nameValid));
        }

        if (willResetForm) {
            resetForm();
            return false;
        }

        return true;
    }

    const onSubmit = () => {
        resetValidation();
        
        if (validateForm()) {
            props.onSubmit({
                name,
                jobDesc
            })
        }
    }

    return (
        <CustomModal
            visible={props.visible}
            onRequestClose={props.onRequestClose}
            onConfirm={onSubmit}
        >
            <View
                style={{
                    paddingBottom: basePadding.md
                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    HỌ VÀ TÊN
                </Text>
                <AuthTextInput
                    value={name}
                    onChangeText={(value) => setName(() => value)}
                    fieldValidation={nameValidation}
                />
            </View>
            <View
                style={{

                }}
            >
                <Text
                    style={{
                        color: baseColor.three,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    MÔ TẢ CÔNG VIỆC
                </Text>
                <AuthTextInput
                    value={jobDesc}
                    onChangeText={(value) => setJobDesc(() => value)}
                    multiline
                    maxLength={100}
                />
            </View>
        </CustomModal>
    )
}

export default AccountEditModal;
import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    coacherGetLesson,
    coacherReducerUpdate
} from '../../../actions';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseIcon
} from '../../../styles/base';
import HorizontalSlider from '../../common/HorizontalSlider';
import VideoSliderItem from '../../common/VideoSliderItem';
import CustomIcon from '../../common/CustomIcon';

const CoacherLessonTab = (props) => {

    const [mounted, setMounted] = useState(false);

    /**
     * Methods
     */

    const onItemPress = (item) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', item.id);
        props.lessonReducerUpdate('lessonType', item.course ? 'normal' : 'single');
        props.navigation.navigate('Lesson');
    }

    const onRefresh = () => {
        if (
            props.popularLessons !== null &&
            props.newLessons !== null
        ) {
            props.coacherReducerUpdate('lessons', {
                popular: null,
                new: null,
                all: null
            })
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (
            props.popularLessons === null &&
            props.newLessons === null
        ) {
            props.coacherGetLesson(props.accessToken, {
                coacherId: props.coacherForm.id,
                page: 1,
                perPage: 8,
                type: 'popular'
            })
            props.coacherGetLesson(props.accessToken, {
                coacherId: props.coacherForm.id,
                page: 1,
                perPage: 8,
                type: 'new'
            })
        }
    }, [props.popularLessons, props.newLessons])

    /**
     * Render methods
     */

    if (!mounted) {
        return null;
    }

    return (
        <>
            <View
                style={{
                    padding: basePadding.md,
                    paddingBottom: 0
                }}
            >
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                    onPress={() => {
                        props.navigation.navigate('CoacherLessonList');
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold,
                            flex: 1
                        }}
                    >
                        Xem tất cả bài học
                    </Text>
                    <CustomIcon
                        name='arrow-right'
                        size={baseIcon.md}
                        style={{
                            color: baseColor.three
                        }}
                    />
                </TouchableOpacity>
            </View>
            <View
                style={{
                    paddingVertical: basePadding.md,
                    backgroundColor: baseColor.one
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md,
                        paddingHorizontal: basePadding.md
                    }}
                >
                    BÀI HỌC NỔI BẬT
                </Text>
                <HorizontalSlider
                    data={props.popularLessons}
                    keyExtractor={(item, index) => {
                        return 'featured-lesson-' + index;
                    }}
                    renderItem={({item, index}) => {
                        return (
                            <VideoSliderItem
                                hideUserInfo
                                data={item}
                                containerStyle={{
                                    borderWidth: 0.5,
                                    borderColor: baseColor.two
                                }}
                                onPress={() => onItemPress(item)}
                            />
                        )
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                />
            </View>
            <View
                style={{
                    paddingVertical: basePadding.md,
                    backgroundColor: baseColor.two
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md,
                        paddingHorizontal: basePadding.md
                    }}
                >
                    BÀI HỌC MỚI
                </Text>
                <HorizontalSlider
                    data={props.newLessons}
                    keyExtractor={(item, index) => {
                        return 'featured-lesson-' + index;
                    }}
                    renderItem={({item, index}) => {
                        return (
                            <VideoSliderItem
                                hideUserInfo
                                data={item}
                                onPress={() => onItemPress(item)}
                            />
                        )
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                />
            </View>
        </>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm,
        popularLessons: coacher.lessons.popular,
        newLessons: coacher.lessons.new,
    }
}

export default connect(mapStateToProps, {
    coacherReducerUpdate,
    coacherGetLesson,
    lessonReducerUpdate
})(withNavigation(CoacherLessonTab));
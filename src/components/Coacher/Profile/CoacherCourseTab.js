import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';
import {
    coacherGetCourse,
    coacherReducerUpdate,
    courseReducerUpdate
} from '../../../actions';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseIcon
} from '../../../styles/base';
import HorizontalSlider from '../../common/HorizontalSlider';
import CustomIcon from '../../common/CustomIcon';
import CourseSliderItem from '../../common/CourseSliderItem';

const CoacherCourseTab = (props) => {

    const [mounted, setMounted] = useState(false);

    /**
     * Methods
     */

    const onItemPress = (item) => {
        props.courseReducerUpdate('courseForm', {
            course: item
        });
        props.navigation.navigate('CourseDetail');
    }

    const onRefresh = () => {
        if (
            props.popularCourses !== null &&
            props.newCourses !== null
        ) {
            props.coacherReducerUpdate('courses', {
                popular: null,
                new: null,
                all: null
            })
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (
            props.popularCourses === null &&
            props.newCourses === null
        ) {
            props.coacherGetCourse(props.accessToken, {
                coacherId: props.coacherForm.id,
                page: 1,
                perPage: 8,
                type: 'popular'
            })
            props.coacherGetCourse(props.accessToken, {
                coacherId: props.coacherForm.id,
                page: 1,
                perPage: 8,
                type: 'new'
            })
        }
    }, [props.popularCourses, props.newCourses])

    /**
     * Render methods
     */

    if (!mounted) {
        return null;
    }

    return (
        <>
            <View
                style={{
                    padding: basePadding.md,
                    paddingBottom: 0
                }}
            >
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                    onPress={() => {
                        props.navigation.navigate('CoacherCourseList');
                    }}
                >
                    <Text
                        style={{
                            color: baseColor.three,
                            fontSize: baseFontSize.md,
                            fontFamily: baseFontFamily.bold,
                            flex: 1
                        }}
                    >
                        Xem tất cả khóa học
                    </Text>
                    <CustomIcon
                        name='arrow-right'
                        size={baseIcon.md}
                        style={{
                            color: baseColor.three
                        }}
                    />
                </TouchableOpacity>
            </View>
            <View
                style={{
                    paddingVertical: basePadding.md,
                    backgroundColor: baseColor.one
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md,
                        paddingHorizontal: basePadding.md
                    }}
                >
                    KHÓA HỌC NỔI BẬT
                </Text>
                <HorizontalSlider
                    data={props.popularCourses}
                    keyExtractor={(item, index) => {
                        return 'featured-lesson-' + index;
                    }}
                    renderItem={({item, index}) => {
                        return (
                            <CourseSliderItem
                                hideUserInfo
                                data={item}
                                containerStyle={{
                                    borderWidth: 0.5,
                                    borderColor: baseColor.two
                                }}
                                onPress={() => onItemPress(item)}
                            />
                        )
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                />
            </View>
            <View
                style={{
                    paddingVertical: basePadding.md,
                    backgroundColor: baseColor.two
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold,
                        paddingBottom: basePadding.md,
                        paddingHorizontal: basePadding.md
                    }}
                >
                    KHÓA HỌC MỚI
                </Text>
                <HorizontalSlider
                    data={props.newCourses}
                    keyExtractor={(item, index) => {
                        return 'featured-lesson-' + index;
                    }}
                    renderItem={({item, index}) => {
                        return (
                            <CourseSliderItem
                                hideUserInfo
                                data={item}
                                onPress={() => onItemPress(item)}
                            />
                        )
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                />
            </View>
        </>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm,
        popularCourses: coacher.courses.popular,
        newCourses: coacher.courses.new,
    }
}

export default connect(mapStateToProps, {
    coacherReducerUpdate,
    coacherGetCourse,
    courseReducerUpdate
})(withNavigation(CoacherCourseTab));
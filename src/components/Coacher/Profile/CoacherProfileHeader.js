import _ from 'lodash';
import React from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import {
    coacherReducerUpdate,
    coacherSubscribe
} from '../../../actions';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import { withNavigation } from 'react-navigation';
import {
    baseColor,
    dimension,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseBorderRadius,
    baseIcon,
    baseMargin
} from '../../../styles/base';
import CustomMetaData, { CustomMetaDataBreaker } from '../../common/CustomMetaData';
import CustomIcon from '../../common/CustomIcon';

const CoacherProfileHeader = (props) => {
    if (_.isEmpty(props.coacherForm) || _.isEmpty(props.coacherForm.user)) {
        return null;
    }

    return (
        <View
            style={{
                alignItems: 'center',
                justifyContent: 'center'
            }}
        >
            <TouchableOpacity
                style={{
                    position: 'absolute',
                    top: basePadding.md,
                    left: basePadding.md,
                    zIndex: 999
                }}
                onPress={() => {
                    props.navigation.goBack(null);
                }}
            >
                <CustomIcon
                    name={'arrow-left'}
                    size={baseIcon.xl}
                    style={{
                        color: baseColor.one
                    }}
                />
            </TouchableOpacity>
            <LinearGradient
                colors={['#FFCB45', '#FF8D00']}
                style={{
                    width: '100%',
                    height: dimension.width * 0.25,
                    marginBottom: - dimension.width * 0.15
                }}
            />
            <Image
                source={{uri: props.coacherForm.user.avatar}}
                style={{
                    width: dimension.width * 0.3,
                    height: dimension.width * 0.3,
                    borderRadius: dimension.width * 0.15,
                    backgroundColor: baseColor.two,
                    zIndex: 999,
                }}
                resizeMode={'cover'}
            />
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: basePadding.md,
                    paddingVertical: basePadding.sm
                }}
            >
                <Text
                    style={{
                        color: baseColor.four,
                        fontSize: baseFontSize.lg,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    {props.coacherForm.user.fullname}
                </Text>
                <Text
                    style={{
                        color: baseColor.five,
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular
                    }}
                >
                    {props.coacherForm.summary}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingTop: basePadding.xs
                    }}
                >
                    <CustomMetaData
                        iconName={'video'}
                        label={props.coacherForm.total_lessons}
                    />
                    <CustomMetaDataBreaker />
                    <CustomMetaData
                        iconName={'user'}
                        label={props.coacherForm.total_subscribers}
                    />
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingTop: basePadding.sm
                    }}
                >
                    {/* <TouchableOpacity
                        style={{
                            padding: basePadding.xs,
                            paddingHorizontal: basePadding.sm,
                            backgroundColor: baseColor.five,
                            borderRadius: baseBorderRadius.md,
                            marginRight: baseMargin.xs,
                            flexDirection: 'row',
                            alignItems: 'center'
                        }}
                        onPress={() => {
                            
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.one,
                                fontSize: baseFontSize.md,
                                fontFamily: baseFontFamily.bold
                            }}
                        >
                            TRÒ CHUYỆN
                        </Text>
                        <CustomIcon
                            name='message-square'
                            style={{
                                color: baseColor.one,
                                paddingLeft: basePadding.xs
                            }}
                            size={baseIcon.xs}
                        />
                    </TouchableOpacity> */}
                    <TouchableOpacity
                        style={{
                            padding: basePadding.xs,
                            paddingHorizontal: basePadding.sm,
                            backgroundColor: props.coacherForm.subscribed ? baseColor.five : baseColor.three,
                            borderRadius: baseBorderRadius.md
                        }}
                        onPress={() => {
                            props.coacherSubscribe(props.accessToken, {
                                coacherId: props.coacherForm.id
                            })
                        }}
                    >
                        <Text
                            style={{
                                color: baseColor.one,
                                fontSize: baseFontSize.md,
                                fontFamily: baseFontFamily.bold
                            }}
                        >
                            {props.coacherForm.subscribed ? 'ĐÃ THEO DÕI' : 'THEO DÕI'}
                        </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity
                        style={{
                            padding: basePadding.xs,
                            position: 'absolute',
                            top: basePadding.sm,
                            right: - (baseIcon.md + basePadding.md)
                        }}
                    >
                        <Icon
                            name={props.active ? 'bell' : 'bell-o'}
                            size={baseIcon.md}
                            style={{
                                color: props.active ? baseColor.three : baseColor.four
                            }}
                        />
                    </TouchableOpacity> */}
                </View>
            </View>
        </View>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm
    }
}

export default connect(mapStateToProps, {
    coacherReducerUpdate,
    coacherSubscribe
})(withNavigation(CoacherProfileHeader));
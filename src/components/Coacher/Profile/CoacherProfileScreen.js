import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    RefreshControl,
    TouchableOpacity,
    SafeAreaView
} from 'react-native';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import {
    coacherGetDetail,
    coacherReducerUpdate
} from '../../../actions';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    baseBorderRadius
} from '../../../styles/base';
import CoacherProfileHeader from './CoacherProfileHeader';
import CoacherLessonTab from './CoacherLessonTab';
import CoacherCourseTab from './CoacherCourseTab';
import CustomPageBreak from '../../common/CustomPageBreak';

const CoacherProfileScreen = (props) => {

    const [viewMode, setViewMode] = useState('course');
    const [descExpanded, setDescExpanded] = useState(false);
    const [descHeightMinimized, setDescHeightMinimized] = useState(0);
    const [descHeightExpanded, setDescHeightExpanded] = useState(0);

    /**
     * Methods
     */

    const onRefresh = () => {
        props.coacherReducerUpdate('lessons', {
            popular: null,
            new: null,
            all: null
        });
        props.coacherReducerUpdate('courses', {
            popular: null,
            new: null,
            all: null
        })
    }

    const onNavigationStateChange = (navState) => {
        if (navState.title !== 'about:blank' && navState.title != 'undefined') {
            if (!descExpanded && descHeightMinimized === 0) {
                if (parseInt(navState.title)) {
                    setDescHeightMinimized(parseInt(navState.title));
                }
            } else if (descExpanded && (descHeightExpanded === 0 || descHeightExpanded < parseInt(navState.title))) {
                if (parseInt(navState.title)) {
                    setDescHeightExpanded(parseInt(navState.title));
                }
            }
        }
    }

    /**
     * UseEffect
     */

    useEffect(() => {
        if (
            !_.isEmpty(props.coacherForm) &&
            _.isEmpty(props.coacherForm.user)
        ) {
            props.coacherGetDetail(props.accessToken, {
                coacherId: props.coacherForm.id
            })
        }
    }, [props.coacherForm])

    /**
     * Render methods
     */

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
                refreshControl={
                    <RefreshControl
                        onRefresh={onRefresh}
                    />
                }
            >
                <CoacherProfileHeader />
                {
                    props.coacherForm.description ?
                    <View
                        style={{
                            padding: basePadding.md,
                        }}
                    >
                        <View
                            style={[
                                {
                                    height: descHeightMinimized
                                },
                                descExpanded ? 
                                {
                                    height: descHeightExpanded
                                }
                                :
                                null
                            ]}
                        >
                            <WebView
                                style={{
                                    flex: 1
                                }}
                                onNavigationStateChange={onNavigationStateChange}
                                source={{
                                    html: `
                                        <head>
                                            <meta name="viewport" content="width=device-width, initial-scale=1">
                                            <style>
                                                html, body {
                                                    margin: 0;
                                                    padding: 0;
                                                    background-color: ${baseColor.one};
                                                    font-size: ${baseFontSize.md};
                                                    color: ${baseColor.four};
                                                    line-height: 1.5;
                                                }
                                            </style>
                                        </head>
                                        <body>
                                            ${
                                                props.coacherForm.description.length > 150 && !descExpanded ?
                                                props.coacherForm.description.slice(0, 150) + '...'
                                                :
                                                props.coacherForm.description
                                            }
                                            <script>
                                                window.location.hash = 1;
                                                document.title = document.body.scrollHeight;
                                            </script>
                                        </body>
                                    `
                                }}
                            />
                            {
                                props.coacherForm.description.length > 150 ?
                                <TouchableOpacity
                                    onPress={() => {
                                        setDescExpanded(!descExpanded);
                                    }}
                                >
                                    <Text
                                        style={{
                                            fontSize: baseFontSize.md,
                                            fontFamily: baseFontFamily.regular,
                                            color: baseColor.three
                                        }}
                                    >
                                        {
                                            !descExpanded ?
                                            'More'
                                            :
                                            'Less'
                                        }
                                    </Text>
                                </TouchableOpacity>
                                :
                                null
                            }
                        </View>
                    </View>
                    :
                    null
                }
                <CustomPageBreak />
                <View
                    style={{
                        flexDirection: 'row',
                        backgroundColor: baseColor.two
                    }}
                >
                    <TouchableOpacity
                        style={{
                            flex: 1,
                            padding: basePadding.md,
                            backgroundColor: viewMode === 'course' ? baseColor.one : 'transparent',
                            borderTopRightRadius: baseBorderRadius.md
                        }}
                        onPress={() => setViewMode('course')}
                    >
                        <Text
                            style={[
                                {
                                    fontSize: baseFontSize.sm,
                                    fontFamily: baseFontFamily.regular,
                                    color: baseColor.five
                                },
                                viewMode === 'course' ?
                                {
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.bold,
                                    color: baseColor.four
                                }
                                :
                                null
                            ]}
                        >
                            Khóa học
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{
                            flex: 1,
                            padding: basePadding.md,
                            alignItems: 'flex-end',
                            backgroundColor: viewMode === 'lesson' ? baseColor.one : 'transparent',
                            borderTopLeftRadius: baseBorderRadius.md
                        }}
                        onPress={() => setViewMode('lesson')}
                    >
                        <Text
                            style={[
                                {
                                    fontSize: baseFontSize.sm,
                                    fontFamily: baseFontFamily.regular,
                                    color: baseColor.five
                                },
                                viewMode === 'lesson' ?
                                {
                                    fontSize: baseFontSize.md,
                                    fontFamily: baseFontFamily.bold,
                                    color: baseColor.four
                                }
                                :
                                null
                            ]}
                        >
                            Bài học
                        </Text>
                    </TouchableOpacity>        
                </View>
                {
                    viewMode === 'course' ?
                    <CoacherCourseTab />
                    :
                    <CoacherLessonTab />
                }
            </ScrollView>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm
    }
}

export default connect(mapStateToProps, {
    coacherGetDetail,
    coacherReducerUpdate
})(CoacherProfileScreen);
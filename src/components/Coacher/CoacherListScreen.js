import React, { useState } from 'react';
import {
    View,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    coacherReducerUpdate,
    coacherGetCoacher
} from '../../actions';
import {
    baseColor,
    baseMargin,
    basePadding
} from '../../styles/base';
import CoacherGridItem from './CoacherGridItem';
import { AchievementPlaceholder } from '../common/Placeholder';
import CustomFlatListWithFetch from '../common/CustomFlatListWithFetch';
import CustomSearchBar from '../common/CustomSearchBar';

const CoacherListScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    /**
     * Methods
     */

    const onRefresh = () => {
        props.coacherReducerUpdate('coachers', null);
    }

    const onSearchSubmit = (query) => {
        if (query !== searchQuery) {
            setSearchQuery(query);
            onRefresh();
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }
    
    /**
     * UseEffect
     */

    /**
     * Render Methods
     */

    if (!mounted) {
        return null;
    }

    const renderGridItem = ({item, index}) => {
        return (
            <CoacherGridItem
                data={item}
                onPress={() => {
                    props.coacherReducerUpdate('coacherForm', {
                        id: item.id
                    });
                    props.navigation.navigate('CoacherProfile');
                }}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1
            }}
        >
            <View
                style={{
                    flex: 1,
                    backgroundColor: baseColor.two
                }}
            >
                <View
                    style={{
                        backgroundColor: baseColor.one,
                        padding: basePadding.md
                    }}
                >
                    <CustomSearchBar
                        placeholder={'Tìm giảng viên...'}
                        onSubmit={onSearchSubmit}
                    />
                </View>
                <CustomFlatListWithFetch
                    keyExtractor={(item, index) => {
                        return 'coacher-' + index;
                    }}
                    data={props.coachers}
                    renderItem={renderGridItem}
                    numColumns={2}
                    perPage={16}
                    columnWrapperStyle={{
                        marginBottom: baseMargin.md,
                        justifyContent: 'space-between'
                    }}
                    contentContainerStyle={{
                        paddingHorizontal: basePadding.md
                    }}
                    onRefresh={onRefresh}
                    fetchNextBatch={(page, perPage) => {
                        props.coacherGetCoacher(props.accessToken, {
                            page,
                            perPage,
                            searchQuery
                        })
                    }}
                    renderPlaceholder={() => {
                        return (
                            <AchievementPlaceholder
                                backgroundColor={baseColor.one}
                            />
                        )
                    }}
                />
            </View>
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coachers: coacher.coachers
    }
}

export default connect(mapStateToProps, {
    coacherReducerUpdate,
    coacherGetCoacher
})(CoacherListScreen);
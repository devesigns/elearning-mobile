import _ from 'lodash';
import React, { memo } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    baseColor,
    basePadding,
    baseFontSize,
    baseFontFamily,
    dimension,
    baseBorderRadius
} from '../../styles/base';
import CustomMetaData, { CustomMetaDataBreaker } from '../common/CustomMetaData';

const CoacherGridItem = memo((props) => {
    return (
        <TouchableOpacity
            style={{
                width: (dimension.width - basePadding.md * 2) / 2 - basePadding.xs,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center'
            }}
            delayPressIn={50}
            onPress={props.onPress}
        >
            <View
                style={{
                    flex: 1,
                    width: '100%',
                    marginTop: dimension.width * 0.125,
                    padding: basePadding.sm,
                    backgroundColor: baseColor.one,
                    borderRadius: baseBorderRadius.md,
                    elevation: 3,
                    shadowOffset: {
                        width: 3,
                        height: 3
                    },
                    shadowOpacity: 0.5,
                    shadowColor: 'rgba(0, 0, 0, 0.3)',
                }}
            >
                <View
                    style={{
                        marginTop: - dimension.width * 0.125 - basePadding.sm,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <Image
                        source={{uri: props.data.user.avatar}}
                        style={{
                            width: dimension.width * 0.25,
                            height: dimension.width * 0.25,
                            borderRadius: dimension.width * 0.125,
                            backgroundColor: baseColor.one,
                        }}
                        resizeMode={'cover'}
                    />
                </View>
                <Text
                    style={{
                        fontSize: baseFontSize.md,
                        fontFamily: baseFontFamily.bold
                    }}
                >
                    {props.data.user.fullname}
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular,
                        color: baseColor.five
                    }}
                >
                    {props.data.summary}
                </Text>
                <Text
                    style={{
                        fontSize: baseFontSize.sm,
                        fontFamily: baseFontFamily.regular,
                        paddingTop: basePadding.sm
                    }}
                >
                    {
                        props.data.description.length > 60 ?
                        props.data.description.slice(0, 60) + '...'
                        :
                        props.data.description
                    }
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingTop: basePadding.sm
                    }}
                >
                    <CustomMetaData
                        iconName={'video'}
                        label={props.data.total_lessons}
                    />
                    <CustomMetaDataBreaker />
                    <CustomMetaData
                        iconName={'user'}
                        label={props.data.total_subscribers}
                    />
                </View>
            </View>
        </TouchableOpacity>
    )
}, (prevProps, nextProps) => {
    return _.isEqual(prevProps, nextProps);
})

export default CoacherGridItem;
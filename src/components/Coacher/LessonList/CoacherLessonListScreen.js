import React, { useState } from 'react';
import {
    View,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import {
    lessonReducerUpdate,
    coacherReducerUpdate,
    coacherGetLesson
} from '../../../actions';
import {
    basePadding,
    baseColor
} from '../../../styles/base';
import CoacherLessonListItem from './CoacherLessonListItem';
import CustomFlatListWithFetch from '../../common/CustomFlatListWithFetch';
import { VideoListPlaceholder } from '../../common/Placeholder';
import CustomSearchBar from '../../common/CustomSearchBar';

const CoacherLessonListScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    /**
     * Methods
     */

    const onRefresh = () => {
        props.coacherReducerUpdate('lessons', {
            ...props.lessons,
            all: null
        });
    }

    const onLessonPress = (lesson) => {
        props.lessonReducerUpdate('currentSession', {});
        props.lessonReducerUpdate('lessonId', lesson.id);
        props.lessonReducerUpdate('lessonType', lesson.course ? 'normal' : 'single');
        props.navigation.navigate('Lesson');
    }

    const onSearchSubmit = (query) => {
        if (query !== searchQuery) {
            setSearchQuery(query);
            onRefresh();
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }

    /**
     * Render methods
     */

    const renderLessonItem = ({item, index}) => {
        return (
            <CoacherLessonListItem
                data={item}
                onPress={() => onLessonPress(item)}
            />
        )
    }

    return (
        <SafeAreaView
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.md
                }}
            >
                <CustomSearchBar
                    placeholder={'Tìm bài học...'}
                    onSubmit={onSearchSubmit}
                />
            </View>
            <CustomFlatListWithFetch
                keyExtractor={(item, index) => {
                    return 'playlist-item-' + item.id
                }}
                data={props.lessons.all}
                renderItem={renderLessonItem}
                fetchNextBatch={(page, perPage) => {
                    props.coacherGetLesson(props.accessToken, {
                        coacherId: props.coacherForm.id,
                        page,
                        perPage,
                        type: 'all',
                        searchQuery
                    })
                }}
                onRefresh={onRefresh}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md,
                }}
                renderPlaceholder={() => {
                    return (
                        <VideoListPlaceholder />
                    )
                }}
                perPage={16}
            />
        </SafeAreaView>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm,
        lessons: coacher.lessons
    }
}

export default connect(mapStateToProps, {
    lessonReducerUpdate,
    coacherReducerUpdate,
    coacherGetLesson
})(CoacherLessonListScreen);
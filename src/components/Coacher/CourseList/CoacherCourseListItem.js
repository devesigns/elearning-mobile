import React, { useState } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
import {
    baseFontSize,
    baseFontFamily,
    basePadding,
    dimension,
    baseBorderRadius,
    baseColor,
    baseMargin,
} from '../../../styles/base';
import CustomMetaData, { CustomMetaDataBreaker } from '../../common/CustomMetaData';

const CoacherCourseListItem = (props) => {
    return (
        <TouchableOpacity
            style={{
                marginBottom: baseMargin.md
            }}
            onPress={props.onPress}
            delayPressIn={50}
        >
            <Image
                source={{uri: props.data.thumbnail}}
                style={{
                    width: '100%',
                    height: (dimension.width - basePadding.md * 2) * 9 / 16,
                    borderRadius: baseBorderRadius.md,
                    backgroundColor: baseColor.two
                }}
                resizeMode={'cover'}
            />
            <Text
                style={{
                    fontSize: baseFontSize.md,
                    fontFamily: baseFontFamily.bold,
                    color: baseColor.four,
                    paddingTop: basePadding.sm
                }}
                numberOfLines={2}
                ellipsizeMode={'tail'}
            >
                {props.data.title}
            </Text>
            <Text
                style={{
                    fontSize: baseFontSize.sm,
                    fontFamily: baseFontFamily.regular,
                    color: baseColor.four
                }}
                numberOfLines={2}
                ellipsizeMode={'tail'}
            >
                {props.data.description}
            </Text>
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingTop: basePadding.xs,
                    paddingBottom: basePadding.md
                }}
            >
                <CustomMetaData
                    iconName={'clock'}
                    label={props.data.total_time}
                />
                <CustomMetaDataBreaker />
                <CustomMetaData
                    iconName={'file-text'}
                    label={props.data.total_lesson}
                />    
            </View>
        </TouchableOpacity>
    )
}

export default CoacherCourseListItem;
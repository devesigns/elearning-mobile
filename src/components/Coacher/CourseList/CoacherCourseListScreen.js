import React, { useState } from 'react';
import {
    View
} from 'react-native';
import { connect } from 'react-redux';
import {
    courseReducerUpdate,
    coacherReducerUpdate,
    coacherGetCourse
} from '../../../actions';
import {
    basePadding,
    baseColor
} from '../../../styles/base';
import CoacherCourseListItem from './CoacherCourseListItem';
import CustomFlatListWithFetch from '../../common/CustomFlatListWithFetch';
import { VideoListPlaceholderType2 } from '../../common/Placeholder';
import CustomSearchBar from '../../common/CustomSearchBar';

const CoacherCourseListScreen = (props) => {

    const [mounted, setMounted] = useState(false);
    const [searchQuery, setSearchQuery] = useState('');

    /**
     * Methods
     */

    const onRefresh = () => {
        props.coacherReducerUpdate('courses', {
            ...props.courses,
            all: null
        });
    }

    const onCoursePress = (item) => {
        props.courseReducerUpdate('courseForm', {
            course: item
        });
        props.navigation.navigate('CourseDetail');
    }

    const onSearchSubmit = (query) => {
        if (query !== searchQuery) {
            setSearchQuery(query);
            onRefresh();
        }
    }

    if (!mounted) {
        onRefresh();
        setMounted(true);
    }

    /**
     * Render methods
     */

    const renderLessonItem = ({item, index}) => {
        return (
            <CoacherCourseListItem
                data={item}
                onPress={() => onCoursePress(item)}
            />
        )
    }

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: baseColor.one
            }}
        >
            <View
                style={{
                    backgroundColor: baseColor.one,
                    padding: basePadding.md
                }}
            >
                <CustomSearchBar
                    placeholder={'Tìm khóa học...'}
                    onSubmit={onSearchSubmit}
                />
            </View>
            <CustomFlatListWithFetch
                keyExtractor={(item, index) => {
                    return 'playlist-item-' + item.id
                }}
                data={props.courses.all}
                renderItem={renderLessonItem}
                fetchNextBatch={(page, perPage) => {
                    props.coacherGetCourse(props.accessToken, {
                        coacherId: props.coacherForm.id,
                        page,
                        perPage,
                        type: 'all',
                        searchQuery
                    })
                }}
                onRefresh={onRefresh}
                contentContainerStyle={{
                    paddingHorizontal: basePadding.md,
                }}
                renderPlaceholder={() => {
                    return (
                        <VideoListPlaceholderType2 />
                    )
                }}
                perPage={16}
            />
        </View>
    )
}

const mapStateToProps = ({ auth, coacher }) => {
    return {
        accessToken: auth.accessToken,
        coacherForm: coacher.coacherForm,
        courses: coacher.courses
    }
}

export default connect(mapStateToProps, {
    courseReducerUpdate,
    coacherReducerUpdate,
    coacherGetCourse
})(CoacherCourseListScreen);